# README #

This README guides you to first program ether_test_top_row0.bit onto [Atlys FPGA] [Atlys Link] and then some other .bit files. The bit files are created using Xilinx ISE 13.3 on Ubuntu 13.04 Linux. What this (ether_test_top_row0.bit) file does is make [Atlys] [Atlys Link] FPGA create 3 RTP packets of 1292,1292 and 1316 bytes from one row (1280 pixels) of 720p frame. These RTP packet are then  encapsulated as UDP packets, then encapsulated as IP packets and finally encapsulated as Ethernet frames and sent off to destination PC via [Atlys] [Atlys Link] 1 Gbps MAC interface. On the destination side, these packets are reassembled by [Gstreamer] [Gstreamer Link] to construct one row (1280 pixels) and displayed.

[Atlys Link]: http://bit.ly/1wA8Dim

### Prerequisites ###

* [Atlys FPGA] [Atlys Link]
* [JTAG Cable Drivers] [Cable Drivers Link]
* [Xilinx Impact tool] [Impact Link] or [Xc3sprog][Xc3sprog Link] or 
  [Adept][Adept Link] if you are using Windows. I have only used the 
  Impact and Adept.
* Linux (Ubuntu is preferred) or Windows.
* Machine with Ethernet LAN Card that support **1 Gbps**
* [Gstreamer] [Gstreamer Link]. On Ubuntu you can install by executing

```
#!bash
$sudo apt-get install gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer-tools
```

* [Wireshark] [Wireshark Link]. On Ubuntu, you can install by executing the following command

```
#!bash
$sudo apt-get install wireshark

```



[Cable Drivers Link]: http://wiki.gentoo.org/wiki/Xilinx_USB_JTAG_Programmers
[Impact Link]: http://www.xilinx.com/support/answers/12858.html
[Xc3sprog Link]: http://xc3sprog.sourceforge.net/
[Adept Link]: http://www.digilentinc.com/Products/Detail.cfm?NavPath=2,66,828&Prod=ADEPT2
[Gstreamer Link]: http://gstreamer.freedesktop.org/download/
[Wireshark Link]: http://www.wireshark.org/download.html

### How do I get set up? ###

* Connect Ethernet Cable (Cat5 or above) between the FPGA and your machine
* Change the IP address of your machine to 192.168.1.1
* Run Wireshark to capture packets on the interface with IP address 
  192.168.1.1
* Run Gstreamer program in a shell with the following command (It would launch a blank window)



```
#!bash

$GST_DEBUG=rtp*:9 gst-launch-1.0 -v udpsrc uri=udp://192.168.1.1:5004 caps="application/x-rtp, media=(string)video, clock-rate=(int)90000,encoding-name=(string)RAW, sampling=(string)RGB, depth=(string)8,width=(string)1280, height=(string)1" !  rtpvrawdepay  ! videoconvert ! videoscale ! "video/x-raw,height=720" ! autovideosink
```


* Turn the Atlys board on
* Use Xilinx Impact or Xc3s or Adept to program the ethernet_test_top_row0.bit onto Atlys FPGA
* Observe Wireshark and Gstreamer window

You should observe the following in the Wireshark and Gstreamer window

![Wireshark_row0_capture.jpg](https://bitbucket.org/repo/5AGLyB/images/1472311329-Wireshark_row0_capture.jpg)



![Gstreamer_row0_capture.jpg](https://bitbucket.org/repo/5AGLyB/images/1227942229-Gstreamer_row0_capture.jpg)


### Using other .bit files ###

Other bit files generate a whole frame or back to back frames. These files are:

1. ethernet_test_top_frame.bit
2. ethernet_test_top_rgb.bit
3. ethernet_test_top_rgb_repeat.bit

The instructions for programming any of these bit files are the same as mentioned above, except that you have to execute the following Gstreamer command in the shell prior to downloading the bit file on the FPGA.


```
#!bash

$GST_DEBUG_FILE=DEBUG_RTP2.log GST_DEBUG=rtp*:9 gst-launch-1.0 -v udpsrc uri=udp://192.168.1.1:5004  caps="application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)RAW, sampling=(string)RGB, depth=(string)8, width=(string)1280, height=(string)720"   ! rtpbin ! rtpvrawdepay  ! videoconvert !  autovideosink

```

Here is how the output of ethernet_test_top_frame.bit looks on Gstreamer

![720p_frame_clean.png](https://bitbucket.org/repo/5AGLyB/images/1105487926-720p_frame_clean.png)


and here is how the output of ethernet_test_top_rgb.bit looks on Gstreamer

![rgb_frame.png](https://bitbucket.org/repo/5AGLyB/images/233145736-rgb_frame.png)