////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: O.76xd
//  \   \         Application: netgen
//  /   /         Filename: ethernet_test_top_timesim.v
// /___/   /\     Timestamp: Tue Jul 29 14:34:07 2014
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -filter /home/user/gsoc14/atlys_ethernet_test_v1/iseconfig/filter.filter -intstyle ise -s 3 -pcf ethernet_test_top.pcf -sdf_anno true -sdf_path netgen/par -insert_glbl true -insert_pp_buffers true -w -dir netgen/par -ofmt verilog -sim ethernet_test_top.ncd ethernet_test_top_timesim.v 
// Device	: 6slx45csg324-3 (PRODUCTION 1.20 2011-10-03)
// Input file	: ethernet_test_top.ncd
// Output file	: /home/user/gsoc14/atlys_ethernet_test_v1/netgen/par/ethernet_test_top_timesim.v
// # of Modules	: 58
// Design Name	: ethernet_test_top
// Xilinx        : /home/centos_home/eda/xilinx/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps
module ip_header_checksum (
  clk, reset, header, checksum
);
  input clk;
  input reset;
  input [31 : 0] header;
  output [15 : 0] checksum;
  wire n0000_inv;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<0>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<1>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<2>_0 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_19127 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<3>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<4>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<5>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<6>_0 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_19140 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<7>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<8>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<9>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<10>_0 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_19153 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<11>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<12>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<13>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<14>_0 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<15>_0 ;
  wire \_n0042<0>_0 ;
  wire \_n0042<1>_0 ;
  wire \_n0042<2>_0 ;
  wire \Maccum_checksum_int_cy[3] ;
  wire \_n0042<3>_0 ;
  wire \_n0042<4>_0 ;
  wire \_n0042<5>_0 ;
  wire \_n0042<6>_0 ;
  wire \Maccum_checksum_int_cy[7] ;
  wire \_n0042<7>_0 ;
  wire \_n0042<8>_0 ;
  wire \_n0042<9>_0 ;
  wire \_n0042<10>_0 ;
  wire \Maccum_checksum_int_cy[11] ;
  wire \_n0042<11>_0 ;
  wire \_n0042<12>_0 ;
  wire \_n0042<13>_0 ;
  wire \_n0042<14>_0 ;
  wire \Maccum_checksum_int_cy[15] ;
  wire \_n0042<15>_0 ;
  wire \Maccum_checksum_int_cy[19] ;
  wire \Maccum_checksum_int_cy[23] ;
  wire \Maccum_checksum_int_cy[27] ;
  wire \Madd__n0042_cy[6] ;
  wire \Madd__n0042_cy[10] ;
  wire \header_count<2>_0 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<0>_18661 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<1>_18657 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<2>_18653 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<0> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<1> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<2> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<3> ;
  wire \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/ProtoComp159.CYINITGND.0 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<3>_18643 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<4>_18683 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<5>_18679 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<6>_18675 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<4> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<5> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<6> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<7> ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<7>_18665 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<8>_18705 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<9>_18701 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<10>_18697 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<8> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<9> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<10> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<11> ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<11>_18687 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<12>_18725 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<13>_18721 ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<14>_18717 ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<12> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<13> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<14> ;
  wire \checksum_int[31]_checksum_int[15]_add_11_OUT<15> ;
  wire \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<15>_18708 ;
  wire \Result<0>1 ;
  wire \Result<1>1 ;
  wire \Result<2>1 ;
  wire \packet_sender/ip_header_checksum/checksum_int<3>/ProtoComp181.CYINITGND.0 ;
  wire \checksum_int<16>_rt_18867 ;
  wire \checksum_int<17>_rt_18862 ;
  wire \checksum_int<18>_rt_18858 ;
  wire \checksum_int<19>_rt_18846 ;
  wire \checksum_int<20>_rt_18894 ;
  wire \checksum_int<21>_rt_18890 ;
  wire \checksum_int<22>_rt_18886 ;
  wire \checksum_int<23>_rt_18874 ;
  wire \checksum_int<24>_rt_18923 ;
  wire \checksum_int<25>_rt_18919 ;
  wire \checksum_int<26>_rt_18915 ;
  wire \checksum_int<27>_rt_18903 ;
  wire \checksum_int<28>_rt_18950 ;
  wire \checksum_int<29>_rt_18946 ;
  wire \checksum_int<30>_rt_18942 ;
  wire \checksum_int<31>_rt_18932 ;
  wire \Madd__n0042_lut[3] ;
  wire \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_20_rt_18969 ;
  wire \Madd__n0042_lut[5] ;
  wire \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/ProtoComp194.CYINITGND.0 ;
  wire \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_6_rt_18957 ;
  wire \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_31_rt_18994 ;
  wire \Madd__n0042_lut[8] ;
  wire \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_29_rt_18987 ;
  wire \Madd__n0042_lut[10] ;
  wire \packet_sender/ip_header_checksum/_n0042<12>/packet_sender/header_checksum_input_29_rt ;
  wire \Madd__n0042_lut[0] ;
  wire \Madd__n0042_lut[1] ;
  wire \packet_sender/ip_header_checksum/_n0042<2>/ProtoComp199.CYINITGND.0 ;
  wire \packet_sender/ip_header_checksum/_n0042<13>/packet_sender/header_checksum_input_29_rt ;
  wire \packet_sender/ip_header_checksum/_n0042<13>/ProtoComp543.CYINITGND.0 ;
  wire \packet_sender/ip_header_checksum/_n0042<15>/packet_sender/header_checksum_input_31_rt ;
  wire \packet_sender/ip_header_checksum/_n0042<15>/ProtoComp543.CYINITGND.0 ;
  wire \packet_sender/header_checksum<3>/packet_sender/header_checksum_input_30_rt_19057 ;
  wire \packet_sender/header_checksum<3>/ProtoComp626.CYINITGND.0 ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_3/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_2/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_1/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_0/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_7/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_6/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_5/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_4/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_11/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_10/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_9/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_8/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_15/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_14/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_13/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_12/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_19/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_18/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<19>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_17/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_16/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_23/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_22/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_21/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_20/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_27/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_26/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_25/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_24/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_31/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_30/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_29/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_28/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<2> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<3> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<0> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<1> ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/header_count_2/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/header_count_1/CLK ;
  wire \NlwBufferSignal_packet_sender/ip_header_checksum/header_count_0/CLK ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[3]_UNCONNECTED ;
  wire \NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_DI[3]_UNCONNECTED ;
  wire GND;
  wire \NLW_Maccum_checksum_int_cy<3>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<3>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<3>_CO[2]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<7>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<7>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<7>_CO[2]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<11>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<11>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<11>_CO[2]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<15>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<15>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<15>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_16.D5LUT_O_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<19>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<19>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<19>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_15.C5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_14.B5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_20.D5LUT_O_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<23>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<23>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<23>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_19.C5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_18.B5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_17.A5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_24.D5LUT_O_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<27>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<27>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_cy<27>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_23.C5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_22.B5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_21.A5LUT_O_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_xor<31>_CO[0]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_xor<31>_CO[1]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_xor<31>_CO[2]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_xor<31>_CO[3]_UNCONNECTED ;
  wire \NLW_Maccum_checksum_int_xor<31>_DI[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_27.C5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_26.B5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_25.A5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<6>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<6>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<6>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/wr2_flags<2>_78.B5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<10>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<10>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<10>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/wr2_flags<2>_80.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/wr2_flags<2>_79.A5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_CO[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_DI[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_DI[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_DI[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_O[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_O[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_S[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<12>_S[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_6.B6LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/ip_header_checksum/_n0042<12>/wr2_flags<2>_81.A5LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_CO[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_DI[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_DI[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_O[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<2>_S[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_cy<12>_3.C6LUT_O_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_CO[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_DI[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_DI[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_DI[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_DI[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_O[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_O[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_O[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_S[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_S[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<13>_S[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_CO[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_DI[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_DI[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_DI[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_DI[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_O[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_O[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_O[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_S[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_S[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<15>_S[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_CO[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_DI[0]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_DI[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_DI[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_DI[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_O[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_O[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_O[3]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_S[1]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_S[2]_UNCONNECTED ;
  wire \NLW_Madd__n0042_xor<14>_S[3]_UNCONNECTED ;
  wire [31 : 0] checksum_int;
  wire [2 : 0] header_count;
  wire [15 : 0] Maccum_checksum_int_lut;
  wire [31 : 0] Result;
  wire [15 : 0] _n0042;
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_DMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<3> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<3>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_CMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<2> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<2>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_BMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<1> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<1>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_AMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<0> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<0>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y36" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<3>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR5(checksum_int[19]),
    .ADR4(checksum_int[3]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<3>_18643 )
  );
  X_ZERO #(
    .LOC ( "SLICE_X12Y36" ))
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/ProtoComp159.CYINITGND.1  (
    .O(\packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/ProtoComp159.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X12Y36" ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/ProtoComp159.CYINITGND.0 ),
    .CO({\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_19127 , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_CO[2]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_CO[1]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<0> }),
    .O({\checksum_int[31]_checksum_int[15]_add_11_OUT<3> , \checksum_int[31]_checksum_int[15]_add_11_OUT<2> , 
\checksum_int[31]_checksum_int[15]_add_11_OUT<1> , \checksum_int[31]_checksum_int[15]_add_11_OUT<0> }),
    .S({\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<3>_18643 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<2>_18653 , 
\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<1>_18657 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<0>_18661 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y36" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<2>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[18]),
    .ADR3(checksum_int[2]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<2>_18653 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y36" ),
    .INIT ( 64'h00FF00FFFF00FF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<1>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(checksum_int[17]),
    .ADR5(checksum_int[1]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<1>_18657 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y36" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<0>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[16]),
    .ADR3(checksum_int[0]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<0>_18661 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_DMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<7> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<7>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_CMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<6> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<6>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_BMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<5> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<5>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_AMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<4> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<4>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y37" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<7>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR5(checksum_int[23]),
    .ADR4(checksum_int[7]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<7>_18665 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X12Y37" ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>  (
    .CI(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>_19127 ),
    .CYINIT(1'b0),
    .CO({\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_19140 , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_CO[2]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_CO[1]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<0> }),
    .O({\checksum_int[31]_checksum_int[15]_add_11_OUT<7> , \checksum_int[31]_checksum_int[15]_add_11_OUT<6> , 
\checksum_int[31]_checksum_int[15]_add_11_OUT<5> , \checksum_int[31]_checksum_int[15]_add_11_OUT<4> }),
    .S({\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<7>_18665 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<6>_18675 , 
\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<5>_18679 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<4>_18683 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y37" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<6>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[22]),
    .ADR3(checksum_int[6]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<6>_18675 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y37" ),
    .INIT ( 64'h00FF00FFFF00FF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<5>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(checksum_int[21]),
    .ADR5(checksum_int[5]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<5>_18679 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y37" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<4>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[20]),
    .ADR3(checksum_int[4]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<4>_18683 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_DMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<11> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<11>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_CMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<10> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<10>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_BMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<9> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<9>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_AMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<8> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<8>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y38" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<11>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR5(checksum_int[27]),
    .ADR4(checksum_int[11]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<11>_18687 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X12Y38" ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>  (
    .CI(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>_19140 ),
    .CYINIT(1'b0),
    .CO({\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_19153 , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_CO[2]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_CO[1]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<0> }),
    .O({\checksum_int[31]_checksum_int[15]_add_11_OUT<11> , \checksum_int[31]_checksum_int[15]_add_11_OUT<10> , 
\checksum_int[31]_checksum_int[15]_add_11_OUT<9> , \checksum_int[31]_checksum_int[15]_add_11_OUT<8> }),
    .S({\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<11>_18687 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<10>_18697 , 
\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<9>_18701 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<8>_18705 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y38" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<10>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[26]),
    .ADR3(checksum_int[10]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<10>_18697 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y38" ),
    .INIT ( 64'h00FF00FFFF00FF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<9>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(checksum_int[25]),
    .ADR5(checksum_int[9]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<9>_18701 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y38" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<8>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[24]),
    .ADR3(checksum_int[8]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<8>_18705 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>/packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>_DMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<15> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<15>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>/packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>_CMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<14> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<14>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>/packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>_BMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<13> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<13>_0 )
  );
  X_BUF 
  \packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>/packet_sender/ip_header_checksum/checksum_int[31]_checksum_int[15]_add_11_OUT<15>_AMUX_Delay  (
    .I(\checksum_int[31]_checksum_int[15]_add_11_OUT<12> ),
    .O(\checksum_int[31]_checksum_int[15]_add_11_OUT<12>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y39" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<15>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR3(checksum_int[31]),
    .ADR4(checksum_int[15]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<15>_18708 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X12Y39" ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>  (
    .CI(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>_19153 ),
    .CYINIT(1'b0),
    .CO({\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[3]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[2]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[1]_UNCONNECTED , 
\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>_DI[3]_UNCONNECTED , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<0> }),
    .O({\checksum_int[31]_checksum_int[15]_add_11_OUT<15> , \checksum_int[31]_checksum_int[15]_add_11_OUT<14> , 
\checksum_int[31]_checksum_int[15]_add_11_OUT<13> , \checksum_int[31]_checksum_int[15]_add_11_OUT<12> }),
    .S({\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<15>_18708 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<14>_18717 , 
\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<13>_18721 , \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<12>_18725 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y39" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<14>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[30]),
    .ADR3(checksum_int[14]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<14>_18717 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y39" ),
    .INIT ( 64'h00FF00FFFF00FF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<13>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(checksum_int[29]),
    .ADR5(checksum_int[13]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<13>_18721 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y39" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<12>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[28]),
    .ADR3(checksum_int[12]),
    .O(\Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_lut<12>_18725 )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 1'b0 ))
  checksum_int_3 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_3/CLK ),
    .I(Result[3]),
    .O(checksum_int[3]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 64'h33333333CCCCCCCC ))
  \Maccum_checksum_int_lut<3>  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR5(checksum_int[3]),
    .ADR4(1'b1),
    .ADR1(\_n0042<3>_0 ),
    .O(Maccum_checksum_int_lut[3])
  );
  X_ZERO #(
    .LOC ( "SLICE_X10Y32" ))
  \packet_sender/ip_header_checksum/checksum_int<3>/ProtoComp181.CYINITGND  (
    .O(\packet_sender/ip_header_checksum/checksum_int<3>/ProtoComp181.CYINITGND.0 )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 1'b0 ))
  checksum_int_2 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_2/CLK ),
    .I(\Result<2>1 ),
    .O(checksum_int[2]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y32" ))
  \Maccum_checksum_int_cy<3>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/ip_header_checksum/checksum_int<3>/ProtoComp181.CYINITGND.0 ),
    .CO({\Maccum_checksum_int_cy[3] , \NLW_Maccum_checksum_int_cy<3>_CO[2]_UNCONNECTED , \NLW_Maccum_checksum_int_cy<3>_CO[1]_UNCONNECTED , 
\NLW_Maccum_checksum_int_cy<3>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<0> }),
    .O({Result[3], \Result<2>1 , \Result<1>1 , \Result<0>1 }),
    .S({Maccum_checksum_int_lut[3], Maccum_checksum_int_lut[2], Maccum_checksum_int_lut[1], Maccum_checksum_int_lut[0]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Maccum_checksum_int_lut<2>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[2]),
    .ADR3(1'b1),
    .ADR5(\_n0042<2>_0 ),
    .O(Maccum_checksum_int_lut[2])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 1'b0 ))
  checksum_int_1 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_1/CLK ),
    .I(\Result<1>1 ),
    .O(checksum_int[1]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Maccum_checksum_int_lut<1>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[1]),
    .ADR5(1'b1),
    .ADR3(\_n0042<1>_0 ),
    .O(Maccum_checksum_int_lut[1])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 1'b0 ))
  checksum_int_0 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_0/CLK ),
    .I(\Result<0>1 ),
    .O(checksum_int[0]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y32" ),
    .INIT ( 64'h55555555AAAAAAAA ))
  \Maccum_checksum_int_lut<0>  (
    .ADR3(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(checksum_int[0]),
    .ADR4(1'b1),
    .ADR0(\_n0042<0>_0 ),
    .O(Maccum_checksum_int_lut[0])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 1'b0 ))
  checksum_int_7 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_7/CLK ),
    .I(Result[7]),
    .O(checksum_int[7]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Maccum_checksum_int_lut<7>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(checksum_int[7]),
    .ADR3(1'b1),
    .ADR4(\_n0042<7>_0 ),
    .O(Maccum_checksum_int_lut[7])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 1'b0 ))
  checksum_int_6 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_6/CLK ),
    .I(Result[6]),
    .O(checksum_int[6]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y33" ))
  \Maccum_checksum_int_cy<7>  (
    .CI(\Maccum_checksum_int_cy[3] ),
    .CYINIT(1'b0),
    .CO({\Maccum_checksum_int_cy[7] , \NLW_Maccum_checksum_int_cy<7>_CO[2]_UNCONNECTED , \NLW_Maccum_checksum_int_cy<7>_CO[1]_UNCONNECTED , 
\NLW_Maccum_checksum_int_cy<7>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<0> }),
    .O({Result[7], Result[6], Result[5], Result[4]}),
    .S({Maccum_checksum_int_lut[7], Maccum_checksum_int_lut[6], Maccum_checksum_int_lut[5], Maccum_checksum_int_lut[4]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Maccum_checksum_int_lut<6>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[6]),
    .ADR5(1'b1),
    .ADR3(\_n0042<6>_0 ),
    .O(Maccum_checksum_int_lut[6])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 1'b0 ))
  checksum_int_5 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_5/CLK ),
    .I(Result[5]),
    .O(checksum_int[5]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 64'h3333CCCC3333CCCC ))
  \Maccum_checksum_int_lut<5>  (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[5]),
    .ADR3(1'b1),
    .ADR1(\_n0042<5>_0 ),
    .O(Maccum_checksum_int_lut[5])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 1'b0 ))
  checksum_int_4 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_4/CLK ),
    .I(Result[4]),
    .O(checksum_int[4]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y33" ),
    .INIT ( 64'h0F0F0F0FF0F0F0F0 ))
  \Maccum_checksum_int_lut<4>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR5(checksum_int[4]),
    .ADR4(1'b1),
    .ADR2(\_n0042<4>_0 ),
    .O(Maccum_checksum_int_lut[4])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 1'b0 ))
  checksum_int_11 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_11/CLK ),
    .I(Result[11]),
    .O(checksum_int[11]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Maccum_checksum_int_lut<11>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(checksum_int[11]),
    .ADR3(1'b1),
    .ADR4(\_n0042<11>_0 ),
    .O(Maccum_checksum_int_lut[11])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 1'b0 ))
  checksum_int_10 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_10/CLK ),
    .I(Result[10]),
    .O(checksum_int[10]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y34" ))
  \Maccum_checksum_int_cy<11>  (
    .CI(\Maccum_checksum_int_cy[7] ),
    .CYINIT(1'b0),
    .CO({\Maccum_checksum_int_cy[11] , \NLW_Maccum_checksum_int_cy<11>_CO[2]_UNCONNECTED , \NLW_Maccum_checksum_int_cy<11>_CO[1]_UNCONNECTED , 
\NLW_Maccum_checksum_int_cy<11>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<0> }),
    .O({Result[11], Result[10], Result[9], Result[8]}),
    .S({Maccum_checksum_int_lut[11], Maccum_checksum_int_lut[10], Maccum_checksum_int_lut[9], Maccum_checksum_int_lut[8]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Maccum_checksum_int_lut<10>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[10]),
    .ADR5(1'b1),
    .ADR3(\_n0042<10>_0 ),
    .O(Maccum_checksum_int_lut[10])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 1'b0 ))
  checksum_int_9 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_9/CLK ),
    .I(Result[9]),
    .O(checksum_int[9]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 64'h3333CCCC3333CCCC ))
  \Maccum_checksum_int_lut<9>  (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[9]),
    .ADR3(1'b1),
    .ADR1(\_n0042<9>_0 ),
    .O(Maccum_checksum_int_lut[9])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 1'b0 ))
  checksum_int_8 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_8/CLK ),
    .I(Result[8]),
    .O(checksum_int[8]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y34" ),
    .INIT ( 64'h0F0F0F0FF0F0F0F0 ))
  \Maccum_checksum_int_lut<8>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR5(checksum_int[8]),
    .ADR4(1'b1),
    .ADR2(\_n0042<8>_0 ),
    .O(Maccum_checksum_int_lut[8])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 1'b0 ))
  checksum_int_15 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_15/CLK ),
    .I(Result[15]),
    .O(checksum_int[15]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Maccum_checksum_int_lut<15>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(checksum_int[15]),
    .ADR3(1'b1),
    .ADR4(\_n0042<15>_0 ),
    .O(Maccum_checksum_int_lut[15])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 1'b0 ))
  checksum_int_14 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_14/CLK ),
    .I(Result[14]),
    .O(checksum_int[14]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y35" ))
  \Maccum_checksum_int_cy<15>  (
    .CI(\Maccum_checksum_int_cy[11] ),
    .CYINIT(1'b0),
    .CO({\Maccum_checksum_int_cy[15] , \NLW_Maccum_checksum_int_cy<15>_CO[2]_UNCONNECTED , \NLW_Maccum_checksum_int_cy<15>_CO[1]_UNCONNECTED , 
\NLW_Maccum_checksum_int_cy<15>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<2> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<0> }),
    .O({Result[15], Result[14], Result[13], Result[12]}),
    .S({Maccum_checksum_int_lut[15], Maccum_checksum_int_lut[14], Maccum_checksum_int_lut[13], Maccum_checksum_int_lut[12]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Maccum_checksum_int_lut<14>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[14]),
    .ADR5(1'b1),
    .ADR3(\_n0042<14>_0 ),
    .O(Maccum_checksum_int_lut[14])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 1'b0 ))
  checksum_int_13 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_13/CLK ),
    .I(Result[13]),
    .O(checksum_int[13]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 64'h0F0FF0F00F0FF0F0 ))
  \Maccum_checksum_int_lut<13>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR4(checksum_int[13]),
    .ADR3(1'b1),
    .ADR2(\_n0042<13>_0 ),
    .O(Maccum_checksum_int_lut[13])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 1'b0 ))
  checksum_int_12 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_12/CLK ),
    .I(Result[12]),
    .O(checksum_int[12]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y35" ),
    .INIT ( 64'h0F0F0F0FF0F0F0F0 ))
  \Maccum_checksum_int_lut<12>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR5(checksum_int[12]),
    .ADR4(1'b1),
    .ADR2(\_n0042<12>_0 ),
    .O(Maccum_checksum_int_lut[12])
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 1'b0 ))
  checksum_int_19 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_19/CLK ),
    .I(Result[19]),
    .O(checksum_int[19]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<19>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[19]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<19>_rt_18846 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_16.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_16.D5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 1'b0 ))
  checksum_int_18 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_18/CLK ),
    .I(Result[18]),
    .O(checksum_int[18]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y36" ))
  \Maccum_checksum_int_cy<19>  (
    .CI(\Maccum_checksum_int_cy[15] ),
    .CYINIT(1'b0),
    .CO({\Maccum_checksum_int_cy[19] , \NLW_Maccum_checksum_int_cy<19>_CO[2]_UNCONNECTED , \NLW_Maccum_checksum_int_cy<19>_CO[1]_UNCONNECTED , 
\NLW_Maccum_checksum_int_cy<19>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, \NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<19>/DI<0> }),
    .O({Result[19], Result[18], Result[17], Result[16]}),
    .S({\checksum_int<19>_rt_18846 , \checksum_int<18>_rt_18858 , \checksum_int<17>_rt_18862 , \checksum_int<16>_rt_18867 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<18>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[18]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<18>_rt_18858 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_15.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_15.C5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 1'b0 ))
  checksum_int_17 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_17/CLK ),
    .I(Result[17]),
    .O(checksum_int[17]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<17>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[17]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<17>_rt_18862 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_14.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_14.B5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 1'b0 ))
  checksum_int_16 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_16/CLK ),
    .I(Result[16]),
    .O(checksum_int[16]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y36" ),
    .INIT ( 64'hFFFFFFFF00000000 ))
  \checksum_int<16>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(checksum_int[16]),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .O(\checksum_int<16>_rt_18867 )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 1'b0 ))
  checksum_int_23 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_23/CLK ),
    .I(Result[23]),
    .O(checksum_int[23]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<23>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[23]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<23>_rt_18874 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_20.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_20.D5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 1'b0 ))
  checksum_int_22 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_22/CLK ),
    .I(Result[22]),
    .O(checksum_int[22]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y37" ))
  \Maccum_checksum_int_cy<23>  (
    .CI(\Maccum_checksum_int_cy[19] ),
    .CYINIT(1'b0),
    .CO({\Maccum_checksum_int_cy[23] , \NLW_Maccum_checksum_int_cy<23>_CO[2]_UNCONNECTED , \NLW_Maccum_checksum_int_cy<23>_CO[1]_UNCONNECTED , 
\NLW_Maccum_checksum_int_cy<23>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Result[23], Result[22], Result[21], Result[20]}),
    .S({\checksum_int<23>_rt_18874 , \checksum_int<22>_rt_18886 , \checksum_int<21>_rt_18890 , \checksum_int<20>_rt_18894 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<22>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[22]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<22>_rt_18886 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_19.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_19.C5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 1'b0 ))
  checksum_int_21 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_21/CLK ),
    .I(Result[21]),
    .O(checksum_int[21]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<21>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[21]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<21>_rt_18890 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_18.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_18.B5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 1'b0 ))
  checksum_int_20 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_20/CLK ),
    .I(Result[20]),
    .O(checksum_int[20]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<20>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[20]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<20>_rt_18894 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y37" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_17.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_17.A5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 1'b0 ))
  checksum_int_27 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_27/CLK ),
    .I(Result[27]),
    .O(checksum_int[27]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<27>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[27]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<27>_rt_18903 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_24.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_24.D5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 1'b0 ))
  checksum_int_26 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_26/CLK ),
    .I(Result[26]),
    .O(checksum_int[26]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y38" ))
  \Maccum_checksum_int_cy<27>  (
    .CI(\Maccum_checksum_int_cy[23] ),
    .CYINIT(1'b0),
    .CO({\Maccum_checksum_int_cy[27] , \NLW_Maccum_checksum_int_cy<27>_CO[2]_UNCONNECTED , \NLW_Maccum_checksum_int_cy<27>_CO[1]_UNCONNECTED , 
\NLW_Maccum_checksum_int_cy<27>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Result[27], Result[26], Result[25], Result[24]}),
    .S({\checksum_int<27>_rt_18903 , \checksum_int<26>_rt_18915 , \checksum_int<25>_rt_18919 , \checksum_int<24>_rt_18923 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<26>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[26]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<26>_rt_18915 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_23.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_23.C5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 1'b0 ))
  checksum_int_25 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_25/CLK ),
    .I(Result[25]),
    .O(checksum_int[25]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<25>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[25]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<25>_rt_18919 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_22.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_22.B5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 1'b0 ))
  checksum_int_24 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_24/CLK ),
    .I(Result[24]),
    .O(checksum_int[24]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<24>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[24]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<24>_rt_18923 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y38" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_21.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_21.A5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 1'b0 ))
  checksum_int_31 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_31/CLK ),
    .I(Result[31]),
    .O(checksum_int[31]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 64'hFFFFFFFF00000000 ))
  \checksum_int<31>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(checksum_int[31]),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .O(\checksum_int<31>_rt_18932 )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 1'b0 ))
  checksum_int_30 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_30/CLK ),
    .I(Result[30]),
    .O(checksum_int[30]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X10Y39" ))
  \Maccum_checksum_int_xor<31>  (
    .CI(\Maccum_checksum_int_cy[27] ),
    .CYINIT(1'b0),
    .CO({\NLW_Maccum_checksum_int_xor<31>_CO[3]_UNCONNECTED , \NLW_Maccum_checksum_int_xor<31>_CO[2]_UNCONNECTED , 
\NLW_Maccum_checksum_int_xor<31>_CO[1]_UNCONNECTED , \NLW_Maccum_checksum_int_xor<31>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Maccum_checksum_int_xor<31>_DI[3]_UNCONNECTED , 1'b0, 1'b0, 1'b0}),
    .O({Result[31], Result[30], Result[29], Result[28]}),
    .S({\checksum_int<31>_rt_18932 , \checksum_int<30>_rt_18942 , \checksum_int<29>_rt_18946 , \checksum_int<28>_rt_18950 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<30>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[30]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<30>_rt_18942 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_27.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_27.C5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 1'b0 ))
  checksum_int_29 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_29/CLK ),
    .I(Result[29]),
    .O(checksum_int[29]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<29>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[29]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<29>_rt_18946 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_26.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_26.B5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 1'b0 ))
  checksum_int_28 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_28/CLK ),
    .I(Result[28]),
    .O(checksum_int[28]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \checksum_int<28>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(checksum_int[28]),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .O(\checksum_int<28>_rt_18950 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X10Y39" ),
    .INIT ( 32'h00000000 ))
  \Madd__n0042_cy<12>_25.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_25.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/ip_header_checksum/Madd__n0042_cy<6>_DMUX_Delay  (
    .I(_n0042[6]),
    .O(\_n0042<6>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/ip_header_checksum/Madd__n0042_cy<6>_CMUX_Delay  (
    .I(_n0042[5]),
    .O(\_n0042<5>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/ip_header_checksum/Madd__n0042_cy<6>_BMUX_Delay  (
    .I(_n0042[4]),
    .O(\_n0042<4>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/ip_header_checksum/Madd__n0042_cy<6>_AMUX_Delay  (
    .I(_n0042[3]),
    .O(\_n0042<3>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y33" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_6_rt  (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR1(header[6]),
    .O(\packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_6_rt_18957 )
  );
  X_ZERO #(
    .LOC ( "SLICE_X14Y33" ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/ProtoComp194.CYINITGND  (
    .O(\packet_sender/ip_header_checksum/Madd__n0042_cy<6>/ProtoComp194.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X14Y33" ))
  \Madd__n0042_cy<6>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/ip_header_checksum/Madd__n0042_cy<6>/ProtoComp194.CYINITGND.0 ),
    .CO({\Madd__n0042_cy[6] , \NLW_Madd__n0042_cy<6>_CO[2]_UNCONNECTED , \NLW_Madd__n0042_cy<6>_CO[1]_UNCONNECTED , 
\NLW_Madd__n0042_cy<6>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<3> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<2> , 1'b0, 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<0> }),
    .O({_n0042[6], _n0042[5], _n0042[4], _n0042[3]}),
    .S({\packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_6_rt_18957 , \Madd__n0042_lut[5] , 
\packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_20_rt_18969 , \Madd__n0042_lut[3] })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y33" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd__n0042_lut<5>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(header[3]),
    .ADR3(header[19]),
    .O(\Madd__n0042_lut[5] )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y33" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_20_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(header[20]),
    .ADR5(1'b1),
    .O(\packet_sender/ip_header_checksum/Madd__n0042_cy<6>/packet_sender/header_checksum_input_20_rt_18969 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X14Y33" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<6>/wr2_flags<2>_78.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/wr2_flags<2>_78.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y33" ),
    .INIT ( 64'h55AA55AA55AA55AA ))
  \Madd__n0042_lut<3>  (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(header[3]),
    .ADR0(header[19]),
    .O(\Madd__n0042_lut[3] )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/ip_header_checksum/Madd__n0042_cy<10>_DMUX_Delay  (
    .I(_n0042[10]),
    .O(\_n0042<10>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/ip_header_checksum/Madd__n0042_cy<10>_CMUX_Delay  (
    .I(_n0042[9]),
    .O(\_n0042<9>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/ip_header_checksum/Madd__n0042_cy<10>_BMUX_Delay  (
    .I(_n0042[8]),
    .O(\_n0042<8>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/ip_header_checksum/Madd__n0042_cy<10>_AMUX_Delay  (
    .I(_n0042[7]),
    .O(\_n0042<7>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y34" ),
    .INIT ( 64'h0000FFFFFFFF0000 ))
  \Madd__n0042_lut<10>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(header[10]),
    .ADR5(header[10]),
    .O(\Madd__n0042_lut[10] )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X14Y34" ))
  \Madd__n0042_cy<10>  (
    .CI(\Madd__n0042_cy[6] ),
    .CYINIT(1'b0),
    .CO({\Madd__n0042_cy[10] , \NLW_Madd__n0042_cy<10>_CO[2]_UNCONNECTED , \NLW_Madd__n0042_cy<10>_CO[1]_UNCONNECTED , 
\NLW_Madd__n0042_cy<10>_CO[0]_UNCONNECTED }),
    .DI({\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<3> , 1'b0, 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<1> , 1'b0}),
    .O({_n0042[10], _n0042[9], _n0042[8], _n0042[7]}),
    .S({\Madd__n0042_lut[10] , \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_29_rt_18987 , 
\Madd__n0042_lut[8] , \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_31_rt_18994 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y34" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_29_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(header[25]),
    .ADR5(1'b1),
    .O(\packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_29_rt_18987 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X14Y34" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/wr2_flags<2>_80.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/wr2_flags<2>_80.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y34" ),
    .INIT ( 64'h00FFFF0000FFFF00 ))
  \Madd__n0042_lut<8>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR3(header[8]),
    .ADR4(header[10]),
    .O(\Madd__n0042_lut[8] )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y34" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_31_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(header[23]),
    .ADR5(1'b1),
    .O(\packet_sender/ip_header_checksum/Madd__n0042_cy<10>/packet_sender/header_checksum_input_31_rt_18994 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X14Y34" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/ip_header_checksum/Madd__n0042_cy<10>/wr2_flags<2>_79.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/wr2_flags<2>_79.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/ip_header_checksum/_n0042<12>/packet_sender/ip_header_checksum/_n0042<12>_BMUX_Delay  (
    .I(_n0042[12]),
    .O(\_n0042<12>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/_n0042<12>/packet_sender/ip_header_checksum/_n0042<12>_AMUX_Delay  (
    .I(_n0042[11]),
    .O(\_n0042<11>_0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X14Y35" ))
  \Madd__n0042_xor<12>  (
    .CI(\Madd__n0042_cy[10] ),
    .CYINIT(1'b0),
    .CO({\NLW_Madd__n0042_xor<12>_CO[3]_UNCONNECTED , \NLW_Madd__n0042_xor<12>_CO[2]_UNCONNECTED , \NLW_Madd__n0042_xor<12>_CO[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<12>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Madd__n0042_xor<12>_DI[3]_UNCONNECTED , \NLW_Madd__n0042_xor<12>_DI[2]_UNCONNECTED , \NLW_Madd__n0042_xor<12>_DI[1]_UNCONNECTED , 1'b0})
,
    .O({\NLW_Madd__n0042_xor<12>_O[3]_UNCONNECTED , \NLW_Madd__n0042_xor<12>_O[2]_UNCONNECTED , _n0042[12], _n0042[11]}),
    .S({\NLW_Madd__n0042_xor<12>_S[3]_UNCONNECTED , \NLW_Madd__n0042_xor<12>_S[2]_UNCONNECTED , 1'b0, 
\packet_sender/ip_header_checksum/_n0042<12>/packet_sender/header_checksum_input_29_rt })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y35" ),
    .INIT ( 64'h0000000000000000 ))
  \Madd__n0042_cy<12>_6.B6LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR5(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_6.B6LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y35" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \packet_sender/ip_header_checksum/_n0042<12>/packet_sender/header_checksum_input_29_rt.1  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(header[27]),
    .ADR5(1'b1),
    .O(\packet_sender/ip_header_checksum/_n0042<12>/packet_sender/header_checksum_input_29_rt )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X14Y35" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/ip_header_checksum/_n0042<12>/wr2_flags<2>_81.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/ip_header_checksum/_n0042<12>/wr2_flags<2>_81.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/ip_header_checksum/_n0042<2>/packet_sender/ip_header_checksum/_n0042<2>_CMUX_Delay  (
    .I(_n0042[2]),
    .O(\_n0042<2>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/_n0042<2>/packet_sender/ip_header_checksum/_n0042<2>_BMUX_Delay  (
    .I(_n0042[1]),
    .O(\_n0042<1>_0 )
  );
  X_BUF   \packet_sender/ip_header_checksum/_n0042<2>/packet_sender/ip_header_checksum/_n0042<2>_AMUX_Delay  (
    .I(_n0042[0]),
    .O(\_n0042<0>_0 )
  );
  X_ZERO #(
    .LOC ( "SLICE_X16Y34" ))
  \packet_sender/ip_header_checksum/_n0042<2>/ProtoComp199.CYINITGND  (
    .O(\packet_sender/ip_header_checksum/_n0042<2>/ProtoComp199.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X16Y34" ))
  \Madd__n0042_xor<2>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/ip_header_checksum/_n0042<2>/ProtoComp199.CYINITGND.0 ),
    .CO({\NLW_Madd__n0042_xor<2>_CO[3]_UNCONNECTED , \NLW_Madd__n0042_xor<2>_CO[2]_UNCONNECTED , \NLW_Madd__n0042_xor<2>_CO[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<2>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Madd__n0042_xor<2>_DI[3]_UNCONNECTED , \NLW_Madd__n0042_xor<2>_DI[2]_UNCONNECTED , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<1> , 
\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<0> }),
    .O({\NLW_Madd__n0042_xor<2>_O[3]_UNCONNECTED , _n0042[2], _n0042[1], _n0042[0]}),
    .S({\NLW_Madd__n0042_xor<2>_S[3]_UNCONNECTED , 1'b0, \Madd__n0042_lut[1] , \Madd__n0042_lut[0] })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X16Y34" ),
    .INIT ( 64'h0000000000000000 ))
  \Madd__n0042_cy<12>_3.C6LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR5(1'b1),
    .O(\NLW_Madd__n0042_cy<12>_3.C6LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X16Y34" ),
    .INIT ( 64'h00FF00FFFF00FF00 ))
  \Madd__n0042_lut<1>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(header[1]),
    .ADR5(header[17]),
    .O(\Madd__n0042_lut[1] )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X16Y34" ),
    .INIT ( 64'h5555AAAA5555AAAA ))
  \Madd__n0042_lut<0>  (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(header[0]),
    .ADR0(header[16]),
    .O(\Madd__n0042_lut[0] )
  );
  X_BUF   \packet_sender/ip_header_checksum/_n0042<13>/packet_sender/ip_header_checksum/_n0042<13>_AMUX_Delay  (
    .I(_n0042[13]),
    .O(\_n0042<13>_0 )
  );
  X_ZERO #(
    .LOC ( "SLICE_X12Y35" ))
  \packet_sender/ip_header_checksum/_n0042<13>/ProtoComp543.CYINITGND  (
    .O(\packet_sender/ip_header_checksum/_n0042<13>/ProtoComp543.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X12Y35" ))
  \Madd__n0042_xor<13>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/ip_header_checksum/_n0042<13>/ProtoComp543.CYINITGND.0 ),
    .CO({\NLW_Madd__n0042_xor<13>_CO[3]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_CO[2]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_CO[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<13>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Madd__n0042_xor<13>_DI[3]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_DI[2]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_DI[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<13>_DI[0]_UNCONNECTED }),
    .O({\NLW_Madd__n0042_xor<13>_O[3]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_O[2]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_O[1]_UNCONNECTED , _n0042[13]
}),
    .S({\NLW_Madd__n0042_xor<13>_S[3]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_S[2]_UNCONNECTED , \NLW_Madd__n0042_xor<13>_S[1]_UNCONNECTED , 
\packet_sender/ip_header_checksum/_n0042<13>/packet_sender/header_checksum_input_29_rt })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y35" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \packet_sender/ip_header_checksum/_n0042<13>/packet_sender/header_checksum_input_29_rt.2  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(header[29]),
    .O(\packet_sender/ip_header_checksum/_n0042<13>/packet_sender/header_checksum_input_29_rt )
  );
  X_BUF   \packet_sender/ip_header_checksum/_n0042<15>/packet_sender/ip_header_checksum/_n0042<15>_AMUX_Delay  (
    .I(_n0042[15]),
    .O(\_n0042<15>_0 )
  );
  X_ZERO #(
    .LOC ( "SLICE_X12Y34" ))
  \packet_sender/ip_header_checksum/_n0042<15>/ProtoComp543.CYINITGND.2  (
    .O(\packet_sender/ip_header_checksum/_n0042<15>/ProtoComp543.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X12Y34" ))
  \Madd__n0042_xor<15>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/ip_header_checksum/_n0042<15>/ProtoComp543.CYINITGND.0 ),
    .CO({\NLW_Madd__n0042_xor<15>_CO[3]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_CO[2]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_CO[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<15>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Madd__n0042_xor<15>_DI[3]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_DI[2]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_DI[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<15>_DI[0]_UNCONNECTED }),
    .O({\NLW_Madd__n0042_xor<15>_O[3]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_O[2]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_O[1]_UNCONNECTED , _n0042[15]
}),
    .S({\NLW_Madd__n0042_xor<15>_S[3]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_S[2]_UNCONNECTED , \NLW_Madd__n0042_xor<15>_S[1]_UNCONNECTED , 
\packet_sender/ip_header_checksum/_n0042<15>/packet_sender/header_checksum_input_31_rt })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X12Y34" ),
    .INIT ( 64'hAAAAAAAAAAAAAAAA ))
  \packet_sender/ip_header_checksum/_n0042<15>/packet_sender/header_checksum_input_31_rt.1  (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR0(header[31]),
    .O(\packet_sender/ip_header_checksum/_n0042<15>/packet_sender/header_checksum_input_31_rt )
  );
  X_BUF   \packet_sender/ip_header_checksum/header_count<1>/packet_sender/ip_header_checksum/header_count<1>_CMUX_Delay  (
    .I(header_count[2]),
    .O(\header_count<2>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X11Y33" ),
    .INIT ( 64'hBFBFBFBFBFBFBFBF ))
  n0000_inv1 (
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(header_count[0]),
    .ADR0(header_count[1]),
    .ADR1(\header_count<2>_0 ),
    .ADR5(1'b1),
    .O(n0000_inv)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X11Y33" ),
    .INIT ( 32'h6C6C6C6C ))
  \Mcount_header_count_xor<2>11  (
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(header_count[0]),
    .ADR0(header_count[1]),
    .ADR1(\header_count<2>_0 ),
    .O(Result[2])
  );
  X_SFF #(
    .LOC ( "SLICE_X11Y33" ),
    .INIT ( 1'b0 ))
  header_count_2 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/header_count_2/CLK ),
    .I(Result[2]),
    .O(header_count[2]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_SFF #(
    .LOC ( "SLICE_X11Y33" ),
    .INIT ( 1'b0 ))
  header_count_1 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/header_count_1/CLK ),
    .I(Result[1]),
    .O(header_count[1]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X11Y33" ),
    .INIT ( 64'h5555AAAA5555AAAA ))
  \Mcount_header_count_xor<1>11  (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(header_count[1]),
    .ADR3(1'b1),
    .ADR0(header_count[0]),
    .O(Result[1])
  );
  X_SFF #(
    .LOC ( "SLICE_X11Y33" ),
    .INIT ( 1'b0 ))
  header_count_0 (
    .CE(n0000_inv),
    .CLK(\NlwBufferSignal_packet_sender/ip_header_checksum/header_count_0/CLK ),
    .I(Result[0]),
    .O(header_count[0]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X11Y33" ),
    .INIT ( 64'h00000000FFFFFFFF ))
  \Mcount_header_count_xor<0>11_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(header_count[0]),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .O(Result[0])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X13Y38" ),
    .INIT ( 64'h5555555555555555 ))
  \checksum<10>1_INV_0  (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR0(\checksum_int[31]_checksum_int[15]_add_11_OUT<10>_0 ),
    .O(checksum[10])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X13Y38" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<12>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<12>_0 ),
    .O(checksum[12])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X13Y39" ),
    .INIT ( 64'h0F0F0F0F0F0F0F0F ))
  \checksum<9>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(\checksum_int[31]_checksum_int[15]_add_11_OUT<9>_0 ),
    .O(checksum[9])
  );
  X_BUF   \packet_sender/header_checksum<3>/packet_sender/header_checksum<3>_AMUX_Delay  (
    .I(_n0042[14]),
    .O(\_n0042<14>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y36" ),
    .INIT ( 64'h00000000FFFFFFFF ))
  \checksum<3>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR5(\checksum_int[31]_checksum_int[15]_add_11_OUT<3>_0 ),
    .O(checksum[3])
  );
  X_ZERO #(
    .LOC ( "SLICE_X14Y36" ))
  \packet_sender/header_checksum<3>/ProtoComp626.CYINITGND  (
    .O(\packet_sender/header_checksum<3>/ProtoComp626.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X14Y36" ))
  \Madd__n0042_xor<14>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/header_checksum<3>/ProtoComp626.CYINITGND.0 ),
    .CO({\NLW_Madd__n0042_xor<14>_CO[3]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_CO[2]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_CO[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<14>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Madd__n0042_xor<14>_DI[3]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_DI[2]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_DI[1]_UNCONNECTED , 
\NLW_Madd__n0042_xor<14>_DI[0]_UNCONNECTED }),
    .O({\NLW_Madd__n0042_xor<14>_O[3]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_O[2]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_O[1]_UNCONNECTED , _n0042[14]
}),
    .S({\NLW_Madd__n0042_xor<14>_S[3]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_S[2]_UNCONNECTED , \NLW_Madd__n0042_xor<14>_S[1]_UNCONNECTED , 
\packet_sender/header_checksum<3>/packet_sender/header_checksum_input_30_rt_19057 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y36" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<2>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<2>_0 ),
    .O(checksum[2])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y36" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \packet_sender/header_checksum<3>/packet_sender/header_checksum_input_30_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(header[30]),
    .O(\packet_sender/header_checksum<3>/packet_sender/header_checksum_input_30_rt_19057 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y37" ),
    .INIT ( 64'h00000000FFFFFFFF ))
  \checksum<5>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR5(\checksum_int[31]_checksum_int[15]_add_11_OUT<5>_0 ),
    .O(checksum[5])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y37" ),
    .INIT ( 64'h0F0F0F0F0F0F0F0F ))
  \checksum<4>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(\checksum_int[31]_checksum_int[15]_add_11_OUT<4>_0 ),
    .O(checksum[4])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y38" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<11>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<11>_0 ),
    .O(checksum[11])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y38" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<6>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<6>_0 ),
    .O(checksum[6])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y39" ),
    .INIT ( 64'h0F0F0F0F0F0F0F0F ))
  \checksum<8>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(\checksum_int[31]_checksum_int[15]_add_11_OUT<8>_0 ),
    .O(checksum[8])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X14Y42" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<0>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<0>_0 ),
    .O(checksum[0])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X15Y36" ),
    .INIT ( 64'h00000000FFFFFFFF ))
  \checksum<14>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR5(\checksum_int[31]_checksum_int[15]_add_11_OUT<14>_0 ),
    .O(checksum[14])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X15Y37" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<7>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<7>_0 ),
    .O(checksum[7])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X15Y38" ),
    .INIT ( 64'h0F0F0F0F0F0F0F0F ))
  \checksum<13>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(\checksum_int[31]_checksum_int[15]_add_11_OUT<13>_0 ),
    .O(checksum[13])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X15Y42" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<1>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<1>_0 ),
    .O(checksum[1])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X16Y38" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \checksum<15>1_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR3(\checksum_int[31]_checksum_int[15]_add_11_OUT<15>_0 ),
    .O(checksum[15])
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<0>  (
    .I(checksum_int[16]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<1>  (
    .I(checksum_int[17]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<2>  (
    .I(checksum_int[18]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<3>  (
    .I(checksum_int[19]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<3>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<0>  (
    .I(checksum_int[20]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<1>  (
    .I(checksum_int[21]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<2>  (
    .I(checksum_int[22]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<3>  (
    .I(checksum_int[23]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<7>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<0>  (
    .I(checksum_int[24]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<1>  (
    .I(checksum_int[25]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<2>  (
    .I(checksum_int[26]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<3>  (
    .I(checksum_int[27]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_cy<11>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<0>  (
    .I(checksum_int[28]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<1>  (
    .I(checksum_int[29]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<2>  (
    .I(checksum_int[30]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd_checksum_int[31]_checksum_int[15]_add_11_OUT_xor<15>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<0>  (
    .I(checksum_int[0]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<1>  (
    .I(checksum_int[1]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<2>  (
    .I(checksum_int[2]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<3>  (
    .I(checksum_int[3]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<3>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_7/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_7/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_6/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_6/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<0>  (
    .I(checksum_int[4]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<1>  (
    .I(checksum_int[5]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<2>  (
    .I(checksum_int[6]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<3>  (
    .I(checksum_int[7]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<7>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_5/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_5/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_11/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_11/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_10/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_10/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<0>  (
    .I(checksum_int[8]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<1>  (
    .I(checksum_int[9]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<2>  (
    .I(checksum_int[10]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<3>  (
    .I(checksum_int[11]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<11>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_9/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_9/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_8/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_8/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_15/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_15/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_14/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_14/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<0>  (
    .I(checksum_int[12]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<1>  (
    .I(checksum_int[13]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<2>  (
    .I(checksum_int[14]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<3>  (
    .I(checksum_int[15]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<15>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_13/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_13/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_12/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_12/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_19/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_19/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_18/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_18/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<19>/DI<0>  (
    .I(checksum_int[16]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Maccum_checksum_int_cy<19>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_17/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_17/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_16/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_16/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_23/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_23/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_22/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_22/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_21/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_21/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_20/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_20/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_27/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_27/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_26/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_26/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_25/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_25/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_24/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_24/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_31/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_31/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_30/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_30/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_29/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_29/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/checksum_int_28/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/checksum_int_28/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<0>  (
    .I(header[3]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<2>  (
    .I(header[3]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<2> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<3>  (
    .I(header[6]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<6>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<1>  (
    .I(header[8]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<3>  (
    .I(header[10]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_cy<10>/DI<3> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<0>  (
    .I(header[0]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<0> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<1>  (
    .I(header[1]),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/Madd__n0042_xor<2>/DI<1> )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/header_count_2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/header_count_2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/header_count_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/header_count_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/ip_header_checksum/header_count_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/ip_header_checksum/header_count_0/CLK )
  );
  X_ZERO   NlwBlock_ip_header_checksum_GND (
    .O(GND)
  );
endmodule

module packet_sender (
  clk, reset, start, wr_dst_rdy_i, wr_src_rdy_o, wr_flags_o, wr_data_o
);
  input clk;
  input reset;
  input start;
  input wr_dst_rdy_i;
  output wr_src_rdy_o;
  output [3 : 0] wr_flags_o;
  output [31 : 0] wr_data_o;
  wire NlwRenamedSig_OI_wr_src_rdy_o;
  wire header_checksum_input_31_21065;
  wire header_checksum_input_30_21066;
  wire header_checksum_input_29_21067;
  wire header_checksum_input_26_21068;
  wire header_checksum_input_20_21069;
  wire header_checksum_input_8_21070;
  wire header_checksum_input_6_21071;
  wire header_checksum_input_3_21072;
  wire header_checksum_input_1_21073;
  wire header_checksum_input_0_21074;
  wire header_checksum_reset_21075;
  wire \state[4]_GND_55_o_Mux_197_o ;
  wire state_FSM_FFd4_21099;
  wire state_FSM_FFd5_21100;
  wire state_FSM_FFd1_21101;
  wire state_FSM_FFd2_21102;
  wire state_FSM_FFd3_21103;
  wire Mmux_nxt_wr_data_o331_0;
  wire _n0658_inv;
  wire \state_FSM_FFd2-In_21106 ;
  wire N5;
  wire GND_53_o_GND_53_o_OR_291_o;
  wire Mmux_nxt_wr_data_o49_0;
  wire Mmux_nxt_wr_data_o191_21111;
  wire Mmux_nxt_wr_data_o496;
  wire N20;
  wire N21_0;
  wire Mmux_nxt_wr_data_o431_21116;
  wire Mmux_nxt_wr_data_o432_0;
  wire \state_FSM_FFd5-In ;
  wire \state_FSM_FFd5-In2_0 ;
  wire \state_FSM_FFd5-In3_21121 ;
  wire N17;
  wire N18_0;
  wire \pkt_offset[4] ;
  wire Mmux_nxt_wr_data_o25_0;
  wire N32;
  wire \pkt_offset<3>_0 ;
  wire N33_0;
  wire \seqn[15]_GND_53_o_LessThan_80_o21 ;
  wire N12_0;
  wire Mmux_nxt_wr_data_o47_0;
  wire Mmux_nxt_wr_data_o311;
  wire Mmux_nxt_wr_data_o312_21140;
  wire _n0486_inv;
  wire \pkt_offset[15]_GND_53_o_add_141_OUT<3>_0 ;
  wire \pkt_offset[15]_GND_53_o_add_141_OUT<4>_0 ;
  wire N14;
  wire N15_0;
  wire Mmux_nxt_wr_data_o462_21147;
  wire Mmux_nxt_wr_data_o351_0;
  wire \Result<0>3_0 ;
  wire \Result<1>3_0 ;
  wire \Result<2>3_0 ;
  wire \Mcount_timest_cy[3] ;
  wire \Result<3>3_0 ;
  wire \Result<4>3_0 ;
  wire \Result<5>3_0 ;
  wire \Result<6>3_0 ;
  wire \Mcount_timest_cy[7] ;
  wire \Result<7>3_0 ;
  wire \Result<8>3_0 ;
  wire \Result<9>3_0 ;
  wire \Result<10>3_0 ;
  wire \Mcount_timest_cy[11] ;
  wire \Result<11>3_0 ;
  wire \Result<12>3_0 ;
  wire \Result<13>3_0 ;
  wire \Result<14>3_0 ;
  wire \Mcount_timest_cy[15] ;
  wire \Result<15>3_0 ;
  wire \Result<16>_0 ;
  wire \Result<17>_0 ;
  wire \Result<18>_0 ;
  wire \Mcount_timest_cy[19] ;
  wire \Result<19>_0 ;
  wire \Result<20>_0 ;
  wire \Result<21>_0 ;
  wire \Result<22>_0 ;
  wire \Mcount_timest_cy[23] ;
  wire \Result<23>_0 ;
  wire \Result<24>_0 ;
  wire \Result<25>_0 ;
  wire \Result<26>_0 ;
  wire \Mcount_timest_cy[27] ;
  wire \Result<27>_0 ;
  wire \Result<28>_0 ;
  wire \Result<29>_0 ;
  wire \Result<30>_0 ;
  wire \Result<31>_0 ;
  wire \Result<0>1_0 ;
  wire \Result<1>1_0 ;
  wire \Result<2>1_0 ;
  wire \Mcount_line_cnt_cy[3] ;
  wire \Result<3>1_0 ;
  wire \Result<4>1_0 ;
  wire \Result<5>1_0 ;
  wire \Result<6>1_0 ;
  wire \Mcount_line_cnt_cy[7] ;
  wire \Result<7>1_0 ;
  wire \Result<8>1_0 ;
  wire \Result<9>1_0 ;
  wire _n0780_inv;
  wire \Mcount_packet_size_count_cy[3] ;
  wire \Mcount_packet_size_count_cy[7] ;
  wire \Mcount_packet_size_count_cy[11] ;
  wire \Result<0>2_0 ;
  wire \Result<1>2_0 ;
  wire \Result<2>2_0 ;
  wire \Mcount_seqn_cy[3] ;
  wire \Result<3>2_0 ;
  wire \Result<4>2_0 ;
  wire \Result<5>2_0 ;
  wire \Result<6>2_0 ;
  wire \Mcount_seqn_cy[7] ;
  wire \Result<7>2_0 ;
  wire \Result<8>2_0 ;
  wire \Result<9>2_0 ;
  wire \Result<10>2_0 ;
  wire \Mcount_seqn_cy[11] ;
  wire \Result<11>2_0 ;
  wire \Result<12>2_0 ;
  wire \Result<13>2_0 ;
  wire \Result<14>2_0 ;
  wire \Result<15>2_0 ;
  wire \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_21279 ;
  wire \pkt_offset[8] ;
  wire \pkt_offset[15]_GND_53_o_add_141_OUT<8>_0 ;
  wire \packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ;
  wire \seqn[15]_GND_53_o_LessThan_44_o2 ;
  wire \state_FSM_FFd3-In_0 ;
  wire _n0495_inv;
  wire Reset_OR_DriverANDClockEnable161_21286;
  wire _n0701_inv1;
  wire GND_53_o_GND_53_o_equal_156_o;
  wire Reset_OR_DriverANDClockEnable16;
  wire Mmux_nxt_wr_data_o38;
  wire Mmux_nxt_wr_data_o221_21291;
  wire Mmux_nxt_wr_data_o401_0;
  wire Mmux_nxt_wr_data_o361_21293;
  wire Mmux_nxt_wr_data_o171_21294;
  wire Mmux_nxt_wr_data_o42;
  wire Mmux_nxt_wr_data_o192_21296;
  wire Mmux_nxt_wr_data_o291_21297;
  wire N29;
  wire N30;
  wire Mmux_nxt_wr_data_o451_21300;
  wire N01;
  wire \state_FSM_FFd1-In ;
  wire \state_FSM_FFd4-In ;
  wire GND_53_o_GND_53_o_OR_291_o1_21305;
  wire state_FSM_FFd5_1_21306;
  wire state_FSM_FFd2_1_21307;
  wire state_FSM_FFd4_1_21308;
  wire state_FSM_FFd3_1_21309;
  wire \seqn[15]_GND_53_o_LessThan_44_o11 ;
  wire \state[4]_header_checksum_input[8]_Mux_242_o_0 ;
  wire \state[4]_header_checksum_input[6]_Mux_246_o_0 ;
  wire \state[4]_header_checksum_input[29]_Mux_200_o_0 ;
  wire Mmux_nxt_wr_data_o29;
  wire N26;
  wire N27_0;
  wire N24_0;
  wire N23;
  wire Mmux_nxt_wr_data_o34;
  wire Mmux_nxt_wr_data_o381;
  wire Mmux_nxt_wr_data_o36;
  wire Mmux_nxt_wr_data_o22;
  wire Mmux_nxt_wr_data_o19;
  wire GND_53_o_GND_53_o_OR_291_o2_21324;
  wire GND_53_o_GND_53_o_OR_291_o3_21325;
  wire GND_53_o_GND_53_o_OR_291_o4_0;
  wire N2;
  wire Mmux_nxt_wr_data_o272_21328;
  wire Mmux_nxt_wr_data_o271_0;
  wire \state_FSM_FFd5-In1_0 ;
  wire N39;
  wire Mmux_nxt_wr_data_o15;
  wire Mmux_nxt_wr_data_o17;
  wire N4;
  wire N6_0;
  wire state_FSM_FFd1_1_21336;
  wire Mmux_nxt_wr_data_o40;
  wire _n0701_inv_0;
  wire Mmux_nxt_wr_data_o41;
  wire Mmux_nxt_wr_data_o1;
  wire Mmux_nxt_wr_data_o23;
  wire Mmux_nxt_wr_data_o45;
  wire Mmux_nxt_wr_data_o421_21343;
  wire Mmux_nxt_wr_data_o43;
  wire N37_0;
  wire Mmux_nxt_wr_data_o51;
  wire N41_0;
  wire Mmux_nxt_wr_data_o461;
  wire Mmux_nxt_wr_data_o54;
  wire \timest<1>_rt_19209 ;
  wire \timest<2>_rt_19206 ;
  wire \Result<0>3 ;
  wire \Result<1>3 ;
  wire \Result<2>3 ;
  wire \Result<3>3 ;
  wire \packet_sender/Mcount_timest_cy<3>/ProtoComp145.CYINITGND.0 ;
  wire \timest<3>_rt_19197 ;
  wire \timest<4>_rt_19234 ;
  wire \timest<5>_rt_19231 ;
  wire \timest<6>_rt_19228 ;
  wire \Result<4>3 ;
  wire \Result<5>3 ;
  wire \Result<6>3 ;
  wire \Result<7>3 ;
  wire \timest<7>_rt_19219 ;
  wire \timest<8>_rt_19256 ;
  wire \timest<9>_rt_19253 ;
  wire \timest<10>_rt_19250 ;
  wire \Result<8>3 ;
  wire \Result<9>3 ;
  wire \Result<10>3 ;
  wire \Result<11>3 ;
  wire \timest<11>_rt_19241 ;
  wire \timest<12>_rt_19278 ;
  wire \timest<13>_rt_19275 ;
  wire \timest<14>_rt_19272 ;
  wire \Result<12>3 ;
  wire \Result<13>3 ;
  wire \Result<14>3 ;
  wire \Result<15>3 ;
  wire \timest<15>_rt_19263 ;
  wire \timest<16>_rt_19300 ;
  wire \timest<17>_rt_19297 ;
  wire \timest<18>_rt_19294 ;
  wire \timest<19>_rt_19285 ;
  wire \timest<20>_rt_19322 ;
  wire \timest<21>_rt_19319 ;
  wire \timest<22>_rt_19316 ;
  wire \timest<23>_rt_19307 ;
  wire \timest<24>_rt_19344 ;
  wire \timest<25>_rt_19341 ;
  wire \timest<26>_rt_19338 ;
  wire \timest<27>_rt_19329 ;
  wire \timest<28>_rt_19364 ;
  wire \timest<29>_rt_19361 ;
  wire \timest<30>_rt_19358 ;
  wire \timest<31>_rt_19351 ;
  wire \line_cnt<1>_rt_19382 ;
  wire \line_cnt<2>_rt_19379 ;
  wire \Result<0>1 ;
  wire \Result<1>1 ;
  wire \Result<2>1 ;
  wire \Result<3>1 ;
  wire \packet_sender/Mcount_line_cnt_cy<3>/ProtoComp145.CYINITGND.0 ;
  wire \line_cnt<3>_rt_19370 ;
  wire \line_cnt<4>_rt_19407 ;
  wire \line_cnt<5>_rt_19404 ;
  wire \line_cnt<6>_rt_19401 ;
  wire \Result<4>1 ;
  wire \Result<5>1 ;
  wire \Result<6>1 ;
  wire \Result<7>1 ;
  wire \line_cnt<7>_rt_19392 ;
  wire \line_cnt<8>_rt_19419 ;
  wire \line_cnt<9>_rt_19417 ;
  wire \Result<8>1 ;
  wire \Result<9>1 ;
  wire Mcount_packet_size_count;
  wire Mcount_packet_size_count1;
  wire Mcount_packet_size_count2;
  wire Mcount_packet_size_count3;
  wire Mcount_packet_size_count4;
  wire Mcount_packet_size_count5;
  wire Mcount_packet_size_count6;
  wire Mcount_packet_size_count7;
  wire Mcount_packet_size_count8;
  wire Mcount_packet_size_count9;
  wire Mcount_packet_size_count10;
  wire Mcount_packet_size_count11;
  wire Mcount_packet_size_count12;
  wire Mcount_packet_size_count13;
  wire \seqn<1>_rt_19550 ;
  wire \seqn<2>_rt_19547 ;
  wire \Result<0>2 ;
  wire \Result<1>2 ;
  wire \Result<2>2 ;
  wire \Result<3>2 ;
  wire \packet_sender/Mcount_seqn_cy<3>/ProtoComp145.CYINITGND.0 ;
  wire \seqn<3>_rt_19538 ;
  wire \seqn<4>_rt_19575 ;
  wire \seqn<5>_rt_19572 ;
  wire \seqn<6>_rt_19569 ;
  wire \Result<4>2 ;
  wire \Result<5>2 ;
  wire \Result<6>2 ;
  wire \Result<7>2 ;
  wire \seqn<7>_rt_19560 ;
  wire \seqn<8>_rt_19597 ;
  wire \seqn<9>_rt_19594 ;
  wire \seqn<10>_rt_19591 ;
  wire \Result<8>2 ;
  wire \Result<9>2 ;
  wire \Result<10>2 ;
  wire \Result<11>2 ;
  wire \seqn<11>_rt_19582 ;
  wire \seqn<12>_rt_19617 ;
  wire \seqn<13>_rt_19614 ;
  wire \seqn<14>_rt_19611 ;
  wire \Result<12>2 ;
  wire \Result<13>2 ;
  wire \Result<14>2 ;
  wire \Result<15>2 ;
  wire \seqn<15>_rt_19604 ;
  wire \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<3> ;
  wire \pkt_offset<4>_rt_19633 ;
  wire \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<5> ;
  wire \pkt_offset[15]_GND_53_o_add_141_OUT<3> ;
  wire \pkt_offset[15]_GND_53_o_add_141_OUT<4> ;
  wire \packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/ProtoComp179.CYINITGND.0 ;
  wire \pkt_offset<4>_rt_pack_3 ;
  wire \state[4]_header_checksum_input[8]_Mux_242_o ;
  wire \state[4]_header_checksum_input[26]_Mux_206_o ;
  wire Mmux_nxt_wr_data_o351;
  wire \state[4]_header_checksum_input[20]_Mux_218_o ;
  wire Mmux_nxt_wr_data_o25;
  wire \state[4]_header_checksum_input[1]_Mux_256_o ;
  wire \state[4]_header_checksum_input[6]_Mux_246_o ;
  wire \state[4]_header_checksum_input[3]_Mux_252_o ;
  wire \state[4]_header_checksum_input[29]_Mux_200_o ;
  wire \state[4]_header_checksum_input[31]_Mux_196_o ;
  wire Mmux_nxt_wr_data_o47;
  wire \state[4]_header_checksum_input[30]_Mux_198_o ;
  wire N27;
  wire N18;
  wire N46;
  wire N45;
  wire N33;
  wire \pkt_offset[3] ;
  wire \pkt_offset[15]_pkt_offset[15]_mux_151_OUT<3> ;
  wire \pkt_offset[15]_pkt_offset[15]_mux_151_OUT<4> ;
  wire \pkt_offset[15]_pkt_offset[15]_mux_151_OUT<8> ;
  wire \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<7> ;
  wire \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<8> ;
  wire N50;
  wire \pkt_offset[15]_GND_53_o_add_141_OUT<8> ;
  wire N49;
  wire Mmux_nxt_wr_data_o331;
  wire N24;
  wire N21;
  wire N15;
  wire GND_53_o_GND_53_o_OR_291_o4_20009;
  wire N52;
  wire N51;
  wire header_checksum_reset_rstpot_20059;
  wire N48;
  wire N47;
  wire Mmux_nxt_wr_data_o271;
  wire Mmux_nxt_wr_data_o401_20082;
  wire \state_FSM_FFd5-In1_20116 ;
  wire N36;
  wire N35;
  wire \state_FSM_FFd3-In_20150 ;
  wire N12;
  wire N6;
  wire seqn_0_rstpot_20280;
  wire Mmux_nxt_wr_data_o432_20304;
  wire N53;
  wire _n0701_inv;
  wire Mmux_nxt_wr_data_o49;
  wire N54;
  wire \state_FSM_FFd5-In2_20311 ;
  wire timest_11_dpot_20388;
  wire timest_12_dpot_20382;
  wire timest_13_dpot_20376;
  wire timest_14_dpot_20368;
  wire N44;
  wire N43;
  wire seqn_12_rstpot_20476;
  wire seqn_13_rstpot_20470;
  wire timest_3_dpot_20527;
  wire timest_4_dpot_20521;
  wire timest_5_dpot_20515;
  wire timest_6_dpot_20507;
  wire N37;
  wire wr_src_rdy_o_rstpot_20583;
  wire N41;
  wire timest_27_dpot_20707;
  wire timest_28_dpot_20701;
  wire timest_29_dpot_20695;
  wire timest_30_dpot_20687;
  wire seqn_2_rstpot_20730;
  wire seqn_3_rstpot_20724;
  wire seqn_1_rstpot_20718;
  wire seqn_7_rstpot_20751;
  wire seqn_4_rstpot_20745;
  wire seqn_5_rstpot_20739;
  wire seqn_6_rstpot_20732;
  wire seqn_11_rstpot_20776;
  wire seqn_8_rstpot_20770;
  wire seqn_9_rstpot_20764;
  wire seqn_10_rstpot_20757;
  wire seqn_14_rstpot_20789;
  wire seqn_15_rstpot_20783;
  wire timest_2_dpot_20844;
  wire timest_0_dpot_20837;
  wire timest_1_dpot_20824;
  wire timest_7_dpot_20868;
  wire timest_8_dpot_20862;
  wire timest_9_dpot_20856;
  wire timest_10_dpot_20848;
  wire timest_15_dpot_20895;
  wire timest_16_dpot_20889;
  wire timest_17_dpot_20883;
  wire timest_18_dpot_20875;
  wire timest_19_dpot_20922;
  wire timest_20_dpot_20916;
  wire timest_21_dpot_20910;
  wire timest_22_dpot_20902;
  wire timest_23_dpot_20949;
  wire timest_24_dpot_20943;
  wire timest_25_dpot_20937;
  wire timest_26_dpot_20929;
  wire timest_31_dpot_20958;
  wire line_cnt_0_rstpot_20983;
  wire line_cnt_1_rstpot_20977;
  wire line_cnt_2_rstpot_20971;
  wire line_cnt_3_rstpot_20964;
  wire line_cnt_4_rstpot_21008;
  wire line_cnt_5_rstpot_21002;
  wire line_cnt_6_rstpot_20996;
  wire line_cnt_7_rstpot_20989;
  wire line_cnt_9_rstpot_21021;
  wire line_cnt_8_rstpot_21014;
  wire \NLW_ip_header_checksum_header<28>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<22>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<18>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<15>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<14>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<13>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<12>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<11>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<9>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<7>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<4>_UNCONNECTED ;
  wire \NLW_ip_header_checksum_header<2>_UNCONNECTED ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_3/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_2/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_1/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_0/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_7/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_6/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_5/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_4/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_11/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_10/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_9/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_8/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_13/CLK ;
  wire \NlwBufferSignal_packet_sender/packet_size_count_12/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_20/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_26/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_8/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_8/IN ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_3/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_6/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_6/IN ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_1/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_0/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_30/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_31/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_29/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_input_29/IN ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_26/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_23/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_22/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_20/CLK ;
  wire \NlwBufferSignal_packet_sender/pkt_offset_8/CLK ;
  wire \NlwBufferSignal_packet_sender/pkt_offset_4/CLK ;
  wire \NlwBufferSignal_packet_sender/pkt_offset_3/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_25/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_24/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_28/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_27/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_19/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_18/CLK ;
  wire \NlwBufferSignal_packet_sender/header_checksum_reset/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_21/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd5/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd4/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd5_1/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd5_1/IN ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd3/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd3/IN ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd2/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd2/IN ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd1/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd3_1/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd3_1/IN ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_17/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_16/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd4_1/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd4_1/IN ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd2_1/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd1_1/CLK ;
  wire \NlwBufferSignal_packet_sender/state_FSM_FFd1_1/IN ;
  wire \NlwBufferSignal_packet_sender/seqn_0/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_29/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_flags_o_1/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_flags_o_0/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_2/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_14/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_13/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_12/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_11/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_1/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_0/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_3/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_4/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_30/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_13/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_12/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_31/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_6/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_5/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_4/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_3/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_8/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_7/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_src_rdy_o/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_6/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_5/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_11/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_10/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_9/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_15/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_14/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_13/CLK ;
  wire \NlwBufferSignal_packet_sender/wr_data_o_12/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_30/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_29/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_28/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_27/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_2/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_1/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_3/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_6/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_5/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_4/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_7/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_10/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_9/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_8/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_11/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_15/CLK ;
  wire \NlwBufferSignal_packet_sender/seqn_14/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_2/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_1/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_0/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_10/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_9/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_8/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_7/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_18/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_17/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_16/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_15/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_22/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_21/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_20/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_19/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_26/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_25/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_24/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_23/CLK ;
  wire \NlwBufferSignal_packet_sender/timest_31/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_3/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_2/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_1/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_0/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_7/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_6/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_5/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_4/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_9/CLK ;
  wire \NlwBufferSignal_packet_sender/line_cnt_8/CLK ;
  wire \NLW_packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_24.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<3>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<3>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<3>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_23.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_22.B5LUT_O_UNCONNECTED ;
  wire \NLW_N0_4.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_28.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<7>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<7>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<7>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_27.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_26.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_25.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_32.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<11>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<11>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<11>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_31.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_30.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_29.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_36.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<15>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<15>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<15>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_35.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_34.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_33.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_40.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<19>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<19>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<19>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_39.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_38.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_37.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_44.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<23>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<23>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<23>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_43.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_42.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_41.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_48.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<27>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<27>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_cy<27>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_47.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_46.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_45.A5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_timest_xor<31>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_timest_xor<31>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_timest_xor<31>_CO[2]_UNCONNECTED ;
  wire \NLW_Mcount_timest_xor<31>_CO[3]_UNCONNECTED ;
  wire \NLW_Mcount_timest_xor<31>_DI[3]_UNCONNECTED ;
  wire \NLW_packet_sender/Result<31>/wr2_flags<2>_51.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Result<31>/wr2_flags<2>_50.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Result<31>/wr2_flags<2>_49.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_56.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_cy<3>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_cy<3>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_cy<3>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_55.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_54.B5LUT_O_UNCONNECTED ;
  wire \NLW_N0_5.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_60.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_cy<7>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_cy<7>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_cy<7>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_59.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_58.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_57.A5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_CO[2]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_CO[3]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_DI[1]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_DI[2]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_DI[3]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_O[2]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_O[3]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_S[2]_UNCONNECTED ;
  wire \NLW_Mcount_line_cnt_xor<9>_S[3]_UNCONNECTED ;
  wire \NLW_packet_sender/Result<9>1/wr2_flags<2>_61.A5LUT_O_UNCONNECTED ;
  wire GND;
  wire \NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_6.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<3>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<3>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<3>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_5.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_4.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_3.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_10.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<7>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<7>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<7>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_9.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_8.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_7.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_14.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<11>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<11>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_cy<11>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_13.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_12.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_11.A5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_CO[2]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_CO[3]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_DI[1]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_DI[2]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_DI[3]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_O[2]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_O[3]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_S[2]_UNCONNECTED ;
  wire \NLW_Mcount_packet_size_count_xor<13>_S[3]_UNCONNECTED ;
  wire \NLW_packet_sender/packet_size_count<13>/wr2_flags<2>_15.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_66.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<3>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<3>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<3>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_65.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_64.B5LUT_O_UNCONNECTED ;
  wire \NLW_N0_6.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_70.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<7>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<7>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<7>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_69.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_68.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_67.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_74.D5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<11>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<11>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_cy<11>_CO[2]_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_73.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_72.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_71.A5LUT_O_UNCONNECTED ;
  wire \NLW_Mcount_seqn_xor<15>_CO[0]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_xor<15>_CO[1]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_xor<15>_CO[2]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_xor<15>_CO[3]_UNCONNECTED ;
  wire \NLW_Mcount_seqn_xor<15>_DI[3]_UNCONNECTED ;
  wire \NLW_packet_sender/Result<15>2/wr2_flags<2>_77.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Result<15>2/wr2_flags<2>_76.B5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Result<15>2/wr2_flags<2>_75.A5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/wr2_flags<2>_19.D5LUT_O_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_O[2]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_O[3]_UNCONNECTED ;
  wire \NLW_N0_2.C5LUT_O_UNCONNECTED ;
  wire \NLW_packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/wr2_flags<2>_18.B5LUT_O_UNCONNECTED ;
  wire \NLW_N0.A5LUT_O_UNCONNECTED ;
  wire VCC;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[0]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[1]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[2]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[3]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_DI[1]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_DI[2]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_DI[3]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_O[0]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_O[2]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_O[3]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_S[2]_UNCONNECTED ;
  wire \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_S[3]_UNCONNECTED ;
  wire \NLW_N0_3.A5LUT_O_UNCONNECTED ;
  wire [15 : 0] header_checksum;
  wire [13 : 0] packet_size_count;
  wire [31 : 0] timest;
  wire [9 : 0] line_cnt;
  wire [15 : 0] seqn;
  wire [15 : 15] GND_53_o_GND_53_o_equal_156_o_0;
  wire [0 : 0] Mcount_timest_lut;
  wire [31 : 16] Result;
  wire [0 : 0] Mcount_line_cnt_lut;
  wire [13 : 0] Mcount_packet_size_count_lut;
  wire [0 : 0] Mcount_seqn_lut;
  wire [31 : 0] nxt_wr_data_o;
  wire [1 : 0] nxt_wr_flags_o;
  wire [2 : 2] \packet_sender/Mmux_nxt_wr_data_o361/wr2_data ;
  
  initial $sdf_annotate("./packet_sender.sdf");

  assign
    wr_src_rdy_o = NlwRenamedSig_OI_wr_src_rdy_o;
  ip_header_checksum   ip_header_checksum (
    .clk(clk),
    .reset(header_checksum_reset_21075),
    .header({header_checksum_input_31_21065, header_checksum_input_30_21066, header_checksum_input_29_21067, 
\NLW_ip_header_checksum_header<28>_UNCONNECTED , header_checksum_input_29_21067, header_checksum_input_26_21068, header_checksum_input_29_21067, 
header_checksum_input_26_21068, header_checksum_input_31_21065, \NLW_ip_header_checksum_header<22>_UNCONNECTED , header_checksum_input_31_21065, 
header_checksum_input_20_21069, header_checksum_input_31_21065, \NLW_ip_header_checksum_header<18>_UNCONNECTED , header_checksum_input_29_21067, 
header_checksum_input_20_21069, \NLW_ip_header_checksum_header<15>_UNCONNECTED , \NLW_ip_header_checksum_header<14>_UNCONNECTED , 
\NLW_ip_header_checksum_header<13>_UNCONNECTED , \NLW_ip_header_checksum_header<12>_UNCONNECTED , \NLW_ip_header_checksum_header<11>_UNCONNECTED , 
header_checksum_input_26_21068, \NLW_ip_header_checksum_header<9>_UNCONNECTED , header_checksum_input_8_21070, 
\NLW_ip_header_checksum_header<7>_UNCONNECTED , header_checksum_input_6_21071, header_checksum_input_3_21072, 
\NLW_ip_header_checksum_header<4>_UNCONNECTED , header_checksum_input_3_21072, \NLW_ip_header_checksum_header<2>_UNCONNECTED , 
header_checksum_input_1_21073, header_checksum_input_0_21074}),
    .checksum({header_checksum[15], header_checksum[14], header_checksum[13], header_checksum[12], header_checksum[11], header_checksum[10], 
header_checksum[9], header_checksum[8], header_checksum[7], header_checksum[6], header_checksum[5], header_checksum[4], header_checksum[3], 
header_checksum[2], header_checksum[1], header_checksum[0]})
  );
  X_BUF   \packet_sender/Mcount_timest_cy<3>/packet_sender/Mcount_timest_cy<3>_DMUX_Delay  (
    .I(\Result<3>3 ),
    .O(\Result<3>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<3>/packet_sender/Mcount_timest_cy<3>_CMUX_Delay  (
    .I(\Result<2>3 ),
    .O(\Result<2>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<3>/packet_sender/Mcount_timest_cy<3>_BMUX_Delay  (
    .I(\Result<1>3 ),
    .O(\Result<1>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<3>/packet_sender/Mcount_timest_cy<3>_AMUX_Delay  (
    .I(\Result<0>3 ),
    .O(\Result<0>3_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \timest<3>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(timest[3]),
    .ADR5(1'b1),
    .O(\timest<3>_rt_19197 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_24.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_24.D5LUT_O_UNCONNECTED )
  );
  X_ZERO #(
    .LOC ( "SLICE_X28Y44" ))
  \packet_sender/Mcount_timest_cy<3>/ProtoComp145.CYINITGND.1  (
    .O(\packet_sender/Mcount_timest_cy<3>/ProtoComp145.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y44" ))
  \Mcount_timest_cy<3>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/Mcount_timest_cy<3>/ProtoComp145.CYINITGND.0 ),
    .CO({\Mcount_timest_cy[3] , \NLW_Mcount_timest_cy<3>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_cy<3>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_cy<3>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b1}),
    .O({\Result<3>3 , \Result<2>3 , \Result<1>3 , \Result<0>3 }),
    .S({\timest<3>_rt_19197 , \timest<2>_rt_19206 , \timest<1>_rt_19209 , Mcount_timest_lut[0]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<2>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[2]),
    .ADR5(1'b1),
    .O(\timest<2>_rt_19206 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_23.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_23.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<1>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[1]),
    .ADR5(1'b1),
    .O(\timest<1>_rt_19209 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_22.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<3>/wr2_flags<2>_22.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 64'h3333333333333333 ))
  \Mcount_timest_lut<0>_INV_0  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(timest[0]),
    .ADR5(1'b1),
    .O(Mcount_timest_lut[0])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y44" ),
    .INIT ( 32'hFFFFFFFF ))
  \N0_4.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_N0_4.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<7>/packet_sender/Mcount_timest_cy<7>_DMUX_Delay  (
    .I(\Result<7>3 ),
    .O(\Result<7>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<7>/packet_sender/Mcount_timest_cy<7>_CMUX_Delay  (
    .I(\Result<6>3 ),
    .O(\Result<6>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<7>/packet_sender/Mcount_timest_cy<7>_BMUX_Delay  (
    .I(\Result<5>3 ),
    .O(\Result<5>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<7>/packet_sender/Mcount_timest_cy<7>_AMUX_Delay  (
    .I(\Result<4>3 ),
    .O(\Result<4>3_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<7>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[7]),
    .ADR5(1'b1),
    .O(\timest<7>_rt_19219 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_28.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_28.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y45" ))
  \Mcount_timest_cy<7>  (
    .CI(\Mcount_timest_cy[3] ),
    .CYINIT(1'b0),
    .CO({\Mcount_timest_cy[7] , \NLW_Mcount_timest_cy<7>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_cy<7>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_cy<7>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({\Result<7>3 , \Result<6>3 , \Result<5>3 , \Result<4>3 }),
    .S({\timest<7>_rt_19219 , \timest<6>_rt_19228 , \timest<5>_rt_19231 , \timest<4>_rt_19234 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \timest<6>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(timest[6]),
    .ADR5(1'b1),
    .O(\timest<6>_rt_19228 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_27.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_27.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<5>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[5]),
    .ADR5(1'b1),
    .O(\timest<5>_rt_19231 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_26.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_26.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \timest<4>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(timest[4]),
    .ADR5(1'b1),
    .O(\timest<4>_rt_19234 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_25.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<7>/wr2_flags<2>_25.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<11>/packet_sender/Mcount_timest_cy<11>_DMUX_Delay  (
    .I(\Result<11>3 ),
    .O(\Result<11>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<11>/packet_sender/Mcount_timest_cy<11>_CMUX_Delay  (
    .I(\Result<10>3 ),
    .O(\Result<10>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<11>/packet_sender/Mcount_timest_cy<11>_BMUX_Delay  (
    .I(\Result<9>3 ),
    .O(\Result<9>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<11>/packet_sender/Mcount_timest_cy<11>_AMUX_Delay  (
    .I(\Result<8>3 ),
    .O(\Result<8>3_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \timest<11>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(timest[11]),
    .ADR5(1'b1),
    .O(\timest<11>_rt_19241 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_32.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_32.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y46" ))
  \Mcount_timest_cy<11>  (
    .CI(\Mcount_timest_cy[7] ),
    .CYINIT(1'b0),
    .CO({\Mcount_timest_cy[11] , \NLW_Mcount_timest_cy<11>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_cy<11>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_cy<11>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({\Result<11>3 , \Result<10>3 , \Result<9>3 , \Result<8>3 }),
    .S({\timest<11>_rt_19241 , \timest<10>_rt_19250 , \timest<9>_rt_19253 , \timest<8>_rt_19256 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<10>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[10]),
    .ADR5(1'b1),
    .O(\timest<10>_rt_19250 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_31.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_31.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<9>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[9]),
    .ADR5(1'b1),
    .O(\timest<9>_rt_19253 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_30.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_30.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<8>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[8]),
    .ADR5(1'b1),
    .O(\timest<8>_rt_19256 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y46" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_29.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<11>/wr2_flags<2>_29.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<15>/packet_sender/Mcount_timest_cy<15>_DMUX_Delay  (
    .I(\Result<15>3 ),
    .O(\Result<15>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<15>/packet_sender/Mcount_timest_cy<15>_CMUX_Delay  (
    .I(\Result<14>3 ),
    .O(\Result<14>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<15>/packet_sender/Mcount_timest_cy<15>_BMUX_Delay  (
    .I(\Result<13>3 ),
    .O(\Result<13>3_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<15>/packet_sender/Mcount_timest_cy<15>_AMUX_Delay  (
    .I(\Result<12>3 ),
    .O(\Result<12>3_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<15>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[15]),
    .ADR5(1'b1),
    .O(\timest<15>_rt_19263 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_36.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_36.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y47" ))
  \Mcount_timest_cy<15>  (
    .CI(\Mcount_timest_cy[11] ),
    .CYINIT(1'b0),
    .CO({\Mcount_timest_cy[15] , \NLW_Mcount_timest_cy<15>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_cy<15>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_cy<15>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({\Result<15>3 , \Result<14>3 , \Result<13>3 , \Result<12>3 }),
    .S({\timest<15>_rt_19263 , \timest<14>_rt_19272 , \timest<13>_rt_19275 , \timest<12>_rt_19278 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \timest<14>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(timest[14]),
    .ADR5(1'b1),
    .O(\timest<14>_rt_19272 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_35.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_35.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<13>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[13]),
    .ADR5(1'b1),
    .O(\timest<13>_rt_19275 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_34.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_34.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 64'hAAAAAAAAAAAAAAAA ))
  \timest<12>_rt  (
    .ADR4(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(timest[12]),
    .ADR5(1'b1),
    .O(\timest<12>_rt_19278 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y47" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_33.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<15>/wr2_flags<2>_33.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<19>/packet_sender/Mcount_timest_cy<19>_DMUX_Delay  (
    .I(Result[19]),
    .O(\Result<19>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<19>/packet_sender/Mcount_timest_cy<19>_CMUX_Delay  (
    .I(Result[18]),
    .O(\Result<18>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<19>/packet_sender/Mcount_timest_cy<19>_BMUX_Delay  (
    .I(Result[17]),
    .O(\Result<17>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<19>/packet_sender/Mcount_timest_cy<19>_AMUX_Delay  (
    .I(Result[16]),
    .O(\Result<16>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<19>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[19]),
    .ADR5(1'b1),
    .O(\timest<19>_rt_19285 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_40.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_40.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y48" ))
  \Mcount_timest_cy<19>  (
    .CI(\Mcount_timest_cy[15] ),
    .CYINIT(1'b0),
    .CO({\Mcount_timest_cy[19] , \NLW_Mcount_timest_cy<19>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_cy<19>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_cy<19>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Result[19], Result[18], Result[17], Result[16]}),
    .S({\timest<19>_rt_19285 , \timest<18>_rt_19294 , \timest<17>_rt_19297 , \timest<16>_rt_19300 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<18>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[18]),
    .ADR5(1'b1),
    .O(\timest<18>_rt_19294 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_39.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_39.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<17>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[17]),
    .ADR5(1'b1),
    .O(\timest<17>_rt_19297 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_38.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_38.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<16>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[16]),
    .ADR5(1'b1),
    .O(\timest<16>_rt_19300 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y48" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_37.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<19>/wr2_flags<2>_37.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<23>/packet_sender/Mcount_timest_cy<23>_DMUX_Delay  (
    .I(Result[23]),
    .O(\Result<23>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<23>/packet_sender/Mcount_timest_cy<23>_CMUX_Delay  (
    .I(Result[22]),
    .O(\Result<22>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<23>/packet_sender/Mcount_timest_cy<23>_BMUX_Delay  (
    .I(Result[21]),
    .O(\Result<21>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<23>/packet_sender/Mcount_timest_cy<23>_AMUX_Delay  (
    .I(Result[20]),
    .O(\Result<20>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<23>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[23]),
    .ADR5(1'b1),
    .O(\timest<23>_rt_19307 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_44.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_44.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y49" ))
  \Mcount_timest_cy<23>  (
    .CI(\Mcount_timest_cy[19] ),
    .CYINIT(1'b0),
    .CO({\Mcount_timest_cy[23] , \NLW_Mcount_timest_cy<23>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_cy<23>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_cy<23>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Result[23], Result[22], Result[21], Result[20]}),
    .S({\timest<23>_rt_19307 , \timest<22>_rt_19316 , \timest<21>_rt_19319 , \timest<20>_rt_19322 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \timest<22>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .ADR2(timest[22]),
    .ADR5(1'b1),
    .O(\timest<22>_rt_19316 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_43.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_43.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<21>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[21]),
    .ADR5(1'b1),
    .O(\timest<21>_rt_19319 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_42.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_42.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<20>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[20]),
    .ADR5(1'b1),
    .O(\timest<20>_rt_19322 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y49" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_41.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<23>/wr2_flags<2>_41.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<27>/packet_sender/Mcount_timest_cy<27>_DMUX_Delay  (
    .I(Result[27]),
    .O(\Result<27>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<27>/packet_sender/Mcount_timest_cy<27>_CMUX_Delay  (
    .I(Result[26]),
    .O(\Result<26>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<27>/packet_sender/Mcount_timest_cy<27>_BMUX_Delay  (
    .I(Result[25]),
    .O(\Result<25>_0 )
  );
  X_BUF   \packet_sender/Mcount_timest_cy<27>/packet_sender/Mcount_timest_cy<27>_AMUX_Delay  (
    .I(Result[24]),
    .O(\Result<24>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<27>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[27]),
    .ADR5(1'b1),
    .O(\timest<27>_rt_19329 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_48.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_48.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y50" ))
  \Mcount_timest_cy<27>  (
    .CI(\Mcount_timest_cy[23] ),
    .CYINIT(1'b0),
    .CO({\Mcount_timest_cy[27] , \NLW_Mcount_timest_cy<27>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_cy<27>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_cy<27>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Result[27], Result[26], Result[25], Result[24]}),
    .S({\timest<27>_rt_19329 , \timest<26>_rt_19338 , \timest<25>_rt_19341 , \timest<24>_rt_19344 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<26>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[26]),
    .ADR5(1'b1),
    .O(\timest<26>_rt_19338 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_47.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_47.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<25>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[25]),
    .ADR5(1'b1),
    .O(\timest<25>_rt_19341 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_46.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_46.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \timest<24>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(timest[24]),
    .ADR5(1'b1),
    .O(\timest<24>_rt_19344 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y50" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_45.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_timest_cy<27>/wr2_flags<2>_45.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Result<31>/packet_sender/Result<31>_DMUX_Delay  (
    .I(Result[31]),
    .O(\Result<31>_0 )
  );
  X_BUF   \packet_sender/Result<31>/packet_sender/Result<31>_CMUX_Delay  (
    .I(Result[30]),
    .O(\Result<30>_0 )
  );
  X_BUF   \packet_sender/Result<31>/packet_sender/Result<31>_BMUX_Delay  (
    .I(Result[29]),
    .O(\Result<29>_0 )
  );
  X_BUF   \packet_sender/Result<31>/packet_sender/Result<31>_AMUX_Delay  (
    .I(Result[28]),
    .O(\Result<28>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y51" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \timest<31>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR2(timest[31]),
    .O(\timest<31>_rt_19351 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y51" ))
  \Mcount_timest_xor<31>  (
    .CI(\Mcount_timest_cy[27] ),
    .CYINIT(1'b0),
    .CO({\NLW_Mcount_timest_xor<31>_CO[3]_UNCONNECTED , \NLW_Mcount_timest_xor<31>_CO[2]_UNCONNECTED , \NLW_Mcount_timest_xor<31>_CO[1]_UNCONNECTED , 
\NLW_Mcount_timest_xor<31>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Mcount_timest_xor<31>_DI[3]_UNCONNECTED , 1'b0, 1'b0, 1'b0}),
    .O({Result[31], Result[30], Result[29], Result[28]}),
    .S({\timest<31>_rt_19351 , \timest<30>_rt_19358 , \timest<29>_rt_19361 , \timest<28>_rt_19364 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y51" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \timest<30>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .ADR2(timest[30]),
    .ADR5(1'b1),
    .O(\timest<30>_rt_19358 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y51" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Result<31>/wr2_flags<2>_51.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Result<31>/wr2_flags<2>_51.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y51" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<29>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[29]),
    .ADR5(1'b1),
    .O(\timest<29>_rt_19361 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y51" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Result<31>/wr2_flags<2>_50.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Result<31>/wr2_flags<2>_50.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y51" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \timest<28>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(timest[28]),
    .ADR5(1'b1),
    .O(\timest<28>_rt_19364 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y51" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Result<31>/wr2_flags<2>_49.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Result<31>/wr2_flags<2>_49.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<3>/packet_sender/Mcount_line_cnt_cy<3>_DMUX_Delay  (
    .I(\Result<3>1 ),
    .O(\Result<3>1_0 )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<3>/packet_sender/Mcount_line_cnt_cy<3>_CMUX_Delay  (
    .I(\Result<2>1 ),
    .O(\Result<2>1_0 )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<3>/packet_sender/Mcount_line_cnt_cy<3>_BMUX_Delay  (
    .I(\Result<1>1 ),
    .O(\Result<1>1_0 )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<3>/packet_sender/Mcount_line_cnt_cy<3>_AMUX_Delay  (
    .I(\Result<0>1 ),
    .O(\Result<0>1_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \line_cnt<3>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(line_cnt[3]),
    .ADR5(1'b1),
    .O(\line_cnt<3>_rt_19370 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_56.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_56.D5LUT_O_UNCONNECTED )
  );
  X_ZERO #(
    .LOC ( "SLICE_X32Y39" ))
  \packet_sender/Mcount_line_cnt_cy<3>/ProtoComp145.CYINITGND.2  (
    .O(\packet_sender/Mcount_line_cnt_cy<3>/ProtoComp145.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X32Y39" ))
  \Mcount_line_cnt_cy<3>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/Mcount_line_cnt_cy<3>/ProtoComp145.CYINITGND.0 ),
    .CO({\Mcount_line_cnt_cy[3] , \NLW_Mcount_line_cnt_cy<3>_CO[2]_UNCONNECTED , \NLW_Mcount_line_cnt_cy<3>_CO[1]_UNCONNECTED , 
\NLW_Mcount_line_cnt_cy<3>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b1}),
    .O({\Result<3>1 , \Result<2>1 , \Result<1>1 , \Result<0>1 }),
    .S({\line_cnt<3>_rt_19370 , \line_cnt<2>_rt_19379 , \line_cnt<1>_rt_19382 , Mcount_line_cnt_lut[0]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 64'hAAAAAAAAAAAAAAAA ))
  \line_cnt<2>_rt  (
    .ADR4(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(line_cnt[2]),
    .ADR5(1'b1),
    .O(\line_cnt<2>_rt_19379 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_55.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_55.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 64'hAAAAAAAAAAAAAAAA ))
  \line_cnt<1>_rt  (
    .ADR4(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(line_cnt[1]),
    .ADR5(1'b1),
    .O(\line_cnt<1>_rt_19382 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_54.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_line_cnt_cy<3>/wr2_flags<2>_54.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 64'h3333333333333333 ))
  \Mcount_line_cnt_lut<0>_INV_0  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(line_cnt[0]),
    .ADR5(1'b1),
    .O(Mcount_line_cnt_lut[0])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y39" ),
    .INIT ( 32'hFFFFFFFF ))
  \N0_5.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_N0_5.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<7>/packet_sender/Mcount_line_cnt_cy<7>_DMUX_Delay  (
    .I(\Result<7>1 ),
    .O(\Result<7>1_0 )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<7>/packet_sender/Mcount_line_cnt_cy<7>_CMUX_Delay  (
    .I(\Result<6>1 ),
    .O(\Result<6>1_0 )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<7>/packet_sender/Mcount_line_cnt_cy<7>_BMUX_Delay  (
    .I(\Result<5>1 ),
    .O(\Result<5>1_0 )
  );
  X_BUF   \packet_sender/Mcount_line_cnt_cy<7>/packet_sender/Mcount_line_cnt_cy<7>_AMUX_Delay  (
    .I(\Result<4>1 ),
    .O(\Result<4>1_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \line_cnt<7>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(line_cnt[7]),
    .ADR5(1'b1),
    .O(\line_cnt<7>_rt_19392 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_60.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_60.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X32Y40" ))
  \Mcount_line_cnt_cy<7>  (
    .CI(\Mcount_line_cnt_cy[3] ),
    .CYINIT(1'b0),
    .CO({\Mcount_line_cnt_cy[7] , \NLW_Mcount_line_cnt_cy<7>_CO[2]_UNCONNECTED , \NLW_Mcount_line_cnt_cy<7>_CO[1]_UNCONNECTED , 
\NLW_Mcount_line_cnt_cy<7>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({\Result<7>1 , \Result<6>1 , \Result<5>1 , \Result<4>1 }),
    .S({\line_cnt<7>_rt_19392 , \line_cnt<6>_rt_19401 , \line_cnt<5>_rt_19404 , \line_cnt<4>_rt_19407 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 64'hAAAAAAAAAAAAAAAA ))
  \line_cnt<6>_rt  (
    .ADR4(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(line_cnt[6]),
    .ADR5(1'b1),
    .O(\line_cnt<6>_rt_19401 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_59.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_59.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 64'hAAAAAAAAAAAAAAAA ))
  \line_cnt<5>_rt  (
    .ADR4(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(line_cnt[5]),
    .ADR5(1'b1),
    .O(\line_cnt<5>_rt_19404 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_58.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_58.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \line_cnt<4>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(line_cnt[4]),
    .ADR5(1'b1),
    .O(\line_cnt<4>_rt_19407 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y40" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_57.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_line_cnt_cy<7>/wr2_flags<2>_57.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Result<9>1/packet_sender/Result<9>1_BMUX_Delay  (
    .I(\Result<9>1 ),
    .O(\Result<9>1_0 )
  );
  X_BUF   \packet_sender/Result<9>1/packet_sender/Result<9>1_AMUX_Delay  (
    .I(\Result<8>1 ),
    .O(\Result<8>1_0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X32Y41" ))
  \Mcount_line_cnt_xor<9>  (
    .CI(\Mcount_line_cnt_cy[7] ),
    .CYINIT(1'b0),
    .CO({\NLW_Mcount_line_cnt_xor<9>_CO[3]_UNCONNECTED , \NLW_Mcount_line_cnt_xor<9>_CO[2]_UNCONNECTED , 
\NLW_Mcount_line_cnt_xor<9>_CO[1]_UNCONNECTED , \NLW_Mcount_line_cnt_xor<9>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Mcount_line_cnt_xor<9>_DI[3]_UNCONNECTED , \NLW_Mcount_line_cnt_xor<9>_DI[2]_UNCONNECTED , 
\NLW_Mcount_line_cnt_xor<9>_DI[1]_UNCONNECTED , 1'b0}),
    .O({\NLW_Mcount_line_cnt_xor<9>_O[3]_UNCONNECTED , \NLW_Mcount_line_cnt_xor<9>_O[2]_UNCONNECTED , \Result<9>1 , \Result<8>1 }),
    .S({\NLW_Mcount_line_cnt_xor<9>_S[3]_UNCONNECTED , \NLW_Mcount_line_cnt_xor<9>_S[2]_UNCONNECTED , \line_cnt<9>_rt_19417 , \line_cnt<8>_rt_19419 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y41" ),
    .INIT ( 64'hAAAAAAAAAAAAAAAA ))
  \line_cnt<9>_rt  (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .ADR0(line_cnt[9]),
    .O(\line_cnt<9>_rt_19417 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X32Y41" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \line_cnt<8>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(line_cnt[8]),
    .ADR5(1'b1),
    .O(\line_cnt<8>_rt_19419 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X32Y41" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Result<9>1/wr2_flags<2>_61.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Result<9>1/wr2_flags<2>_61.A5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 1'b0 ))
  packet_size_count_3 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_3/CLK ),
    .I(Mcount_packet_size_count3),
    .O(packet_size_count[3]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 64'hFF000000FF000000 ))
  \Mcount_packet_size_count_lut<3>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[3]),
    .ADR3(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[3])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<3>/wr2_flags<2>_6.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_6.D5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 1'b0 ))
  packet_size_count_2 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_2/CLK ),
    .I(Mcount_packet_size_count2),
    .O(packet_size_count[2]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X22Y43" ))
  \Mcount_packet_size_count_cy<3>  (
    .CI(1'b0),
    .CYINIT(state_FSM_FFd1_21101),
    .CO({\Mcount_packet_size_count_cy[3] , \NLW_Mcount_packet_size_count_cy<3>_CO[2]_UNCONNECTED , 
\NLW_Mcount_packet_size_count_cy<3>_CO[1]_UNCONNECTED , \NLW_Mcount_packet_size_count_cy<3>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Mcount_packet_size_count3, Mcount_packet_size_count2, Mcount_packet_size_count1, Mcount_packet_size_count}),
    .S({Mcount_packet_size_count_lut[3], Mcount_packet_size_count_lut[2], Mcount_packet_size_count_lut[1], Mcount_packet_size_count_lut[0]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 64'hF0F00000F0F00000 ))
  \Mcount_packet_size_count_lut<2>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR4(packet_size_count[2]),
    .ADR2(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[2])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<3>/wr2_flags<2>_5.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_5.C5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 1'b0 ))
  packet_size_count_1 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_1/CLK ),
    .I(Mcount_packet_size_count1),
    .O(packet_size_count[1]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 64'hCCCC0000CCCC0000 ))
  \Mcount_packet_size_count_lut<1>  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[1]),
    .ADR1(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[1])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<3>/wr2_flags<2>_4.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_4.B5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 1'b0 ))
  packet_size_count_0 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_0/CLK ),
    .I(Mcount_packet_size_count),
    .O(packet_size_count[0]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 64'hCCCC0000CCCC0000 ))
  \Mcount_packet_size_count_lut<0>  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[0]),
    .ADR1(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[0])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y43" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<3>/wr2_flags<2>_3.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<3>/wr2_flags<2>_3.A5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 1'b0 ))
  packet_size_count_7 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_7/CLK ),
    .I(Mcount_packet_size_count7),
    .O(packet_size_count[7]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 64'hAAAA0000AAAA0000 ))
  \Mcount_packet_size_count_lut<7>  (
    .ADR3(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[7]),
    .ADR0(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[7])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<7>/wr2_flags<2>_10.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_10.D5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 1'b0 ))
  packet_size_count_6 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_6/CLK ),
    .I(Mcount_packet_size_count6),
    .O(packet_size_count[6]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X22Y44" ))
  \Mcount_packet_size_count_cy<7>  (
    .CI(\Mcount_packet_size_count_cy[3] ),
    .CYINIT(1'b0),
    .CO({\Mcount_packet_size_count_cy[7] , \NLW_Mcount_packet_size_count_cy<7>_CO[2]_UNCONNECTED , 
\NLW_Mcount_packet_size_count_cy<7>_CO[1]_UNCONNECTED , \NLW_Mcount_packet_size_count_cy<7>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Mcount_packet_size_count7, Mcount_packet_size_count6, Mcount_packet_size_count5, Mcount_packet_size_count4}),
    .S({Mcount_packet_size_count_lut[7], Mcount_packet_size_count_lut[6], Mcount_packet_size_count_lut[5], Mcount_packet_size_count_lut[4]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 64'hCCCC0000CCCC0000 ))
  \Mcount_packet_size_count_lut<6>  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[6]),
    .ADR1(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[6])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<7>/wr2_flags<2>_9.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_9.C5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 1'b0 ))
  packet_size_count_5 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_5/CLK ),
    .I(Mcount_packet_size_count5),
    .O(packet_size_count[5]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 64'hF0F00000F0F00000 ))
  \Mcount_packet_size_count_lut<5>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR4(packet_size_count[5]),
    .ADR2(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[5])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<7>/wr2_flags<2>_8.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_8.B5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 1'b0 ))
  packet_size_count_4 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_4/CLK ),
    .I(Mcount_packet_size_count4),
    .O(packet_size_count[4]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 64'hA0A0A0A0A0A0A0A0 ))
  \Mcount_packet_size_count_lut<4>  (
    .ADR4(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR2(packet_size_count[4]),
    .ADR0(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[4])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y44" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<7>/wr2_flags<2>_7.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<7>/wr2_flags<2>_7.A5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 1'b0 ))
  packet_size_count_11 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_11/CLK ),
    .I(Mcount_packet_size_count11),
    .O(packet_size_count[11]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 64'hFF000000FF000000 ))
  \Mcount_packet_size_count_lut<11>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[11]),
    .ADR3(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[11])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<11>/wr2_flags<2>_14.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_14.D5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 1'b0 ))
  packet_size_count_10 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_10/CLK ),
    .I(Mcount_packet_size_count10),
    .O(packet_size_count[10]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X22Y45" ))
  \Mcount_packet_size_count_cy<11>  (
    .CI(\Mcount_packet_size_count_cy[7] ),
    .CYINIT(1'b0),
    .CO({\Mcount_packet_size_count_cy[11] , \NLW_Mcount_packet_size_count_cy<11>_CO[2]_UNCONNECTED , 
\NLW_Mcount_packet_size_count_cy<11>_CO[1]_UNCONNECTED , \NLW_Mcount_packet_size_count_cy<11>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({Mcount_packet_size_count11, Mcount_packet_size_count10, Mcount_packet_size_count9, Mcount_packet_size_count8}),
    .S({Mcount_packet_size_count_lut[11], Mcount_packet_size_count_lut[10], Mcount_packet_size_count_lut[9], Mcount_packet_size_count_lut[8]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 64'hF0F00000F0F00000 ))
  \Mcount_packet_size_count_lut<10>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR4(packet_size_count[10]),
    .ADR2(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[10])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<11>/wr2_flags<2>_13.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_13.C5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 1'b0 ))
  packet_size_count_9 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_9/CLK ),
    .I(Mcount_packet_size_count9),
    .O(packet_size_count[9]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 64'hCCCC0000CCCC0000 ))
  \Mcount_packet_size_count_lut<9>  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[9]),
    .ADR1(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[9])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<11>/wr2_flags<2>_12.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_12.B5LUT_O_UNCONNECTED )
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 1'b0 ))
  packet_size_count_8 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_8/CLK ),
    .I(Mcount_packet_size_count8),
    .O(packet_size_count[8]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 64'hCCCC0000CCCC0000 ))
  \Mcount_packet_size_count_lut<8>  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[8]),
    .ADR1(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[8])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y45" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<11>/wr2_flags<2>_11.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<11>/wr2_flags<2>_11.A5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X22Y46" ))
  \Mcount_packet_size_count_xor<13>  (
    .CI(\Mcount_packet_size_count_cy[11] ),
    .CYINIT(1'b0),
    .CO({\NLW_Mcount_packet_size_count_xor<13>_CO[3]_UNCONNECTED , \NLW_Mcount_packet_size_count_xor<13>_CO[2]_UNCONNECTED , 
\NLW_Mcount_packet_size_count_xor<13>_CO[1]_UNCONNECTED , \NLW_Mcount_packet_size_count_xor<13>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Mcount_packet_size_count_xor<13>_DI[3]_UNCONNECTED , \NLW_Mcount_packet_size_count_xor<13>_DI[2]_UNCONNECTED , 
\NLW_Mcount_packet_size_count_xor<13>_DI[1]_UNCONNECTED , 1'b0}),
    .O({\NLW_Mcount_packet_size_count_xor<13>_O[3]_UNCONNECTED , \NLW_Mcount_packet_size_count_xor<13>_O[2]_UNCONNECTED , Mcount_packet_size_count13, 
Mcount_packet_size_count12}),
    .S({\NLW_Mcount_packet_size_count_xor<13>_S[3]_UNCONNECTED , \NLW_Mcount_packet_size_count_xor<13>_S[2]_UNCONNECTED , 
Mcount_packet_size_count_lut[13], Mcount_packet_size_count_lut[12]})
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y46" ),
    .INIT ( 1'b0 ))
  packet_size_count_13 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_13/CLK ),
    .I(Mcount_packet_size_count13),
    .O(packet_size_count[13]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y46" ),
    .INIT ( 64'hF0F00000F0F00000 ))
  \Mcount_packet_size_count_lut<13>  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR5(1'b1),
    .ADR4(packet_size_count[13]),
    .ADR3(1'b1),
    .ADR2(state_FSM_FFd1_21101),
    .O(Mcount_packet_size_count_lut[13])
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y46" ),
    .INIT ( 1'b0 ))
  packet_size_count_12 (
    .CE(_n0780_inv),
    .CLK(\NlwBufferSignal_packet_sender/packet_size_count_12/CLK ),
    .I(Mcount_packet_size_count12),
    .O(packet_size_count[12]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y46" ),
    .INIT ( 64'hCCCC0000CCCC0000 ))
  \Mcount_packet_size_count_lut<12>  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR4(packet_size_count[12]),
    .ADR1(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(Mcount_packet_size_count_lut[12])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X22Y46" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/packet_size_count<13>/wr2_flags<2>_15.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/packet_size_count<13>/wr2_flags<2>_15.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<3>/packet_sender/Mcount_seqn_cy<3>_DMUX_Delay  (
    .I(\Result<3>2 ),
    .O(\Result<3>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<3>/packet_sender/Mcount_seqn_cy<3>_CMUX_Delay  (
    .I(\Result<2>2 ),
    .O(\Result<2>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<3>/packet_sender/Mcount_seqn_cy<3>_BMUX_Delay  (
    .I(\Result<1>2 ),
    .O(\Result<1>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<3>/packet_sender/Mcount_seqn_cy<3>_AMUX_Delay  (
    .I(\Result<0>2 ),
    .O(\Result<0>2_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \seqn<3>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .ADR2(seqn[3]),
    .ADR5(1'b1),
    .O(\seqn<3>_rt_19538 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_66.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_66.D5LUT_O_UNCONNECTED )
  );
  X_ZERO #(
    .LOC ( "SLICE_X28Y35" ))
  \packet_sender/Mcount_seqn_cy<3>/ProtoComp145.CYINITGND.3  (
    .O(\packet_sender/Mcount_seqn_cy<3>/ProtoComp145.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y35" ))
  \Mcount_seqn_cy<3>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/Mcount_seqn_cy<3>/ProtoComp145.CYINITGND.0 ),
    .CO({\Mcount_seqn_cy[3] , \NLW_Mcount_seqn_cy<3>_CO[2]_UNCONNECTED , \NLW_Mcount_seqn_cy<3>_CO[1]_UNCONNECTED , 
\NLW_Mcount_seqn_cy<3>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b1}),
    .O({\Result<3>2 , \Result<2>2 , \Result<1>2 , \Result<0>2 }),
    .S({\seqn<3>_rt_19538 , \seqn<2>_rt_19547 , \seqn<1>_rt_19550 , Mcount_seqn_lut[0]})
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \seqn<2>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(seqn[2]),
    .ADR5(1'b1),
    .O(\seqn<2>_rt_19547 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_65.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_65.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \seqn<1>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(seqn[1]),
    .ADR5(1'b1),
    .O(\seqn<1>_rt_19550 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_64.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<3>/wr2_flags<2>_64.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 64'h0000FFFF0000FFFF ))
  \Mcount_seqn_lut<0>_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(seqn[0]),
    .ADR5(1'b1),
    .O(Mcount_seqn_lut[0])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y35" ),
    .INIT ( 32'hFFFFFFFF ))
  \N0_6.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_N0_6.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<7>/packet_sender/Mcount_seqn_cy<7>_DMUX_Delay  (
    .I(\Result<7>2 ),
    .O(\Result<7>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<7>/packet_sender/Mcount_seqn_cy<7>_CMUX_Delay  (
    .I(\Result<6>2 ),
    .O(\Result<6>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<7>/packet_sender/Mcount_seqn_cy<7>_BMUX_Delay  (
    .I(\Result<5>2 ),
    .O(\Result<5>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<7>/packet_sender/Mcount_seqn_cy<7>_AMUX_Delay  (
    .I(\Result<4>2 ),
    .O(\Result<4>2_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \seqn<7>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .ADR2(seqn[7]),
    .ADR5(1'b1),
    .O(\seqn<7>_rt_19560 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_70.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_70.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y36" ))
  \Mcount_seqn_cy<7>  (
    .CI(\Mcount_seqn_cy[3] ),
    .CYINIT(1'b0),
    .CO({\Mcount_seqn_cy[7] , \NLW_Mcount_seqn_cy<7>_CO[2]_UNCONNECTED , \NLW_Mcount_seqn_cy<7>_CO[1]_UNCONNECTED , 
\NLW_Mcount_seqn_cy<7>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({\Result<7>2 , \Result<6>2 , \Result<5>2 , \Result<4>2 }),
    .S({\seqn<7>_rt_19560 , \seqn<6>_rt_19569 , \seqn<5>_rt_19572 , \seqn<4>_rt_19575 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \seqn<6>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(seqn[6]),
    .ADR5(1'b1),
    .O(\seqn<6>_rt_19569 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_69.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_69.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \seqn<5>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(seqn[5]),
    .ADR5(1'b1),
    .O(\seqn<5>_rt_19572 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_68.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_68.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \seqn<4>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(seqn[4]),
    .ADR5(1'b1),
    .O(\seqn<4>_rt_19575 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y36" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_67.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<7>/wr2_flags<2>_67.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<11>/packet_sender/Mcount_seqn_cy<11>_DMUX_Delay  (
    .I(\Result<11>2 ),
    .O(\Result<11>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<11>/packet_sender/Mcount_seqn_cy<11>_CMUX_Delay  (
    .I(\Result<10>2 ),
    .O(\Result<10>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<11>/packet_sender/Mcount_seqn_cy<11>_BMUX_Delay  (
    .I(\Result<9>2 ),
    .O(\Result<9>2_0 )
  );
  X_BUF   \packet_sender/Mcount_seqn_cy<11>/packet_sender/Mcount_seqn_cy<11>_AMUX_Delay  (
    .I(\Result<8>2 ),
    .O(\Result<8>2_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \seqn<11>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .ADR2(seqn[11]),
    .ADR5(1'b1),
    .O(\seqn<11>_rt_19582 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_74.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_74.D5LUT_O_UNCONNECTED )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y37" ))
  \Mcount_seqn_cy<11>  (
    .CI(\Mcount_seqn_cy[7] ),
    .CYINIT(1'b0),
    .CO({\Mcount_seqn_cy[11] , \NLW_Mcount_seqn_cy<11>_CO[2]_UNCONNECTED , \NLW_Mcount_seqn_cy<11>_CO[1]_UNCONNECTED , 
\NLW_Mcount_seqn_cy<11>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b0, 1'b0, 1'b0}),
    .O({\Result<11>2 , \Result<10>2 , \Result<9>2 , \Result<8>2 }),
    .S({\seqn<11>_rt_19582 , \seqn<10>_rt_19591 , \seqn<9>_rt_19594 , \seqn<8>_rt_19597 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \seqn<10>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(seqn[10]),
    .ADR5(1'b1),
    .O(\seqn<10>_rt_19591 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_73.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_73.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \seqn<9>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(seqn[9]),
    .ADR5(1'b1),
    .O(\seqn<9>_rt_19594 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_72.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_72.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \seqn<8>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(seqn[8]),
    .ADR5(1'b1),
    .O(\seqn<8>_rt_19597 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y37" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_71.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Mcount_seqn_cy<11>/wr2_flags<2>_71.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Result<15>2/packet_sender/Result<15>2_DMUX_Delay  (
    .I(\Result<15>2 ),
    .O(\Result<15>2_0 )
  );
  X_BUF   \packet_sender/Result<15>2/packet_sender/Result<15>2_CMUX_Delay  (
    .I(\Result<14>2 ),
    .O(\Result<14>2_0 )
  );
  X_BUF   \packet_sender/Result<15>2/packet_sender/Result<15>2_BMUX_Delay  (
    .I(\Result<13>2 ),
    .O(\Result<13>2_0 )
  );
  X_BUF   \packet_sender/Result<15>2/packet_sender/Result<15>2_AMUX_Delay  (
    .I(\Result<12>2 ),
    .O(\Result<12>2_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y38" ),
    .INIT ( 64'hFFFF0000FFFF0000 ))
  \seqn<15>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .ADR4(seqn[15]),
    .O(\seqn<15>_rt_19604 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X28Y38" ))
  \Mcount_seqn_xor<15>  (
    .CI(\Mcount_seqn_cy[11] ),
    .CYINIT(1'b0),
    .CO({\NLW_Mcount_seqn_xor<15>_CO[3]_UNCONNECTED , \NLW_Mcount_seqn_xor<15>_CO[2]_UNCONNECTED , \NLW_Mcount_seqn_xor<15>_CO[1]_UNCONNECTED , 
\NLW_Mcount_seqn_xor<15>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Mcount_seqn_xor<15>_DI[3]_UNCONNECTED , 1'b0, 1'b0, 1'b0}),
    .O({\Result<15>2 , \Result<14>2 , \Result<13>2 , \Result<12>2 }),
    .S({\seqn<15>_rt_19604 , \seqn<14>_rt_19611 , \seqn<13>_rt_19614 , \seqn<12>_rt_19617 })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y38" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \seqn<14>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(seqn[14]),
    .ADR5(1'b1),
    .O(\seqn<14>_rt_19611 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y38" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Result<15>2/wr2_flags<2>_77.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Result<15>2/wr2_flags<2>_77.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y38" ),
    .INIT ( 64'hFF00FF00FF00FF00 ))
  \seqn<13>_rt  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(seqn[13]),
    .ADR5(1'b1),
    .O(\seqn<13>_rt_19614 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y38" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Result<15>2/wr2_flags<2>_76.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Result<15>2/wr2_flags<2>_76.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y38" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \seqn<12>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(seqn[12]),
    .ADR5(1'b1),
    .O(\seqn<12>_rt_19617 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X28Y38" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Result<15>2/wr2_flags<2>_75.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Result<15>2/wr2_flags<2>_75.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_BMUX_Delay  (
    .I(\pkt_offset[15]_GND_53_o_add_141_OUT<4> ),
    .O(\pkt_offset[15]_GND_53_o_add_141_OUT<4>_0 )
  );
  X_BUF   \packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_AMUX_Delay  (
    .I(\pkt_offset[15]_GND_53_o_add_141_OUT<3> ),
    .O(\pkt_offset[15]_GND_53_o_add_141_OUT<3>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 64'hF0F0F0F0F0F0F0F0 ))
  \pkt_offset<4>_rt_pack_1  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(1'b1),
    .ADR3(1'b1),
    .ADR2(\pkt_offset[4] ),
    .ADR5(1'b1),
    .O(\pkt_offset<4>_rt_pack_3 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/wr2_flags<2>_19.D5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/wr2_flags<2>_19.D5LUT_O_UNCONNECTED )
  );
  X_ZERO #(
    .LOC ( "SLICE_X20Y39" ))
  \packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/ProtoComp179.CYINITGND  (
    .O(\packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/ProtoComp179.CYINITGND.0 )
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X20Y39" ))
  \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>  (
    .CI(1'b0),
    .CYINIT(\packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/ProtoComp179.CYINITGND.0 ),
    .CO({\Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_21279 , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_CO[2]_UNCONNECTED , 
\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_CO[1]_UNCONNECTED , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_CO[0]_UNCONNECTED }),
    .DI({1'b0, 1'b1, 1'b0, 1'b1}),
    .O({\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_O[3]_UNCONNECTED , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_O[2]_UNCONNECTED , 
\pkt_offset[15]_GND_53_o_add_141_OUT<4> , \pkt_offset[15]_GND_53_o_add_141_OUT<3> }),
    .S({\pkt_offset<4>_rt_pack_3 , \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<5> , \pkt_offset<4>_rt_19633 , 
\Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<3> })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 64'h0000FFFF0000FFFF ))
  \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<5>_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(\pkt_offset<3>_0 ),
    .ADR5(1'b1),
    .O(\Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<5> )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 32'hFFFFFFFF ))
  \N0_2.C5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_N0_2.C5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 64'hCCCCCCCCCCCCCCCC ))
  \pkt_offset<4>_rt  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR1(\pkt_offset[4] ),
    .ADR5(1'b1),
    .O(\pkt_offset<4>_rt_19633 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 32'h00000000 ))
  \packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/wr2_flags<2>_18.B5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_packet_sender/Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>/wr2_flags<2>_18.B5LUT_O_UNCONNECTED )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 64'h00FF00FF00FF00FF ))
  \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<3>_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(\pkt_offset<3>_0 ),
    .ADR5(1'b1),
    .O(\Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<3> )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y39" ),
    .INIT ( 32'hFFFFFFFF ))
  \N0.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_N0.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \packet_sender/header_checksum_input_20/packet_sender/header_checksum_input_20_CMUX_Delay  (
    .I(Mmux_nxt_wr_data_o351),
    .O(Mmux_nxt_wr_data_o351_0)
  );
  X_BUF   \packet_sender/header_checksum_input_20/packet_sender/header_checksum_input_20_BMUX_Delay  (
    .I(\state[4]_header_checksum_input[8]_Mux_242_o ),
    .O(\state[4]_header_checksum_input[8]_Mux_242_o_0 )
  );
  X_LATCHE #(
    .LOC ( "SLICE_X16Y35" ),
    .INIT ( 1'b0 ))
  header_checksum_input_20 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_20/CLK ),
    .I(\state[4]_header_checksum_input[20]_Mux_218_o ),
    .O(header_checksum_input_20_21069),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X16Y35" ),
    .INIT ( 64'h8F8F00008F8F0000 ))
  \state__n0859<4>1  (
    .ADR3(1'b1),
    .ADR4(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd2_21102),
    .ADR1(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd3_21103),
    .ADR5(1'b1),
    .O(\state[4]_header_checksum_input[20]_Mux_218_o )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X16Y35" ),
    .INIT ( 32'hF3F33F33 ))
  Mmux_nxt_wr_data_o352 (
    .ADR3(seqn[10]),
    .ADR4(state_FSM_FFd4_21099),
    .ADR0(1'b1),
    .ADR1(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd3_21103),
    .O(Mmux_nxt_wr_data_o351)
  );
  X_LATCHE #(
    .LOC ( "SLICE_X16Y35" ),
    .INIT ( 1'b0 ))
  header_checksum_input_26 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_26/CLK ),
    .I(\state[4]_header_checksum_input[26]_Mux_206_o ),
    .O(header_checksum_input_26_21068),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X16Y35" ),
    .INIT ( 64'hAB0B0303AB0B0303 ))
  \state__n0859<3>1  (
    .ADR2(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd5_21100),
    .ADR5(1'b1),
    .O(\state[4]_header_checksum_input[26]_Mux_206_o )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X16Y35" ),
    .INIT ( 32'hEFCFCFCF ))
  \state__n0859<5>1  (
    .ADR2(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd5_21100),
    .O(\state[4]_header_checksum_input[8]_Mux_242_o )
  );
  X_LATCHE #(
    .LOC ( "SLICE_X16Y35" ),
    .INIT ( 1'b0 ))
  header_checksum_input_8 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_8/CLK ),
    .I(\NlwBufferSignal_packet_sender/header_checksum_input_8/IN ),
    .O(header_checksum_input_8_21070),
    .RST(GND),
    .SET(GND)
  );
  X_BUF   \packet_sender/header_checksum_input_3/packet_sender/header_checksum_input_3_DMUX_Delay  (
    .I(\state[4]_header_checksum_input[6]_Mux_246_o ),
    .O(\state[4]_header_checksum_input[6]_Mux_246_o_0 )
  );
  X_BUF   \packet_sender/header_checksum_input_3/packet_sender/header_checksum_input_3_BMUX_Delay  (
    .I(Mmux_nxt_wr_data_o25),
    .O(Mmux_nxt_wr_data_o25_0)
  );
  X_LATCHE #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 1'b0 ))
  header_checksum_input_3 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_3/CLK ),
    .I(\state[4]_header_checksum_input[3]_Mux_252_o ),
    .O(header_checksum_input_3_21072),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 64'h000000AF000000AF ))
  \Mmux_state[4]_header_checksum_input[3]_Mux_252_o11  (
    .ADR1(1'b1),
    .ADR3(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd4_21099),
    .ADR2(\pkt_offset[4] ),
    .ADR0(\pkt_offset<3>_0 ),
    .ADR5(1'b1),
    .O(\state[4]_header_checksum_input[3]_Mux_252_o )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 32'h00000050 ))
  \Mmux_state[4]_header_checksum_input[6]_Mux_246_o11  (
    .ADR1(1'b1),
    .ADR3(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd4_21099),
    .ADR2(\pkt_offset[4] ),
    .ADR0(\pkt_offset<3>_0 ),
    .O(\state[4]_header_checksum_input[6]_Mux_246_o )
  );
  X_LATCHE #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 1'b0 ))
  header_checksum_input_6 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_6/CLK ),
    .I(\NlwBufferSignal_packet_sender/header_checksum_input_6/IN ),
    .O(header_checksum_input_6_21071),
    .RST(GND),
    .SET(GND)
  );
  X_LATCHE #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 1'b0 ))
  header_checksum_input_1 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_1/CLK ),
    .I(\state[4]_header_checksum_input[1]_Mux_256_o ),
    .O(header_checksum_input_1_21073),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 64'h00F000F000F000F0 ))
  \state__n0859<6>1  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(1'b1),
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd3_21103),
    .ADR5(1'b1),
    .O(\state[4]_header_checksum_input[1]_Mux_256_o )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 32'h20FC20F0 ))
  Mmux_nxt_wr_data_o251 (
    .ADR1(state_FSM_FFd4_21099),
    .ADR0(\pkt_offset[4] ),
    .ADR4(timest[4]),
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd3_21103),
    .O(Mmux_nxt_wr_data_o25)
  );
  X_LATCHE #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 1'b0 ))
  header_checksum_input_0 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_0/CLK ),
    .I(Mmux_nxt_wr_data_o38),
    .O(header_checksum_input_0_21074),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X17Y35" ),
    .INIT ( 64'hFF00FF0000000000 ))
  \state__n0859<7>1  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd3_21103),
    .O(Mmux_nxt_wr_data_o38)
  );
  X_BUF   \packet_sender/header_checksum_input_30/packet_sender/header_checksum_input_30_CMUX_Delay  (
    .I(Mmux_nxt_wr_data_o47),
    .O(Mmux_nxt_wr_data_o47_0)
  );
  X_BUF   \packet_sender/header_checksum_input_30/packet_sender/header_checksum_input_30_BMUX_Delay  (
    .I(\state[4]_header_checksum_input[29]_Mux_200_o ),
    .O(\state[4]_header_checksum_input[29]_Mux_200_o_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 64'h00000000020E0208 ))
  \Mmux_state[4]_GND_55_o_Mux_197_o11  (
    .ADR5(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd2_21102),
    .ADR0(wr_dst_rdy_i),
    .ADR1(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd4_21099),
    .O(\state[4]_GND_55_o_Mux_197_o )
  );
  X_LATCHE #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 1'b0 ))
  header_checksum_input_30 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_30/CLK ),
    .I(\state[4]_header_checksum_input[30]_Mux_198_o ),
    .O(header_checksum_input_30_21066),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 64'hFFF3FFF3FFF3FFF3 ))
  \state__n0859<1>1  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd5_21100),
    .ADR5(1'b1),
    .O(\state[4]_header_checksum_input[30]_Mux_198_o )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 32'hC230C200 ))
  Mmux_nxt_wr_data_o471 (
    .ADR0(timest[22]),
    .ADR4(line_cnt[6]),
    .ADR2(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd5_21100),
    .O(Mmux_nxt_wr_data_o47)
  );
  X_LATCHE #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 1'b0 ))
  header_checksum_input_31 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_31/CLK ),
    .I(\state[4]_header_checksum_input[31]_Mux_196_o ),
    .O(header_checksum_input_31_21065),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 64'hEAFFAAAAEAFFAAAA ))
  \state__n0859<0>1  (
    .ADR4(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd2_21102),
    .ADR0(state_FSM_FFd3_21103),
    .ADR5(1'b1),
    .O(\state[4]_header_checksum_input[31]_Mux_196_o )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 32'hC0D50000 ))
  \state__n0859<2>1  (
    .ADR4(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd2_21102),
    .ADR0(state_FSM_FFd3_21103),
    .O(\state[4]_header_checksum_input[29]_Mux_200_o )
  );
  X_LATCHE #(
    .LOC ( "SLICE_X17Y36" ),
    .INIT ( 1'b0 ))
  header_checksum_input_29 (
    .GE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_input_29/CLK ),
    .I(\NlwBufferSignal_packet_sender/header_checksum_input_29/IN ),
    .O(header_checksum_input_29_21067),
    .RST(GND),
    .SET(GND)
  );
  X_SFF #(
    .LOC ( "SLICE_X18Y38" ),
    .INIT ( 1'b0 ))
  wr_data_o_26 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_26/CLK ),
    .I(nxt_wr_data_o[26]),
    .O(wr_data_o[26]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X18Y38" ),
    .INIT ( 64'h4703440047037733 ))
  Mmux_nxt_wr_data_o353 (
    .ADR1(state_FSM_FFd5_21100),
    .ADR4(header_checksum[10]),
    .ADR5(N17),
    .ADR2(N18_0),
    .ADR3(Mmux_nxt_wr_data_o351_0),
    .ADR0(state_FSM_FFd2_21102),
    .O(nxt_wr_data_o[26])
  );
  X_BUF   \wr2_data<23>/wr2_data<23>_DMUX_Delay  (
    .I(N27),
    .O(N27_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X18Y40" ),
    .INIT ( 64'h8800000088000000 ))
  Mmux_nxt_wr_data_o311_SW0 (
    .ADR2(1'b1),
    .ADR0(state_FSM_FFd5_21100),
    .ADR4(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd2_21102),
    .ADR5(1'b1),
    .O(N26)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X18Y40" ),
    .INIT ( 32'h88004400 ))
  Mmux_nxt_wr_data_o311_SW1 (
    .ADR2(1'b1),
    .ADR0(state_FSM_FFd5_21100),
    .ADR4(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd2_21102),
    .O(N27)
  );
  X_SFF #(
    .LOC ( "SLICE_X18Y40" ),
    .INIT ( 1'b0 ))
  wr_data_o_23 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_23/CLK ),
    .I(nxt_wr_data_o[23]),
    .O(wr_data_o[23]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X18Y40" ),
    .INIT ( 64'hFFAFFFAFFAAAD888 ))
  Mmux_nxt_wr_data_o314 (
    .ADR0(state_FSM_FFd1_21101),
    .ADR4(Mmux_nxt_wr_data_o311),
    .ADR5(N26),
    .ADR1(Mmux_nxt_wr_data_o312_21140),
    .ADR2(header_checksum[7]),
    .ADR3(N27_0),
    .O(nxt_wr_data_o[23])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X18Y40" ),
    .INIT ( 64'hCCCC33BFCCCC33B3 ))
  Mmux_nxt_wr_data_o291 (
    .ADR4(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd4_21099),
    .ADR0(\pkt_offset[4] ),
    .ADR2(state_FSM_FFd3_21103),
    .ADR5(seqn[6]),
    .ADR1(state_FSM_FFd1_21101),
    .O(Mmux_nxt_wr_data_o29)
  );
  X_SFF #(
    .LOC ( "SLICE_X18Y40" ),
    .INIT ( 1'b0 ))
  wr_data_o_22 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_22/CLK ),
    .I(nxt_wr_data_o[22]),
    .O(wr_data_o[22]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X18Y40" ),
    .INIT ( 64'hFFFF5555AFBB0511 ))
  Mmux_nxt_wr_data_o294 (
    .ADR0(state_FSM_FFd5_21100),
    .ADR5(Mmux_nxt_wr_data_o291_21297),
    .ADR3(header_checksum[6]),
    .ADR1(N29),
    .ADR2(N30),
    .ADR4(Mmux_nxt_wr_data_o29),
    .O(nxt_wr_data_o[22])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X18Y41" ),
    .INIT ( 64'hFFDDFFDDFFFFFF7F ))
  Mmux_nxt_wr_data_o293_SW1 (
    .ADR1(state_FSM_FFd4_21099),
    .ADR4(\pkt_offset<3>_0 ),
    .ADR2(\pkt_offset[4] ),
    .ADR5(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd1_21101),
    .O(N30)
  );
  X_BUF   \packet_sender/N17/packet_sender/N17_AMUX_Delay  (
    .I(N18),
    .O(N18_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X19Y38" ),
    .INIT ( 64'hFB9DFBDDFB9DFBDD ))
  Mmux_nxt_wr_data_o351_SW0 (
    .ADR1(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd4_21099),
    .ADR4(timest[10]),
    .ADR3(state_FSM_FFd3_21103),
    .ADR5(1'b1),
    .O(N17)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X19Y38" ),
    .INIT ( 32'hF99DF9DD ))
  Mmux_nxt_wr_data_o351_SW1 (
    .ADR1(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd4_21099),
    .ADR4(timest[10]),
    .ADR3(state_FSM_FFd3_21103),
    .O(N18)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X19Y40" ),
    .INIT ( 64'hFFFFFFFFFBFFFFFF ))
  Mmux_nxt_wr_data_o293_SW0 (
    .ADR0(state_FSM_FFd1_21101),
    .ADR3(\pkt_offset[4] ),
    .ADR5(\pkt_offset<3>_0 ),
    .ADR4(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd3_21103),
    .O(N29)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X19Y40" ),
    .INIT ( 64'hAAAA00A0AAAA0088 ))
  Mmux_nxt_wr_data_o312 (
    .ADR0(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd4_21099),
    .ADR2(\pkt_offset<3>_0 ),
    .ADR5(state_FSM_FFd3_21103),
    .ADR1(seqn[7]),
    .ADR4(state_FSM_FFd2_21102),
    .O(Mmux_nxt_wr_data_o311)
  );
  X_MUX2 #(
    .LOC ( "SLICE_X20Y37" ))
  Mmux_nxt_wr_data_o254 (
    .IA(N45),
    .IB(N46),
    .O(nxt_wr_data_o[20]),
    .SEL(state_FSM_FFd1_21101)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y37" ),
    .INIT ( 64'h0000500000000000 ))
  Mmux_nxt_wr_data_o254_F (
    .ADR1(1'b1),
    .ADR4(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd2_21102),
    .ADR5(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd5_21100),
    .ADR2(header_checksum[4]),
    .O(N45)
  );
  X_SFF #(
    .LOC ( "SLICE_X20Y37" ),
    .INIT ( 1'b0 ))
  wr_data_o_20 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_20/CLK ),
    .I(nxt_wr_data_o[20]),
    .O(wr_data_o[20]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y37" ),
    .INIT ( 64'hFC0CFC0CFC0CFE0C ))
  Mmux_nxt_wr_data_o254_G (
    .ADR3(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd4_21099),
    .ADR0(seqn[4]),
    .ADR4(state_FSM_FFd3_21103),
    .ADR2(state_FSM_FFd2_21102),
    .ADR1(Mmux_nxt_wr_data_o25_0),
    .O(N46)
  );
  X_BUF   \packet_sender/pkt_offset<8>/packet_sender/pkt_offset<8>_DMUX_Delay  (
    .I(N33),
    .O(N33_0)
  );
  X_BUF   \packet_sender/pkt_offset<8>/packet_sender/pkt_offset<8>_AMUX_Delay  (
    .I(\pkt_offset[3] ),
    .O(\pkt_offset<3>_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 64'hFFAFFFEFFFAFFFEF ))
  Mmux_nxt_wr_data_o223_SW0 (
    .ADR3(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd4_21099),
    .ADR1(\pkt_offset[4] ),
    .ADR4(\pkt_offset<3>_0 ),
    .ADR0(state_FSM_FFd3_21103),
    .ADR5(1'b1),
    .O(N32)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 32'hFFA5FFE5 ))
  Mmux_nxt_wr_data_o223_SW1 (
    .ADR3(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd4_21099),
    .ADR1(\pkt_offset[4] ),
    .ADR4(\pkt_offset<3>_0 ),
    .ADR0(state_FSM_FFd3_21103),
    .O(N33)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 64'h0020000000000000 ))
  _n0486_inv1 (
    .ADR4(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .ADR2(state_FSM_FFd1_21101),
    .ADR5(wr_dst_rdy_i),
    .O(_n0486_inv)
  );
  X_SFF #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 1'b0 ))
  pkt_offset_8 (
    .CE(_n0486_inv),
    .CLK(\NlwBufferSignal_packet_sender/pkt_offset_8/CLK ),
    .I(\pkt_offset[15]_pkt_offset[15]_mux_151_OUT<8> ),
    .O(\pkt_offset[8] ),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 64'hF030F030F030F030 ))
  \Mmux_pkt_offset[15]_pkt_offset[15]_mux_151_OUT151  (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR4(1'b1),
    .ADR2(\pkt_offset[15]_GND_53_o_add_141_OUT<8>_0 ),
    .ADR1(\pkt_offset[4] ),
    .ADR3(\pkt_offset<3>_0 ),
    .O(\pkt_offset[15]_pkt_offset[15]_mux_151_OUT<8> )
  );
  X_SFF #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 1'b0 ))
  pkt_offset_4 (
    .CE(_n0486_inv),
    .CLK(\NlwBufferSignal_packet_sender/pkt_offset_4/CLK ),
    .I(\pkt_offset[15]_pkt_offset[15]_mux_151_OUT<4> ),
    .O(\pkt_offset[4] ),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 64'h8888CCCC8888CCCC ))
  \Mmux_pkt_offset[15]_pkt_offset[15]_mux_151_OUT111  (
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR1(\pkt_offset[15]_GND_53_o_add_141_OUT<4>_0 ),
    .ADR4(\pkt_offset[4] ),
    .ADR0(\pkt_offset<3>_0 ),
    .ADR5(1'b1),
    .O(\pkt_offset[15]_pkt_offset[15]_mux_151_OUT<4> )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 32'hA0A0F0F0 ))
  \Mmux_pkt_offset[15]_pkt_offset[15]_mux_151_OUT101  (
    .ADR3(1'b1),
    .ADR2(\pkt_offset[15]_GND_53_o_add_141_OUT<3>_0 ),
    .ADR1(1'b1),
    .ADR4(\pkt_offset[4] ),
    .ADR0(\pkt_offset<3>_0 ),
    .O(\pkt_offset[15]_pkt_offset[15]_mux_151_OUT<3> )
  );
  X_SFF #(
    .LOC ( "SLICE_X20Y38" ),
    .INIT ( 1'b0 ))
  pkt_offset_3 (
    .CE(_n0486_inv),
    .CLK(\NlwBufferSignal_packet_sender/pkt_offset_3/CLK ),
    .I(\pkt_offset[15]_pkt_offset[15]_mux_151_OUT<3> ),
    .O(\pkt_offset[3] ),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_BUF   \packet_sender/Mmux_nxt_wr_data_o331/packet_sender/Mmux_nxt_wr_data_o331_CMUX_Delay  (
    .I(Mmux_nxt_wr_data_o331),
    .O(Mmux_nxt_wr_data_o331_0)
  );
  X_BUF   \packet_sender/Mmux_nxt_wr_data_o331/packet_sender/Mmux_nxt_wr_data_o331_BMUX_Delay  (
    .I(\pkt_offset[15]_GND_53_o_add_141_OUT<8> ),
    .O(\pkt_offset[15]_GND_53_o_add_141_OUT<8>_0 )
  );
  X_MUX2 #(
    .LOC ( "SLICE_X20Y40" ))
  Mmux_nxt_wr_data_o332 (
    .IA(N49),
    .IB(N50),
    .O(Mmux_nxt_wr_data_o331),
    .SEL(state_FSM_FFd5_21100)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y40" ),
    .INIT ( 64'h5520002055200020 ))
  Mmux_nxt_wr_data_o332_F (
    .ADR5(1'b1),
    .ADR3(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd3_21103),
    .ADR4(timest[8]),
    .ADR1(\pkt_offset<3>_0 ),
    .ADR2(\pkt_offset[4] ),
    .O(N49)
  );
  X_CARRY4 #(
    .LOC ( "SLICE_X20Y40" ))
  \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>  (
    .CI(\Madd_pkt_offset[15]_GND_53_o_add_141_OUT_cy<6>_21279 ),
    .CYINIT(1'b0),
    .CO({\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[3]_UNCONNECTED , 
\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[2]_UNCONNECTED , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[1]_UNCONNECTED , 
\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_CO[0]_UNCONNECTED }),
    .DI({\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_DI[3]_UNCONNECTED , 
\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_DI[2]_UNCONNECTED , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_DI[1]_UNCONNECTED , 1'b1
}),
    .O({\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_O[3]_UNCONNECTED , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_O[2]_UNCONNECTED 
, \pkt_offset[15]_GND_53_o_add_141_OUT<8> , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_O[0]_UNCONNECTED }),
    .S({\NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_S[3]_UNCONNECTED , \NLW_Madd_pkt_offset[15]_GND_53_o_add_141_OUT_xor<8>_S[2]_UNCONNECTED 
, \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<8> , \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<7> })
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y40" ),
    .INIT ( 64'hFF00CCF0FF00CCF0 ))
  Mmux_nxt_wr_data_o332_G (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR3(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd4_21099),
    .ADR1(\pkt_offset[8] ),
    .ADR2(seqn[8]),
    .O(N50)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y40" ),
    .INIT ( 64'h0000FFFF0000FFFF ))
  \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<8>_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR5(1'b1),
    .ADR4(\pkt_offset[8] ),
    .O(\Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<8> )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y40" ),
    .INIT ( 64'h0000FFFF0000FFFF ))
  \Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<7>_INV_0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(\pkt_offset<3>_0 ),
    .ADR5(1'b1),
    .O(\Madd_pkt_offset[15]_GND_53_o_add_141_OUT_lut<7> )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y40" ),
    .INIT ( 32'hFFFFFFFF ))
  \N0_3.A5LUT  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(\NLW_N0_3.A5LUT_O_UNCONNECTED )
  );
  X_BUF   \wr2_data<25>/wr2_data<25>_BMUX_Delay  (
    .I(N24),
    .O(N24_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y41" ),
    .INIT ( 64'hAA44A0AAAA44A000 ))
  Mmux_nxt_wr_data_o341 (
    .ADR0(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd4_21099),
    .ADR2(\pkt_offset[4] ),
    .ADR5(seqn[9]),
    .ADR1(timest[9]),
    .O(Mmux_nxt_wr_data_o34)
  );
  X_SFF #(
    .LOC ( "SLICE_X20Y41" ),
    .INIT ( 1'b0 ))
  wr_data_o_25 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_25/CLK ),
    .I(nxt_wr_data_o[25]),
    .O(wr_data_o[25]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y41" ),
    .INIT ( 64'h6662226244400040 ))
  Mmux_nxt_wr_data_o343 (
    .ADR0(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd2_21102),
    .ADR3(header_checksum[9]),
    .ADR4(N21_0),
    .ADR2(N20),
    .ADR5(Mmux_nxt_wr_data_o34),
    .O(nxt_wr_data_o[25])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y41" ),
    .INIT ( 64'hCCCCAAAACCCCAAAA ))
  Mmux_nxt_wr_data_o333_SW0 (
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .ADR5(1'b1),
    .O(N23)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X20Y41" ),
    .INIT ( 32'hCCCCEEEE ))
  Mmux_nxt_wr_data_o333_SW1 (
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR4(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .O(N24)
  );
  X_SFF #(
    .LOC ( "SLICE_X20Y41" ),
    .INIT ( 1'b0 ))
  wr_data_o_24 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_24/CLK ),
    .I(nxt_wr_data_o[24]),
    .O(wr_data_o[24]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y41" ),
    .INIT ( 64'h33003300C4C48080 ))
  Mmux_nxt_wr_data_o334 (
    .ADR5(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd2_21102),
    .ADR0(header_checksum[8]),
    .ADR2(N24_0),
    .ADR4(N23),
    .ADR3(Mmux_nxt_wr_data_o331_0),
    .O(nxt_wr_data_o[24])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X20Y42" ),
    .INIT ( 64'h4454000040500000 ))
  Mmux_nxt_wr_data_o292 (
    .ADR4(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR1(state_FSM_FFd4_21099),
    .ADR5(timest[6]),
    .ADR2(state_FSM_FFd3_21103),
    .ADR3(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ),
    .O(Mmux_nxt_wr_data_o291_21297)
  );
  X_BUF   \packet_sender/N20/packet_sender/N20_AMUX_Delay  (
    .I(N21),
    .O(N21_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X21Y41" ),
    .INIT ( 64'h8888111188881111 ))
  Mmux_nxt_wr_data_o342_SW0 (
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd4_21099),
    .ADR4(state_FSM_FFd3_21103),
    .ADR5(1'b1),
    .O(N20)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X21Y41" ),
    .INIT ( 32'h99991111 ))
  Mmux_nxt_wr_data_o342_SW1 (
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd4_21099),
    .ADR4(state_FSM_FFd3_21103),
    .O(N21)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X21Y42" ),
    .INIT ( 64'h000000A8000000EC ))
  Mmux_nxt_wr_data_o313 (
    .ADR4(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd2_21102),
    .ADR0(state_FSM_FFd4_21099),
    .ADR2(timest[7]),
    .ADR1(state_FSM_FFd3_21103),
    .ADR5(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ),
    .O(Mmux_nxt_wr_data_o312_21140)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y36" ),
    .INIT ( 64'hC0EAAA00C0C00000 ))
  Mmux_nxt_wr_data_o382 (
    .ADR4(state_FSM_FFd1_21101),
    .ADR5(Mmux_nxt_wr_data_o38),
    .ADR3(state_FSM_FFd2_21102),
    .ADR0(state_FSM_FFd4_21099),
    .ADR1(seqn[12]),
    .ADR2(Mmux_nxt_wr_data_o191_21111),
    .O(Mmux_nxt_wr_data_o381)
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y36" ),
    .INIT ( 1'b0 ))
  wr_data_o_28 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_28/CLK ),
    .I(nxt_wr_data_o[28]),
    .O(wr_data_o[28]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y36" ),
    .INIT ( 64'hFFFF0505FFFF1111 ))
  Mmux_nxt_wr_data_o384 (
    .ADR3(1'b1),
    .ADR0(state_FSM_FFd5_21100),
    .ADR5(header_checksum[12]),
    .ADR2(N15_0),
    .ADR1(N14),
    .ADR4(Mmux_nxt_wr_data_o381),
    .O(nxt_wr_data_o[28])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y38" ),
    .INIT ( 64'hC308C008FFFFFFFF ))
  Mmux_nxt_wr_data_o361 (
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd4_21099),
    .ADR4(seqn[11]),
    .ADR0(timest[11]),
    .ADR5(state_FSM_FFd1_21101),
    .O(Mmux_nxt_wr_data_o36)
  );
  X_SFF #(
    .LOC ( "SLICE_X22Y38" ),
    .INIT ( 1'b0 ))
  wr_data_o_27 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_27/CLK ),
    .I(nxt_wr_data_o[27]),
    .O(wr_data_o[27]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X22Y38" ),
    .INIT ( 64'h7030FFFF70307030 ))
  Mmux_nxt_wr_data_o363 (
    .ADR2(Mmux_nxt_wr_data_o361_21293),
    .ADR3(header_checksum[11]),
    .ADR0(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd3_21103),
    .ADR5(Mmux_nxt_wr_data_o36),
    .ADR4(state_FSM_FFd2_21102),
    .O(nxt_wr_data_o[27])
  );
  X_BUF   \packet_sender/N14/packet_sender/N14_DMUX_Delay  (
    .I(N15),
    .O(N15_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y36" ),
    .INIT ( 64'hFDCCFFC0FDCCFFC0 ))
  Mmux_nxt_wr_data_o383_SW0 (
    .ADR3(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd3_21103),
    .ADR0(timest[12]),
    .ADR4(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd2_21102),
    .ADR5(1'b1),
    .O(N14)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X23Y36" ),
    .INIT ( 32'hFDCCFF00 ))
  Mmux_nxt_wr_data_o383_SW1 (
    .ADR3(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd3_21103),
    .ADR0(timest[12]),
    .ADR4(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd2_21102),
    .O(N15)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y38" ),
    .INIT ( 64'hDDDC8080CDCC8080 ))
  Mmux_nxt_wr_data_o221 (
    .ADR4(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd4_21099),
    .ADR5(\pkt_offset<3>_0 ),
    .ADR2(state_FSM_FFd3_21103),
    .ADR3(seqn[3]),
    .ADR1(state_FSM_FFd2_21102),
    .O(Mmux_nxt_wr_data_o22)
  );
  X_SFF #(
    .LOC ( "SLICE_X23Y38" ),
    .INIT ( 1'b0 ))
  wr_data_o_19 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_19/CLK ),
    .I(nxt_wr_data_o[19]),
    .O(wr_data_o[19]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y38" ),
    .INIT ( 64'hFFAEFFBF55045515 ))
  Mmux_nxt_wr_data_o224 (
    .ADR0(state_FSM_FFd5_21100),
    .ADR3(Mmux_nxt_wr_data_o221_21291),
    .ADR1(header_checksum[3]),
    .ADR4(N32),
    .ADR2(N33_0),
    .ADR5(Mmux_nxt_wr_data_o22),
    .O(nxt_wr_data_o[19])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y39" ),
    .INIT ( 64'h4044404410440044 ))
  Mmux_nxt_wr_data_o191 (
    .ADR0(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd4_21099),
    .ADR4(header_checksum[2]),
    .ADR3(state_FSM_FFd2_21102),
    .O(Mmux_nxt_wr_data_o19)
  );
  X_SFF #(
    .LOC ( "SLICE_X23Y39" ),
    .INIT ( 1'b0 ))
  wr_data_o_18 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_18/CLK ),
    .I(nxt_wr_data_o[18]),
    .O(wr_data_o[18]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y39" ),
    .INIT ( 64'hFFFFFFF0FFFFF0F0 ))
  Mmux_nxt_wr_data_o193 (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(Mmux_nxt_wr_data_o191_21111),
    .ADR5(seqn[2]),
    .ADR2(Mmux_nxt_wr_data_o192_21296),
    .ADR4(Mmux_nxt_wr_data_o19),
    .O(nxt_wr_data_o[18])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y42" ),
    .INIT ( 64'hCCCC302000000000 ))
  Mmux_nxt_wr_data_o192 (
    .ADR5(state_FSM_FFd1_21101),
    .ADR4(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd3_21103),
    .ADR0(timest[2]),
    .O(Mmux_nxt_wr_data_o192_21296)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y42" ),
    .INIT ( 64'h2220332033333333 ))
  Mmux_nxt_wr_data_o222 (
    .ADR1(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd4_21099),
    .ADR4(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ),
    .ADR2(timest[3]),
    .ADR5(state_FSM_FFd1_21101),
    .O(Mmux_nxt_wr_data_o221_21291)
  );
  X_BUF   \packet_sender/GND_53_o_GND_53_o_OR_291_o2/packet_sender/GND_53_o_GND_53_o_OR_291_o2_CMUX_Delay  (
    .I(GND_53_o_GND_53_o_OR_291_o4_20009),
    .O(GND_53_o_GND_53_o_OR_291_o4_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y44" ),
    .INIT ( 64'h0000000000000F0F ))
  GND_53_o_GND_53_o_OR_291_o2 (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR5(packet_size_count[10]),
    .ADR2(packet_size_count[11]),
    .ADR4(packet_size_count[0]),
    .O(GND_53_o_GND_53_o_OR_291_o2_21324)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y44" ),
    .INIT ( 64'h0000001100000011 ))
  GND_53_o_GND_53_o_OR_291_o3 (
    .ADR2(1'b1),
    .ADR1(packet_size_count[4]),
    .ADR4(packet_size_count[5]),
    .ADR0(packet_size_count[3]),
    .ADR3(packet_size_count[1]),
    .ADR5(1'b1),
    .O(GND_53_o_GND_53_o_OR_291_o3_21325)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X23Y44" ),
    .INIT ( 32'h08000000 ))
  GND_53_o_GND_53_o_OR_291_o4 (
    .ADR2(packet_size_count[6]),
    .ADR1(packet_size_count[4]),
    .ADR4(packet_size_count[5]),
    .ADR0(packet_size_count[3]),
    .ADR3(packet_size_count[1]),
    .O(GND_53_o_GND_53_o_OR_291_o4_20009)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y44" ),
    .INIT ( 64'h00000000FF00FF00 ))
  \packet_length_ip2[6]_packet_length_ip1[6]_MUX_415_o<15>1  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR5(\pkt_offset<3>_0 ),
    .ADR3(\pkt_offset[4] ),
    .O(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y44" ),
    .INIT ( 64'hA000C0000000C000 ))
  GND_53_o_GND_53_o_OR_291_o5 (
    .ADR5(packet_size_count[6]),
    .ADR2(GND_53_o_GND_53_o_OR_291_o2_21324),
    .ADR0(GND_53_o_GND_53_o_OR_291_o3_21325),
    .ADR3(GND_53_o_GND_53_o_OR_291_o1_21305),
    .ADR1(GND_53_o_GND_53_o_OR_291_o4_0),
    .ADR4(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ),
    .O(GND_53_o_GND_53_o_OR_291_o)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y45" ),
    .INIT ( 64'h0001000000000000 ))
  GND_53_o_GND_53_o_OR_291_o1 (
    .ADR2(packet_size_count[9]),
    .ADR1(packet_size_count[7]),
    .ADR4(packet_size_count[2]),
    .ADR5(packet_size_count[8]),
    .ADR3(packet_size_count[13]),
    .ADR0(packet_size_count[12]),
    .O(GND_53_o_GND_53_o_OR_291_o1_21305)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X23Y45" ),
    .INIT ( 64'h2400240000000101 ))
  _n0780_inv1 (
    .ADR1(state_FSM_FFd2_21102),
    .ADR4(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd4_21099),
    .ADR2(state_FSM_FFd3_21103),
    .ADR5(state_FSM_FFd1_21101),
    .ADR3(wr_dst_rdy_i),
    .O(_n0780_inv)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y37" ),
    .INIT ( 64'hFFFFFF00FF00FF00 ))
  \seqn[15]_GND_53_o_LessThan_44_o1_SW0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR5(seqn[1]),
    .ADR4(seqn[2]),
    .ADR3(seqn[3]),
    .O(N2)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y37" ),
    .INIT ( 64'h1010103030303030 ))
  \seqn[15]_GND_53_o_LessThan_44_o1  (
    .ADR5(seqn[6]),
    .ADR4(seqn[4]),
    .ADR0(seqn[5]),
    .ADR1(seqn[12]),
    .ADR3(N2),
    .ADR2(\seqn[15]_GND_53_o_LessThan_44_o11 ),
    .O(\seqn[15]_GND_53_o_LessThan_44_o2 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y37" ),
    .INIT ( 64'hFFFFFFFF77FE77EE ))
  \state_FSM_FFd5-In3  (
    .ADR1(state_FSM_FFd2_21102),
    .ADR2(\seqn[15]_GND_53_o_LessThan_80_o21 ),
    .ADR3(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd1_21101),
    .ADR4(\seqn[15]_GND_53_o_LessThan_44_o2 ),
    .O(\state_FSM_FFd5-In3_21121 )
  );
  X_MUX2 #(
    .LOC ( "SLICE_X24Y38" ))
  header_checksum_reset_rstpot (
    .IA(N51),
    .IB(N52),
    .O(header_checksum_reset_rstpot_20059),
    .SEL(state_FSM_FFd4_21099)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y38" ),
    .INIT ( 64'hCCCCCCCCC8CCCCCC ))
  header_checksum_reset_rstpot_F (
    .ADR1(header_checksum_reset_21075),
    .ADR2(state_FSM_FFd1_21101),
    .ADR5(state_FSM_FFd3_21103),
    .ADR3(\seqn[15]_GND_53_o_LessThan_44_o2 ),
    .ADR0(N12_0),
    .ADR4(wr_dst_rdy_i),
    .O(N51)
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y38" ),
    .INIT ( 1'b1 ))
  header_checksum_reset (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/header_checksum_reset/CLK ),
    .I(header_checksum_reset_rstpot_20059),
    .O(header_checksum_reset_21075),
    .SSET(reset),
    .SET(GND),
    .RST(GND),
    .SRST(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y38" ),
    .INIT ( 64'hFFFF0000FFFF2000 ))
  header_checksum_reset_rstpot_G (
    .ADR2(state_FSM_FFd1_21101),
    .ADR3(wr_dst_rdy_i),
    .ADR0(state_FSM_FFd2_21102),
    .ADR4(header_checksum_reset_21075),
    .ADR1(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd3_21103),
    .O(N52)
  );
  X_BUF   \wr2_data<21>/wr2_data<21>_CMUX_Delay  (
    .I(Mmux_nxt_wr_data_o271),
    .O(Mmux_nxt_wr_data_o271_0)
  );
  X_BUF   \wr2_data<21>/wr2_data<21>_BMUX_Delay  (
    .I(Mmux_nxt_wr_data_o401_20082),
    .O(Mmux_nxt_wr_data_o401_0)
  );
  X_MUX2 #(
    .LOC ( "SLICE_X24Y39" ))
  Mmux_nxt_wr_data_o272 (
    .IA(N47),
    .IB(N48),
    .O(Mmux_nxt_wr_data_o271),
    .SEL(state_FSM_FFd3_21103)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y39" ),
    .INIT ( 64'h0F0FFF0FCFCF0F0F ))
  Mmux_nxt_wr_data_o272_F (
    .ADR0(1'b1),
    .ADR5(state_FSM_FFd5_21100),
    .ADR4(state_FSM_FFd4_21099),
    .ADR3(seqn[5]),
    .ADR2(state_FSM_FFd1_21101),
    .ADR1(timest[5]),
    .O(N47)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y39" ),
    .INIT ( 64'h55FFFFFF5555FF5F ))
  Mmux_nxt_wr_data_o272_G (
    .ADR1(1'b1),
    .ADR4(state_FSM_FFd5_21100),
    .ADR2(\pkt_offset[4] ),
    .ADR3(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd1_21101),
    .ADR5(\pkt_offset<3>_0 ),
    .O(N48)
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y39" ),
    .INIT ( 1'b0 ))
  wr_data_o_21 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_21/CLK ),
    .I(nxt_wr_data_o[21]),
    .O(wr_data_o[21]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y39" ),
    .INIT ( 64'hC3C0ABAAC3C0ABAA ))
  Mmux_nxt_wr_data_o274 (
    .ADR2(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd1_21101),
    .ADR4(state_FSM_FFd2_21102),
    .ADR0(Mmux_nxt_wr_data_o271_0),
    .ADR3(Mmux_nxt_wr_data_o272_21328),
    .ADR5(1'b1),
    .O(nxt_wr_data_o[21])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X24Y39" ),
    .INIT ( 32'h03030303 ))
  Mmux_nxt_wr_data_o402 (
    .ADR2(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd1_21101),
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR4(1'b1),
    .O(Mmux_nxt_wr_data_o401_20082)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y39" ),
    .INIT ( 64'h5588008855AA00AA ))
  Mmux_nxt_wr_data_o273 (
    .ADR2(1'b1),
    .ADR3(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd4_21099),
    .ADR5(\pkt_offset[4] ),
    .ADR1(\pkt_offset<3>_0 ),
    .ADR4(header_checksum[5]),
    .O(Mmux_nxt_wr_data_o272_21328)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y43" ),
    .INIT ( 64'hFF0000A8000000A8 ))
  Mmux_nxt_wr_data_o172 (
    .ADR4(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd2_21102),
    .ADR0(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .ADR2(timest[1]),
    .ADR5(state_FSM_FFd1_21101),
    .O(Mmux_nxt_wr_data_o171_21294)
  );
  X_BUF   \packet_sender/state_FSM_FFd5/packet_sender/state_FSM_FFd5_CMUX_Delay  (
    .I(\state_FSM_FFd5-In1_20116 ),
    .O(\state_FSM_FFd5-In1_0 )
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y44" ),
    .INIT ( 64'h5070E0A05070E0A0 ))
  _n0658_inv1 (
    .ADR4(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd1_21101),
    .ADR2(wr_dst_rdy_i),
    .ADR5(1'b1),
    .O(_n0658_inv)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X24Y44" ),
    .INIT ( 32'h0F0F0F0B ))
  \state_FSM_FFd5-In1  (
    .ADR4(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd1_21101),
    .ADR2(wr_dst_rdy_i),
    .O(\state_FSM_FFd5-In1_20116 )
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y44" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd5 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd5/CLK ),
    .I(\state_FSM_FFd5-In ),
    .O(state_FSM_FFd5_21100),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y44" ),
    .INIT ( 64'hFF88FF880088F088 ))
  \state_FSM_FFd5-In4  (
    .ADR1(wr_dst_rdy_i),
    .ADR2(\state_FSM_FFd5-In2_0 ),
    .ADR4(GND_53_o_GND_53_o_OR_291_o),
    .ADR3(state_FSM_FFd5_21100),
    .ADR0(\state_FSM_FFd5-In3_21121 ),
    .ADR5(\state_FSM_FFd5-In1_0 ),
    .O(\state_FSM_FFd5-In )
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y44" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd4 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd4/CLK ),
    .I(\state_FSM_FFd4-In ),
    .O(state_FSM_FFd4_21099),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y44" ),
    .INIT ( 64'hB9CC33CCFF00FF40 ))
  \state_FSM_FFd4-In1  (
    .ADR1(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd1_21101),
    .ADR3(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd2_21102),
    .ADR5(wr_dst_rdy_i),
    .O(\state_FSM_FFd4-In )
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y45" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd5_1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd5_1/CLK ),
    .I(\NlwBufferSignal_packet_sender/state_FSM_FFd5_1/IN ),
    .O(state_FSM_FFd5_1_21306),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_BUF   \packet_sender/state_FSM_FFd3/packet_sender/state_FSM_FFd3_CMUX_Delay  (
    .I(\state_FSM_FFd3-In_20150 ),
    .O(\state_FSM_FFd3-In_0 )
  );
  X_MUX2 #(
    .LOC ( "SLICE_X24Y46" ))
  \state_FSM_FFd3-In  (
    .IA(N35),
    .IB(N36),
    .O(\state_FSM_FFd3-In_20150 ),
    .SEL(GND_53_o_GND_53_o_OR_291_o)
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y46" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd3 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd3/CLK ),
    .I(\NlwBufferSignal_packet_sender/state_FSM_FFd3/IN ),
    .O(state_FSM_FFd3_21103),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y46" ),
    .INIT ( 64'h8FFFFFFFF8000000 ))
  \state_FSM_FFd3-In_F  (
    .ADR4(wr_dst_rdy_i),
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd2_21102),
    .ADR1(state_FSM_FFd1_21101),
    .O(N35)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y46" ),
    .INIT ( 64'h3CCC3CCCCCCCCCCC ))
  \state_FSM_FFd3-In_G  (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR1(state_FSM_FFd3_21103),
    .ADR3(wr_dst_rdy_i),
    .ADR2(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd4_21099),
    .O(N36)
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y46" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd2 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd2/CLK ),
    .I(\NlwBufferSignal_packet_sender/state_FSM_FFd2/IN ),
    .O(state_FSM_FFd2_21102),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y46" ),
    .INIT ( 64'h0000000005050000 ))
  Mmux_nxt_wr_data_o1911 (
    .ADR3(1'b1),
    .ADR1(1'b1),
    .ADR4(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd2_21102),
    .ADR5(state_FSM_FFd4_21099),
    .ADR2(state_FSM_FFd3_21103),
    .O(Mmux_nxt_wr_data_o191_21111)
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y46" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd1/CLK ),
    .I(\state_FSM_FFd1-In ),
    .O(state_FSM_FFd1_21101),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X24Y46" ),
    .INIT ( 64'hFF80FF00BF00FF00 ))
  \state_FSM_FFd1-In1  (
    .ADR0(state_FSM_FFd3_21103),
    .ADR5(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd2_21102),
    .ADR4(wr_dst_rdy_i),
    .O(\state_FSM_FFd1-In )
  );
  X_SFF #(
    .LOC ( "SLICE_X24Y47" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd3_1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd3_1/CLK ),
    .I(\NlwBufferSignal_packet_sender/state_FSM_FFd3_1/IN ),
    .O(state_FSM_FFd3_1_21309),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y37" ),
    .INIT ( 64'h0000000000000003 ))
  \seqn[15]_GND_53_o_LessThan_44_o111  (
    .ADR0(1'b1),
    .ADR2(seqn[11]),
    .ADR4(seqn[9]),
    .ADR5(seqn[10]),
    .ADR3(seqn[8]),
    .ADR1(seqn[7]),
    .O(\seqn[15]_GND_53_o_LessThan_44_o11 )
  );
  X_BUF   \packet_sender/seqn[15]_GND_53_o_LessThan_80_o21/packet_sender/seqn[15]_GND_53_o_LessThan_80_o21_CMUX_Delay  (
    .I(N12),
    .O(N12_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y38" ),
    .INIT ( 64'h0000030300000303 ))
  \seqn[15]_GND_53_o_LessThan_80_o211  (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR4(seqn[14]),
    .ADR1(seqn[13]),
    .ADR2(seqn[15]),
    .ADR5(1'b1),
    .O(\seqn[15]_GND_53_o_LessThan_80_o21 )
  );
  X_LUT5 #(
    .LOC ( "SLICE_X25Y38" ),
    .INIT ( 32'hFFFFFFFE ))
  _n0826_inv_SW1 (
    .ADR3(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd2_21102),
    .ADR4(seqn[14]),
    .ADR1(seqn[13]),
    .ADR2(seqn[15]),
    .O(N12)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y38" ),
    .INIT ( 64'h00FF00FF11FF15FF ))
  Mmux_nxt_wr_data_o497_SW0 (
    .ADR5(seqn[4]),
    .ADR0(seqn[3]),
    .ADR1(seqn[2]),
    .ADR4(seqn[1]),
    .ADR2(seqn[0]),
    .ADR3(seqn[6]),
    .O(N39)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y38" ),
    .INIT ( 64'h0B0B0F0B0F0F0F0F ))
  Mmux_nxt_wr_data_o497 (
    .ADR3(seqn[5]),
    .ADR0(seqn[12]),
    .ADR1(\seqn[15]_GND_53_o_LessThan_80_o21 ),
    .ADR5(\seqn[15]_GND_53_o_LessThan_44_o11 ),
    .ADR4(N39),
    .ADR2(state_FSM_FFd3_21103),
    .O(Mmux_nxt_wr_data_o496)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y43" ),
    .INIT ( 64'h04040F0F04000F0F ))
  Mmux_nxt_wr_data_o171 (
    .ADR2(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd5_21100),
    .ADR5(header_checksum[1]),
    .ADR4(state_FSM_FFd2_21102),
    .O(Mmux_nxt_wr_data_o17)
  );
  X_SFF #(
    .LOC ( "SLICE_X25Y43" ),
    .INIT ( 1'b0 ))
  wr_data_o_17 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_17/CLK ),
    .I(nxt_wr_data_o[17]),
    .O(wr_data_o[17]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y43" ),
    .INIT ( 64'hFFFFFFFFFFAAAAAA ))
  Mmux_nxt_wr_data_o173 (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR3(Mmux_nxt_wr_data_o191_21111),
    .ADR4(seqn[1]),
    .ADR0(Mmux_nxt_wr_data_o171_21294),
    .ADR5(Mmux_nxt_wr_data_o17),
    .O(nxt_wr_data_o[17])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y43" ),
    .INIT ( 64'h5544DDCC1111D1D1 ))
  Mmux_nxt_wr_data_o151 (
    .ADR5(state_FSM_FFd3_21103),
    .ADR3(header_checksum[0]),
    .ADR1(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd1_21101),
    .ADR2(timest[0]),
    .ADR4(state_FSM_FFd2_21102),
    .O(Mmux_nxt_wr_data_o15)
  );
  X_SFF #(
    .LOC ( "SLICE_X25Y43" ),
    .INIT ( 1'b0 ))
  wr_data_o_16 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_16/CLK ),
    .I(nxt_wr_data_o[16]),
    .O(wr_data_o[16]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y43" ),
    .INIT ( 64'hFFD5EAC0D5FFC0FF ))
  Mmux_nxt_wr_data_o152 (
    .ADR2(seqn[0]),
    .ADR1(Mmux_nxt_wr_data_o191_21111),
    .ADR0(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd1_21101),
    .ADR3(state_FSM_FFd2_21102),
    .ADR4(Mmux_nxt_wr_data_o15),
    .O(nxt_wr_data_o[16])
  );
  X_SFF #(
    .LOC ( "SLICE_X25Y44" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd4_1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd4_1/CLK ),
    .I(\NlwBufferSignal_packet_sender/state_FSM_FFd4_1/IN ),
    .O(state_FSM_FFd4_1_21308),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_BUF   \packet_sender/state_FSM_FFd2_1/packet_sender/state_FSM_FFd2_1_BMUX_Delay  (
    .I(N6),
    .O(N6_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y45" ),
    .INIT ( 64'hF0FFFFFFF0FFFFFF ))
  \state_FSM_FFd2-In_SW0  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd3_21103),
    .ADR3(state_FSM_FFd4_21099),
    .ADR5(1'b1),
    .O(N4)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X25Y45" ),
    .INIT ( 32'hFFFA0FFF ))
  \state_FSM_FFd2-In_SW2  (
    .ADR1(1'b1),
    .ADR0(GND_53_o_GND_53_o_OR_291_o),
    .ADR4(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd3_21103),
    .ADR3(state_FSM_FFd4_21099),
    .O(N6)
  );
  X_SFF #(
    .LOC ( "SLICE_X25Y45" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd2_1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd2_1/CLK ),
    .I(\state_FSM_FFd2-In_21106 ),
    .O(state_FSM_FFd2_1_21307),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y45" ),
    .INIT ( 64'hE4AAE400AAAAAAAA ))
  \state_FSM_FFd2-In  (
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd5_21100),
    .ADR5(wr_dst_rdy_i),
    .ADR1(N5),
    .ADR4(N4),
    .ADR2(N6_0),
    .O(\state_FSM_FFd2-In_21106 )
  );
  X_SFF #(
    .LOC ( "SLICE_X25Y46" ),
    .INIT ( 1'b0 ))
  state_FSM_FFd1_1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/state_FSM_FFd1_1/CLK ),
    .I(\NlwBufferSignal_packet_sender/state_FSM_FFd1_1/IN ),
    .O(state_FSM_FFd1_1_21336),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X25Y46" ),
    .INIT ( 64'h000000C000000000 ))
  _n0701_inv11 (
    .ADR0(1'b1),
    .ADR4(state_FSM_FFd5_1_21306),
    .ADR1(state_FSM_FFd2_1_21307),
    .ADR5(state_FSM_FFd1_1_21336),
    .ADR2(state_FSM_FFd4_1_21308),
    .ADR3(state_FSM_FFd3_1_21309),
    .O(_n0701_inv1)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y37" ),
    .INIT ( 64'hFFC0FFC0FF00FF00 ))
  Reset_OR_DriverANDClockEnable161 (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR1(_n0701_inv1),
    .ADR5(wr_dst_rdy_i),
    .ADR3(reset),
    .ADR2(GND_53_o_GND_53_o_equal_156_o_0[15]),
    .O(Reset_OR_DriverANDClockEnable16)
  );
  X_FF #(
    .LOC ( "SLICE_X26Y37" ),
    .INIT ( 1'b0 ))
  seqn_0 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_0/CLK ),
    .I(seqn_0_rstpot_20280),
    .O(seqn[0]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y37" ),
    .INIT ( 64'h0000CCFF0000CC00 ))
  seqn_0_rstpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR1(\Result<0>2_0 ),
    .ADR5(seqn[0]),
    .ADR3(_n0486_inv),
    .ADR4(Reset_OR_DriverANDClockEnable16),
    .O(seqn_0_rstpot_20280)
  );
  X_BUF   \wr2_data<29>/wr2_data<29>_AMUX_Delay  (
    .I(Mmux_nxt_wr_data_o432_20304),
    .O(Mmux_nxt_wr_data_o432_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y38" ),
    .INIT ( 64'hA00AA00000C000C0 ))
  Mmux_nxt_wr_data_o401 (
    .ADR3(state_FSM_FFd3_21103),
    .ADR2(state_FSM_FFd4_21099),
    .ADR4(seqn[13]),
    .ADR5(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd1_21101),
    .ADR1(timest[13]),
    .O(Mmux_nxt_wr_data_o40)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y38" ),
    .INIT ( 1'b0 ))
  wr_data_o_29 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_29/CLK ),
    .I(nxt_wr_data_o[29]),
    .O(wr_data_o[29]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y38" ),
    .INIT ( 64'h4404FFFF4404CCCC ))
  Mmux_nxt_wr_data_o403 (
    .ADR1(Mmux_nxt_wr_data_o401_0),
    .ADR0(state_FSM_FFd4_21099),
    .ADR2(state_FSM_FFd3_21103),
    .ADR3(header_checksum[13]),
    .ADR4(state_FSM_FFd2_21102),
    .ADR5(Mmux_nxt_wr_data_o40),
    .O(nxt_wr_data_o[29])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y38" ),
    .INIT ( 64'h0000080000000800 ))
  Mmux_nxt_wr_data_o432 (
    .ADR4(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd1_21101),
    .ADR1(timest[15]),
    .ADR2(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd4_21099),
    .ADR5(1'b1),
    .O(Mmux_nxt_wr_data_o431_21116)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X26Y38" ),
    .INIT ( 32'h00500000 ))
  Mmux_nxt_wr_data_o433 (
    .ADR4(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd1_21101),
    .ADR1(1'b1),
    .ADR2(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd4_21099),
    .O(Mmux_nxt_wr_data_o432_20304)
  );
  X_BUF   \wr2_flags<1>/wr2_flags<1>_CMUX_Delay  (
    .I(_n0701_inv),
    .O(_n0701_inv_0)
  );
  X_BUF   \wr2_flags<1>/wr2_flags<1>_BMUX_Delay  (
    .I(Mmux_nxt_wr_data_o49),
    .O(Mmux_nxt_wr_data_o49_0)
  );
  X_BUF   \wr2_flags<1>/wr2_flags<1>_AMUX_Delay  (
    .I(\state_FSM_FFd5-In2_20311 ),
    .O(\state_FSM_FFd5-In2_0 )
  );
  X_MUX2 #(
    .LOC ( "SLICE_X26Y44" ))
  _n0701_inv3 (
    .IA(N53),
    .IB(N54),
    .O(_n0701_inv),
    .SEL(GND_53_o_GND_53_o_OR_291_o)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 64'h0000080040404040 ))
  _n0701_inv3_F (
    .ADR1(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd1_21101),
    .ADR3(wr_dst_rdy_i),
    .ADR4(state_FSM_FFd5_21100),
    .O(N53)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 64'h1000400000880088 ))
  _n0701_inv3_G (
    .ADR2(wr_dst_rdy_i),
    .ADR0(state_FSM_FFd3_21103),
    .ADR5(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd1_21101),
    .ADR4(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd4_21099),
    .O(N54)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 1'b0 ))
  wr_flags_o_1 (
    .CE(_n0701_inv_0),
    .CLK(\NlwBufferSignal_packet_sender/wr_flags_o_1/CLK ),
    .I(nxt_wr_flags_o[1]),
    .O(wr_flags_o[1]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 64'hAF0F0000AF0F0000 ))
  \state_nxt_wr_flags_o<1>1  (
    .ADR1(1'b1),
    .ADR4(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd4_21099),
    .ADR5(1'b1),
    .O(nxt_wr_flags_o[1])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 32'h40000808 ))
  Mmux_nxt_wr_data_o491 (
    .ADR1(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd4_21099),
    .O(Mmux_nxt_wr_data_o49)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 1'b0 ))
  wr_flags_o_0 (
    .CE(_n0701_inv_0),
    .CLK(\NlwBufferSignal_packet_sender/wr_flags_o_0/CLK ),
    .I(nxt_wr_flags_o[0]),
    .O(wr_flags_o[0]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 64'h4400555544005555 ))
  \state_nxt_wr_flags_o<0>1  (
    .ADR2(1'b1),
    .ADR0(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd2_21102),
    .ADR1(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd1_21101),
    .ADR5(1'b1),
    .O(nxt_wr_flags_o[0])
  );
  X_LUT5 #(
    .LOC ( "SLICE_X26Y44" ),
    .INIT ( 32'hFF000000 ))
  \state_FSM_FFd5-In2  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(state_FSM_FFd2_21102),
    .ADR2(1'b1),
    .ADR4(state_FSM_FFd1_21101),
    .O(\state_FSM_FFd5-In2_20311 )
  );
  X_BUF   \packet_sender/Mmux_nxt_wr_data_o361/packet_sender/Mmux_nxt_wr_data_o361_BMUX_Delay  (
    .I(\packet_sender/Mmux_nxt_wr_data_o361/wr2_data [2]),
    .O(wr_data_o[2])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y45" ),
    .INIT ( 64'h0033003300330033 ))
  Mmux_nxt_wr_data_o362 (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR1(state_FSM_FFd1_21101),
    .ADR3(state_FSM_FFd4_21099),
    .ADR5(1'b1),
    .O(Mmux_nxt_wr_data_o361_21293)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X26Y45" ),
    .INIT ( 32'hEAEAAAAA ))
  Mmux_nxt_wr_data_o412 (
    .ADR4(Mmux_nxt_wr_data_o191_21111),
    .ADR2(timest[18]),
    .ADR0(Mmux_nxt_wr_data_o41),
    .ADR1(state_FSM_FFd1_21101),
    .ADR3(1'b1),
    .O(nxt_wr_data_o[2])
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y45" ),
    .INIT ( 1'b0 ))
  wr_data_o_2 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_2/CLK ),
    .I(nxt_wr_data_o[2]),
    .O(\packet_sender/Mmux_nxt_wr_data_o361/wr2_data [2]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y45" ),
    .INIT ( 64'h00C00020F0330333 ))
  Mmux_nxt_wr_data_o411 (
    .ADR2(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd1_21101),
    .ADR3(state_FSM_FFd2_21102),
    .ADR1(state_FSM_FFd5_21100),
    .ADR0(line_cnt[2]),
    .O(Mmux_nxt_wr_data_o41)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 1'b0 ))
  timest_14 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_14/CLK ),
    .I(timest_14_dpot_20368),
    .O(timest[14]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 64'hF0FFFFFFF0000000 ))
  timest_14_dpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[14]),
    .ADR3(wr_dst_rdy_i),
    .ADR2(\Result<14>3_0 ),
    .O(timest_14_dpot_20368)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 1'b0 ))
  timest_13 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_13/CLK ),
    .I(timest_13_dpot_20376),
    .O(timest[13]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 64'hFAFAAAAA0A0AAAAA ))
  timest_13_dpot (
    .ADR3(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR0(timest[13]),
    .ADR2(wr_dst_rdy_i),
    .ADR5(\Result<13>3_0 ),
    .O(timest_13_dpot_20376)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 1'b0 ))
  timest_12 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_12/CLK ),
    .I(timest_12_dpot_20382),
    .O(timest[12]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 64'hCCFFCC00FFFF0000 ))
  timest_12_dpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR4(timest[12]),
    .ADR5(wr_dst_rdy_i),
    .ADR1(\Result<12>3_0 ),
    .O(timest_12_dpot_20382)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 1'b0 ))
  timest_11 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_11/CLK ),
    .I(timest_11_dpot_20388),
    .O(timest[11]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y46" ),
    .INIT ( 64'hFF33FFFFCC000000 ))
  timest_11_dpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[11]),
    .ADR1(wr_dst_rdy_i),
    .ADR3(\Result<11>3_0 ),
    .O(timest_11_dpot_20388)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y48" ),
    .INIT ( 64'h4141030305010101 ))
  Mmux_nxt_wr_data_o231 (
    .ADR2(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR4(state_FSM_FFd3_21103),
    .ADR5(state_FSM_FFd4_21099),
    .ADR3(line_cnt[1]),
    .O(Mmux_nxt_wr_data_o23)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y48" ),
    .INIT ( 1'b0 ))
  wr_data_o_1 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_1/CLK ),
    .I(nxt_wr_data_o[1]),
    .O(wr_data_o[1]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y48" ),
    .INIT ( 64'hFFC0FFC0FF00FF00 ))
  Mmux_nxt_wr_data_o232 (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR5(Mmux_nxt_wr_data_o191_21111),
    .ADR2(state_FSM_FFd1_21101),
    .ADR1(timest[17]),
    .ADR3(Mmux_nxt_wr_data_o23),
    .O(nxt_wr_data_o[1])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y48" ),
    .INIT ( 64'h000B9303000B8303 ))
  Mmux_nxt_wr_data_o11 (
    .ADR3(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd4_21099),
    .ADR5(line_cnt[0]),
    .ADR4(state_FSM_FFd2_21102),
    .O(Mmux_nxt_wr_data_o1)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y48" ),
    .INIT ( 1'b0 ))
  wr_data_o_0 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_0/CLK ),
    .I(nxt_wr_data_o[0]),
    .O(wr_data_o[0]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y48" ),
    .INIT ( 64'hFFFFC0C0FFFF0000 ))
  Mmux_nxt_wr_data_o12 (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(Mmux_nxt_wr_data_o191_21111),
    .ADR5(state_FSM_FFd1_21101),
    .ADR1(timest[16]),
    .ADR4(Mmux_nxt_wr_data_o1),
    .O(nxt_wr_data_o[0])
  );
  X_MUX2 #(
    .LOC ( "SLICE_X26Y49" ))
  Mmux_nxt_wr_data_o443 (
    .IA(N43),
    .IB(N44),
    .O(nxt_wr_data_o[3]),
    .SEL(state_FSM_FFd2_21102)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y49" ),
    .INIT ( 64'hFF000FA00F00CFF0 ))
  Mmux_nxt_wr_data_o443_F (
    .ADR3(state_FSM_FFd5_21100),
    .ADR4(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd3_21103),
    .ADR1(timest[19]),
    .ADR2(state_FSM_FFd1_21101),
    .ADR0(line_cnt[3]),
    .O(N43)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y49" ),
    .INIT ( 1'b0 ))
  wr_data_o_3 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_3/CLK ),
    .I(nxt_wr_data_o[3]),
    .O(wr_data_o[3]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y49" ),
    .INIT ( 64'h5155515500440044 ))
  Mmux_nxt_wr_data_o443_G (
    .ADR4(1'b1),
    .ADR0(state_FSM_FFd1_21101),
    .ADR5(state_FSM_FFd3_21103),
    .ADR2(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ),
    .ADR1(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd5_21100),
    .O(N44)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y49" ),
    .INIT ( 64'h1311020233110202 ))
  Mmux_nxt_wr_data_o451 (
    .ADR1(state_FSM_FFd1_21101),
    .ADR4(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd4_21099),
    .ADR5(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ),
    .ADR2(state_FSM_FFd3_21103),
    .O(Mmux_nxt_wr_data_o45)
  );
  X_SFF #(
    .LOC ( "SLICE_X26Y49" ),
    .INIT ( 1'b0 ))
  wr_data_o_4 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_4/CLK ),
    .I(nxt_wr_data_o[4]),
    .O(wr_data_o[4]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X26Y49" ),
    .INIT ( 64'hFFFFFAF0FFFFFAF0 ))
  Mmux_nxt_wr_data_o453 (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR3(Mmux_nxt_wr_data_o191_21111),
    .ADR0(timest[20]),
    .ADR2(Mmux_nxt_wr_data_o451_21300),
    .ADR4(Mmux_nxt_wr_data_o45),
    .O(nxt_wr_data_o[4])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y36" ),
    .INIT ( 64'h00FF00FF00000000 ))
  Mmux_nxt_wr_data_o422 (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(1'b1),
    .ADR4(1'b1),
    .ADR3(state_FSM_FFd2_21102),
    .ADR5(Mmux_nxt_wr_data_o42),
    .O(Mmux_nxt_wr_data_o421_21343)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y36" ),
    .INIT ( 1'b0 ))
  wr_data_o_30 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_30/CLK ),
    .I(nxt_wr_data_o[30]),
    .O(wr_data_o[30]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y36" ),
    .INIT ( 64'hFF00FF00FF10FF00 ))
  Mmux_nxt_wr_data_o423 (
    .ADR0(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd4_21099),
    .ADR2(state_FSM_FFd3_21103),
    .ADR5(state_FSM_FFd1_21101),
    .ADR4(header_checksum[14]),
    .ADR3(Mmux_nxt_wr_data_o421_21343),
    .O(nxt_wr_data_o[30])
  );
  X_FF #(
    .LOC ( "SLICE_X27Y37" ),
    .INIT ( 1'b0 ))
  seqn_13 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_13/CLK ),
    .I(seqn_13_rstpot_20470),
    .O(seqn[13]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y37" ),
    .INIT ( 64'h5555550000005500 ))
  seqn_13_rstpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR5(\Result<13>2_0 ),
    .ADR3(seqn[13]),
    .ADR4(_n0486_inv),
    .ADR0(Reset_OR_DriverANDClockEnable16),
    .O(seqn_13_rstpot_20470)
  );
  X_FF #(
    .LOC ( "SLICE_X27Y37" ),
    .INIT ( 1'b0 ))
  seqn_12 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_12/CLK ),
    .I(seqn_12_rstpot_20476),
    .O(seqn[12]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y37" ),
    .INIT ( 64'h0000CCF00000CCF0 ))
  seqn_12_rstpot (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR1(\Result<12>2_0 ),
    .ADR2(seqn[12]),
    .ADR3(_n0486_inv),
    .ADR4(Reset_OR_DriverANDClockEnable16),
    .O(seqn_12_rstpot_20476)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y38" ),
    .INIT ( 64'h2133000020330000 ))
  Mmux_nxt_wr_data_o431 (
    .ADR1(state_FSM_FFd2_21102),
    .ADR4(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd4_21099),
    .ADR2(state_FSM_FFd3_21103),
    .ADR5(seqn[15]),
    .ADR3(state_FSM_FFd1_21101),
    .O(Mmux_nxt_wr_data_o43)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y38" ),
    .INIT ( 1'b0 ))
  wr_data_o_31 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_31/CLK ),
    .I(nxt_wr_data_o[31]),
    .O(wr_data_o[31]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y38" ),
    .INIT ( 64'hFF00FF00FFFAFFF0 ))
  Mmux_nxt_wr_data_o434 (
    .ADR1(1'b1),
    .ADR5(state_FSM_FFd5_21100),
    .ADR2(Mmux_nxt_wr_data_o431_21116),
    .ADR4(Mmux_nxt_wr_data_o432_0),
    .ADR0(header_checksum[15]),
    .ADR3(Mmux_nxt_wr_data_o43),
    .O(nxt_wr_data_o[31])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y38" ),
    .INIT ( 64'hDDDD75755555DD55 ))
  Mmux_nxt_wr_data_o421 (
    .ADR2(timest[14]),
    .ADR3(seqn[14]),
    .ADR1(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd4_21099),
    .ADR4(state_FSM_FFd3_21103),
    .ADR0(state_FSM_FFd1_21101),
    .O(Mmux_nxt_wr_data_o42)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 1'b0 ))
  timest_6 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_6/CLK ),
    .I(timest_6_dpot_20507),
    .O(timest[6]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 64'hFF0FFFFFF0000000 ))
  timest_6_dpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[6]),
    .ADR2(wr_dst_rdy_i),
    .ADR3(\Result<6>3_0 ),
    .O(timest_6_dpot_20507)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 1'b0 ))
  timest_5 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_5/CLK ),
    .I(timest_5_dpot_20515),
    .O(timest[5]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 64'hAAF0F0F0AAF0F0F0 ))
  timest_5_dpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR2(timest[5]),
    .ADR3(wr_dst_rdy_i),
    .ADR0(\Result<5>3_0 ),
    .O(timest_5_dpot_20515)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 1'b0 ))
  timest_4 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_4/CLK ),
    .I(timest_4_dpot_20521),
    .O(timest[4]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 64'hFF00F0F0F0F0F0F0 ))
  timest_4_dpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR2(timest[4]),
    .ADR5(wr_dst_rdy_i),
    .ADR3(\Result<4>3_0 ),
    .O(timest_4_dpot_20521)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 1'b0 ))
  timest_3 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_3/CLK ),
    .I(timest_3_dpot_20527),
    .O(timest[3]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y44" ),
    .INIT ( 64'hCCFFFFFFCC000000 ))
  timest_3_dpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[3]),
    .ADR4(wr_dst_rdy_i),
    .ADR1(\Result<3>3_0 ),
    .O(timest_3_dpot_20527)
  );
  X_BUF   \wr2_data<8>/wr2_data<8>_BMUX_Delay  (
    .I(N37),
    .O(N37_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y45" ),
    .INIT ( 64'h00FF555000FF4400 ))
  Mmux_nxt_wr_data_o511 (
    .ADR4(state_FSM_FFd2_21102),
    .ADR0(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd3_21103),
    .ADR1(timest[24]),
    .ADR2(line_cnt[8]),
    .O(Mmux_nxt_wr_data_o51)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y45" ),
    .INIT ( 1'b0 ))
  wr_data_o_8 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_8/CLK ),
    .I(nxt_wr_data_o[8]),
    .O(wr_data_o[8]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y45" ),
    .INIT ( 64'hFBF1F1FB0B01010B ))
  Mmux_nxt_wr_data_o512 (
    .ADR2(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd2_21102),
    .ADR4(state_FSM_FFd4_21099),
    .ADR3(state_FSM_FFd3_21103),
    .ADR1(state_FSM_FFd5_21100),
    .ADR5(Mmux_nxt_wr_data_o51),
    .O(nxt_wr_data_o[8])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y45" ),
    .INIT ( 64'hF0F00000F0F00000 ))
  \state_FSM_FFd2-In_SW1  (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(1'b1),
    .ADR2(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd4_21099),
    .ADR5(1'b1),
    .O(N5)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X27Y45" ),
    .INIT ( 32'hFFFFFF15 ))
  Mmux_nxt_wr_data_o498_SW0 (
    .ADR1(line_cnt[7]),
    .ADR3(state_FSM_FFd2_21102),
    .ADR0(Mmux_nxt_wr_data_o496),
    .ADR2(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd4_21099),
    .O(N37)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y45" ),
    .INIT ( 1'b0 ))
  wr_data_o_7 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_7/CLK ),
    .I(nxt_wr_data_o[7]),
    .O(wr_data_o[7]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y45" ),
    .INIT ( 64'hEECCEFCFCCCCFFFF ))
  Mmux_nxt_wr_data_o498 (
    .ADR3(timest[23]),
    .ADR4(state_FSM_FFd5_21100),
    .ADR5(state_FSM_FFd1_21101),
    .ADR1(Mmux_nxt_wr_data_o49_0),
    .ADR0(Mmux_nxt_wr_data_o191_21111),
    .ADR2(N37_0),
    .O(nxt_wr_data_o[7])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y46" ),
    .INIT ( 64'h5000004550000000 ))
  Mmux_nxt_wr_data_o452 (
    .ADR0(state_FSM_FFd2_21102),
    .ADR3(state_FSM_FFd5_21100),
    .ADR4(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd1_21101),
    .ADR1(line_cnt[4]),
    .ADR2(state_FSM_FFd3_21103),
    .O(Mmux_nxt_wr_data_o451_21300)
  );
  X_BUF   \wr2_src_rdy/wr2_src_rdy_DMUX_Delay  (
    .I(N41),
    .O(N41_0)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y48" ),
    .INIT ( 64'h00C2000000C20000 ))
  Mmux_nxt_wr_data_o463 (
    .ADR3(state_FSM_FFd2_21102),
    .ADR4(state_FSM_FFd3_21103),
    .ADR2(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd4_21099),
    .ADR0(line_cnt[5]),
    .ADR5(1'b1),
    .O(Mmux_nxt_wr_data_o462_21147)
  );
  X_LUT5 #(
    .LOC ( "SLICE_X27Y48" ),
    .INIT ( 32'h0C0C0C0C ))
  wr_src_rdy_o_rstpot_SW0 (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR2(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd4_21099),
    .ADR4(1'b1),
    .O(N41)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y48" ),
    .INIT ( 1'b0 ))
  wr_src_rdy_o_4547 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/wr_src_rdy_o/CLK ),
    .I(wr_src_rdy_o_rstpot_20583),
    .O(NlwRenamedSig_OI_wr_src_rdy_o),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y48" ),
    .INIT ( 64'hFFFF05007FFF0000 ))
  wr_src_rdy_o_rstpot (
    .ADR3(N41_0),
    .ADR1(wr_dst_rdy_i),
    .ADR0(state_FSM_FFd1_21101),
    .ADR4(NlwRenamedSig_OI_wr_src_rdy_o),
    .ADR2(state_FSM_FFd2_21102),
    .ADR5(state_FSM_FFd3_21103),
    .O(wr_src_rdy_o_rstpot_20583)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y49" ),
    .INIT ( 1'b0 ))
  wr_data_o_6 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_6/CLK ),
    .I(nxt_wr_data_o[6]),
    .O(wr_data_o[6]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y49" ),
    .INIT ( 64'h5050505200000002 ))
  Mmux_nxt_wr_data_o472 (
    .ADR0(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd1_21101),
    .ADR4(state_FSM_FFd3_21103),
    .ADR3(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd5_21100),
    .ADR5(Mmux_nxt_wr_data_o47_0),
    .O(nxt_wr_data_o[6])
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y49" ),
    .INIT ( 1'b0 ))
  wr_data_o_5 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_5/CLK ),
    .I(nxt_wr_data_o[5]),
    .O(wr_data_o[5]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y49" ),
    .INIT ( 64'hFFFFFFFFAAAA8800 ))
  Mmux_nxt_wr_data_o464 (
    .ADR2(1'b1),
    .ADR0(state_FSM_FFd1_21101),
    .ADR3(Mmux_nxt_wr_data_o191_21111),
    .ADR1(timest[21]),
    .ADR4(Mmux_nxt_wr_data_o462_21147),
    .ADR5(Mmux_nxt_wr_data_o461),
    .O(nxt_wr_data_o[5])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y49" ),
    .INIT ( 64'h4040044000400440 ))
  Mmux_nxt_wr_data_o462 (
    .ADR0(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd3_21103),
    .ADR4(state_FSM_FFd5_21100),
    .ADR5(\packet_length_ip2[5]_packet_length_ip1[5]_MUX_416_o_inv ),
    .ADR3(state_FSM_FFd4_21099),
    .O(Mmux_nxt_wr_data_o461)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y52" ),
    .INIT ( 1'b0 ))
  wr_data_o_11 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_11/CLK ),
    .I(nxt_wr_data_o[11]),
    .O(wr_data_o[11]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y52" ),
    .INIT ( 64'h0C0C07073C2C0303 ))
  Mmux_nxt_wr_data_o52 (
    .ADR4(state_FSM_FFd1_21101),
    .ADR5(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd5_21100),
    .ADR3(timest[27]),
    .ADR0(state_FSM_FFd3_21103),
    .O(nxt_wr_data_o[11])
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y52" ),
    .INIT ( 1'b0 ))
  wr_data_o_10 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_10/CLK ),
    .I(nxt_wr_data_o[10]),
    .O(wr_data_o[10]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y52" ),
    .INIT ( 64'h5A000A000AA05A80 ))
  Mmux_nxt_wr_data_o32 (
    .ADR2(state_FSM_FFd5_21100),
    .ADR0(state_FSM_FFd1_21101),
    .ADR3(state_FSM_FFd2_21102),
    .ADR5(state_FSM_FFd4_21099),
    .ADR4(state_FSM_FFd3_21103),
    .ADR1(timest[26]),
    .O(nxt_wr_data_o[10])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y52" ),
    .INIT ( 64'h4747464446464644 ))
  Mmux_nxt_wr_data_o541 (
    .ADR1(state_FSM_FFd2_21102),
    .ADR2(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd5_21100),
    .ADR4(state_FSM_FFd3_21103),
    .ADR3(timest[25]),
    .ADR5(line_cnt[9]),
    .O(Mmux_nxt_wr_data_o54)
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y52" ),
    .INIT ( 1'b0 ))
  wr_data_o_9 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_9/CLK ),
    .I(nxt_wr_data_o[9]),
    .O(wr_data_o[9]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y52" ),
    .INIT ( 64'hCCCC0000FDFF3133 ))
  Mmux_nxt_wr_data_o542 (
    .ADR1(state_FSM_FFd1_21101),
    .ADR5(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd3_21103),
    .ADR2(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd2_21102),
    .ADR4(Mmux_nxt_wr_data_o54),
    .O(nxt_wr_data_o[9])
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 1'b0 ))
  wr_data_o_15 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_15/CLK ),
    .I(nxt_wr_data_o[15]),
    .O(wr_data_o[15]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 64'h14C714C710DB00DB ))
  Mmux_nxt_wr_data_o131 (
    .ADR2(state_FSM_FFd1_21101),
    .ADR0(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd3_21103),
    .ADR3(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd2_21102),
    .ADR4(timest[31]),
    .O(nxt_wr_data_o[15])
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 1'b0 ))
  wr_data_o_14 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_14/CLK ),
    .I(nxt_wr_data_o[14]),
    .O(wr_data_o[14]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 64'h14C714C71CC70CC7 ))
  Mmux_nxt_wr_data_o111 (
    .ADR2(state_FSM_FFd1_21101),
    .ADR3(state_FSM_FFd5_21100),
    .ADR1(state_FSM_FFd2_21102),
    .ADR0(state_FSM_FFd4_21099),
    .ADR5(state_FSM_FFd3_21103),
    .ADR4(timest[30]),
    .O(nxt_wr_data_o[14])
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 1'b0 ))
  wr_data_o_13 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_13/CLK ),
    .I(nxt_wr_data_o[13]),
    .O(wr_data_o[13]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 64'h0202020088C888C8 ))
  Mmux_nxt_wr_data_o91 (
    .ADR0(state_FSM_FFd1_21101),
    .ADR2(state_FSM_FFd4_21099),
    .ADR1(state_FSM_FFd2_21102),
    .ADR5(state_FSM_FFd5_21100),
    .ADR3(state_FSM_FFd3_21103),
    .ADR4(timest[29]),
    .O(nxt_wr_data_o[13])
  );
  X_SFF #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 1'b0 ))
  wr_data_o_12 (
    .CE(_n0658_inv),
    .CLK(\NlwBufferSignal_packet_sender/wr_data_o_12/CLK ),
    .I(nxt_wr_data_o[12]),
    .O(wr_data_o[12]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X27Y53" ),
    .INIT ( 64'h1110AAAA00008282 ))
  Mmux_nxt_wr_data_o71 (
    .ADR5(state_FSM_FFd1_21101),
    .ADR1(state_FSM_FFd4_21099),
    .ADR0(state_FSM_FFd2_21102),
    .ADR4(state_FSM_FFd5_21100),
    .ADR2(state_FSM_FFd3_21103),
    .ADR3(timest[28]),
    .O(nxt_wr_data_o[12])
  );
  X_SFF #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 1'b0 ))
  timest_30 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_30/CLK ),
    .I(timest_30_dpot_20687),
    .O(timest[30]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 64'hCFFFCFFFC000C000 ))
  timest_30_dpot (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[30]),
    .ADR2(wr_dst_rdy_i),
    .ADR1(\Result<30>_0 ),
    .O(timest_30_dpot_20687)
  );
  X_SFF #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 1'b0 ))
  timest_29 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_29/CLK ),
    .I(timest_29_dpot_20695),
    .O(timest[29]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 64'hFFFFF0000FFF0000 ))
  timest_29_dpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR4(timest[29]),
    .ADR2(wr_dst_rdy_i),
    .ADR5(\Result<29>_0 ),
    .O(timest_29_dpot_20695)
  );
  X_SFF #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 1'b0 ))
  timest_28 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_28/CLK ),
    .I(timest_28_dpot_20701),
    .O(timest[28]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 64'hFF00F0F0F0F0F0F0 ))
  timest_28_dpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR2(timest[28]),
    .ADR5(wr_dst_rdy_i),
    .ADR3(\Result<28>_0 ),
    .O(timest_28_dpot_20701)
  );
  X_SFF #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 1'b0 ))
  timest_27 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_27/CLK ),
    .I(timest_27_dpot_20707),
    .O(timest[27]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X28Y52" ),
    .INIT ( 64'hFFFFC0C03F3F0000 ))
  timest_27_dpot (
    .ADR0(1'b1),
    .ADR3(1'b1),
    .ADR1(GND_53_o_GND_53_o_equal_156_o),
    .ADR4(timest[27]),
    .ADR2(wr_dst_rdy_i),
    .ADR5(\Result<27>_0 ),
    .O(timest_27_dpot_20707)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y35" ),
    .INIT ( 1'b0 ))
  seqn_2 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_2/CLK ),
    .I(seqn_2_rstpot_20730),
    .O(seqn[2]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y35" ),
    .INIT ( 64'h00CC00FF00CC0000 ))
  seqn_2_rstpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR1(\Result<2>2_0 ),
    .ADR5(seqn[2]),
    .ADR4(_n0486_inv),
    .ADR3(Reset_OR_DriverANDClockEnable16),
    .O(seqn_2_rstpot_20730)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y35" ),
    .INIT ( 1'b0 ))
  seqn_1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_1/CLK ),
    .I(seqn_1_rstpot_20718),
    .O(seqn[1]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y35" ),
    .INIT ( 64'h00AA00F000AA00F0 ))
  seqn_1_rstpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR0(\Result<1>2_0 ),
    .ADR2(seqn[1]),
    .ADR4(_n0486_inv),
    .ADR3(Reset_OR_DriverANDClockEnable16),
    .O(seqn_1_rstpot_20718)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y35" ),
    .INIT ( 1'b0 ))
  seqn_3 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_3/CLK ),
    .I(seqn_3_rstpot_20724),
    .O(seqn[3]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y35" ),
    .INIT ( 64'h0000AAFF0000AA00 ))
  seqn_3_rstpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR0(\Result<3>2_0 ),
    .ADR5(seqn[3]),
    .ADR3(_n0486_inv),
    .ADR4(Reset_OR_DriverANDClockEnable16),
    .O(seqn_3_rstpot_20724)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 1'b0 ))
  seqn_6 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_6/CLK ),
    .I(seqn_6_rstpot_20732),
    .O(seqn[6]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 64'h00CC00FF00CC0000 ))
  seqn_6_rstpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR1(\Result<6>2_0 ),
    .ADR5(seqn[6]),
    .ADR4(_n0486_inv),
    .ADR3(Reset_OR_DriverANDClockEnable16),
    .O(seqn_6_rstpot_20732)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 1'b0 ))
  seqn_5 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_5/CLK ),
    .I(seqn_5_rstpot_20739),
    .O(seqn[5]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 64'h00AA00F000AA00F0 ))
  seqn_5_rstpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR0(\Result<5>2_0 ),
    .ADR2(seqn[5]),
    .ADR4(_n0486_inv),
    .ADR3(Reset_OR_DriverANDClockEnable16),
    .O(seqn_5_rstpot_20739)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 1'b0 ))
  seqn_4 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_4/CLK ),
    .I(seqn_4_rstpot_20745),
    .O(seqn[4]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 64'h0000AAF00000AAF0 ))
  seqn_4_rstpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR0(\Result<4>2_0 ),
    .ADR2(seqn[4]),
    .ADR3(_n0486_inv),
    .ADR4(Reset_OR_DriverANDClockEnable16),
    .O(seqn_4_rstpot_20745)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 1'b0 ))
  seqn_7 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_7/CLK ),
    .I(seqn_7_rstpot_20751),
    .O(seqn[7]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y36" ),
    .INIT ( 64'h0000AAFF0000AA00 ))
  seqn_7_rstpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR0(\Result<7>2_0 ),
    .ADR5(seqn[7]),
    .ADR3(_n0486_inv),
    .ADR4(Reset_OR_DriverANDClockEnable16),
    .O(seqn_7_rstpot_20751)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 1'b0 ))
  seqn_10 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_10/CLK ),
    .I(seqn_10_rstpot_20757),
    .O(seqn[10]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 64'h0000CCCC0000FF00 ))
  seqn_10_rstpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR1(\Result<10>2_0 ),
    .ADR3(seqn[10]),
    .ADR5(_n0486_inv),
    .ADR4(Reset_OR_DriverANDClockEnable16),
    .O(seqn_10_rstpot_20757)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 1'b0 ))
  seqn_9 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_9/CLK ),
    .I(seqn_9_rstpot_20764),
    .O(seqn[9]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 64'h00000000F5F5A0A0 ))
  seqn_9_rstpot (
    .ADR3(1'b1),
    .ADR1(1'b1),
    .ADR2(\Result<9>2_0 ),
    .ADR4(seqn[9]),
    .ADR0(_n0486_inv),
    .ADR5(Reset_OR_DriverANDClockEnable16),
    .O(seqn_9_rstpot_20764)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 1'b0 ))
  seqn_8 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_8/CLK ),
    .I(seqn_8_rstpot_20770),
    .O(seqn[8]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 64'h00AF00A000AF00A0 ))
  seqn_8_rstpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR0(\Result<8>2_0 ),
    .ADR4(seqn[8]),
    .ADR2(_n0486_inv),
    .ADR3(Reset_OR_DriverANDClockEnable16),
    .O(seqn_8_rstpot_20770)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 1'b0 ))
  seqn_11 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_11/CLK ),
    .I(seqn_11_rstpot_20776),
    .O(seqn[11]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y37" ),
    .INIT ( 64'h00AF00AF00A000A0 ))
  seqn_11_rstpot (
    .ADR4(1'b1),
    .ADR1(1'b1),
    .ADR0(\Result<11>2_0 ),
    .ADR5(seqn[11]),
    .ADR2(_n0486_inv),
    .ADR3(Reset_OR_DriverANDClockEnable16),
    .O(seqn_11_rstpot_20776)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y38" ),
    .INIT ( 1'b0 ))
  seqn_15 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_15/CLK ),
    .I(seqn_15_rstpot_20783),
    .O(seqn[15]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y38" ),
    .INIT ( 64'h2323202023232020 ))
  seqn_15_rstpot (
    .ADR3(1'b1),
    .ADR5(1'b1),
    .ADR0(\Result<15>2_0 ),
    .ADR4(seqn[15]),
    .ADR2(_n0486_inv),
    .ADR1(Reset_OR_DriverANDClockEnable161_21286),
    .O(seqn_15_rstpot_20783)
  );
  X_FF #(
    .LOC ( "SLICE_X29Y38" ),
    .INIT ( 1'b0 ))
  seqn_14 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/seqn_14/CLK ),
    .I(seqn_14_rstpot_20789),
    .O(seqn[14]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y38" ),
    .INIT ( 64'h00000000DDDD8888 ))
  seqn_14_rstpot (
    .ADR3(1'b1),
    .ADR2(1'b1),
    .ADR1(\Result<14>2_0 ),
    .ADR4(seqn[14]),
    .ADR0(_n0486_inv),
    .ADR5(Reset_OR_DriverANDClockEnable161_21286),
    .O(seqn_14_rstpot_20789)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y38" ),
    .INIT ( 64'hFFAAFF00FF00FF00 ))
  Reset_OR_DriverANDClockEnable161_1 (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR5(_n0701_inv1),
    .ADR4(wr_dst_rdy_i),
    .ADR3(reset),
    .ADR0(GND_53_o_GND_53_o_equal_156_o_0[15]),
    .O(Reset_OR_DriverANDClockEnable161_21286)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y41" ),
    .INIT ( 64'hAAAA000000880000 ))
  _n0495_inv1 (
    .ADR2(1'b1),
    .ADR1(\pkt_offset[4] ),
    .ADR3(\pkt_offset<3>_0 ),
    .ADR4(_n0701_inv1),
    .ADR0(wr_dst_rdy_i),
    .ADR5(GND_53_o_GND_53_o_equal_156_o_0[15]),
    .O(_n0495_inv)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y41" ),
    .INIT ( 64'h0000000000000080 ))
  \GND_53_o_GND_53_o_equal_156_o<15>_1  (
    .ADR1(line_cnt[7]),
    .ADR0(line_cnt[6]),
    .ADR5(line_cnt[4]),
    .ADR4(line_cnt[5]),
    .ADR2(line_cnt[2]),
    .ADR3(N01),
    .O(GND_53_o_GND_53_o_equal_156_o_0[15])
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y42" ),
    .INIT ( 64'hF5FFFFFFFFFFFFFF ))
  \GND_53_o_GND_53_o_equal_156_o<15>_SW0  (
    .ADR1(1'b1),
    .ADR0(line_cnt[1]),
    .ADR5(line_cnt[0]),
    .ADR3(line_cnt[9]),
    .ADR4(line_cnt[3]),
    .ADR2(line_cnt[8]),
    .O(N01)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y44" ),
    .INIT ( 1'b0 ))
  timest_2 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_2/CLK ),
    .I(timest_2_dpot_20844),
    .O(timest[2]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y44" ),
    .INIT ( 64'hF3FFF3FFC000C000 ))
  timest_2_dpot (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR1(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[2]),
    .ADR3(wr_dst_rdy_i),
    .ADR2(\Result<2>3_0 ),
    .O(timest_2_dpot_20844)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y44" ),
    .INIT ( 1'b0 ))
  timest_1 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_1/CLK ),
    .I(timest_1_dpot_20824),
    .O(timest[1]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y44" ),
    .INIT ( 64'hAAF0F0F0AAF0F0F0 ))
  timest_1_dpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR2(timest[1]),
    .ADR4(wr_dst_rdy_i),
    .ADR0(\Result<1>3_0 ),
    .O(timest_1_dpot_20824)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y44" ),
    .INIT ( 64'h0000004000000000 ))
  \GND_53_o_GND_53_o_equal_156_o<15>  (
    .ADR5(line_cnt[7]),
    .ADR1(line_cnt[6]),
    .ADR0(line_cnt[4]),
    .ADR3(line_cnt[5]),
    .ADR2(line_cnt[2]),
    .ADR4(N01),
    .O(GND_53_o_GND_53_o_equal_156_o)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y44" ),
    .INIT ( 1'b0 ))
  timest_0 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_0/CLK ),
    .I(timest_0_dpot_20837),
    .O(timest[0]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y44" ),
    .INIT ( 64'hFF0FFFFFF0000000 ))
  timest_0_dpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[0]),
    .ADR4(wr_dst_rdy_i),
    .ADR3(\Result<0>3_0 ),
    .O(timest_0_dpot_20837)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 1'b0 ))
  timest_10 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_10/CLK ),
    .I(timest_10_dpot_20848),
    .O(timest[10]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 64'hBBFFBBFF88008800 ))
  timest_10_dpot (
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR1(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[10]),
    .ADR3(wr_dst_rdy_i),
    .ADR0(\Result<10>3_0 ),
    .O(timest_10_dpot_20848)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 1'b0 ))
  timest_9 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_9/CLK ),
    .I(timest_9_dpot_20856),
    .O(timest[9]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 64'hAAF0F0F0AAF0F0F0 ))
  timest_9_dpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR2(timest[9]),
    .ADR4(wr_dst_rdy_i),
    .ADR0(\Result<9>3_0 ),
    .O(timest_9_dpot_20856)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 1'b0 ))
  timest_8 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_8/CLK ),
    .I(timest_8_dpot_20862),
    .O(timest[8]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 64'hAAAAFF00FF00FF00 ))
  timest_8_dpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR5(GND_53_o_GND_53_o_equal_156_o),
    .ADR3(timest[8]),
    .ADR4(wr_dst_rdy_i),
    .ADR0(\Result<8>3_0 ),
    .O(timest_8_dpot_20862)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 1'b0 ))
  timest_7 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_7/CLK ),
    .I(timest_7_dpot_20868),
    .O(timest[7]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y46" ),
    .INIT ( 64'hDDDDFFFF88880000 ))
  timest_7_dpot (
    .ADR2(1'b1),
    .ADR3(1'b1),
    .ADR0(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[7]),
    .ADR4(wr_dst_rdy_i),
    .ADR1(\Result<7>3_0 ),
    .O(timest_7_dpot_20868)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 1'b0 ))
  timest_18 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_18/CLK ),
    .I(timest_18_dpot_20875),
    .O(timest[18]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 64'hBBFFBBFF88008800 ))
  timest_18_dpot (
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR1(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[18]),
    .ADR3(wr_dst_rdy_i),
    .ADR0(\Result<18>_0 ),
    .O(timest_18_dpot_20875)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 1'b0 ))
  timest_17 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_17/CLK ),
    .I(timest_17_dpot_20883),
    .O(timest[17]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 64'hAAF0F0F0AAF0F0F0 ))
  timest_17_dpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR2(timest[17]),
    .ADR4(wr_dst_rdy_i),
    .ADR0(\Result<17>_0 ),
    .O(timest_17_dpot_20883)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 1'b0 ))
  timest_16 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_16/CLK ),
    .I(timest_16_dpot_20889),
    .O(timest[16]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 64'hAAAAFF00FF00FF00 ))
  timest_16_dpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR5(GND_53_o_GND_53_o_equal_156_o),
    .ADR3(timest[16]),
    .ADR4(wr_dst_rdy_i),
    .ADR0(\Result<16>_0 ),
    .O(timest_16_dpot_20889)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 1'b0 ))
  timest_15 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_15/CLK ),
    .I(timest_15_dpot_20895),
    .O(timest[15]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y48" ),
    .INIT ( 64'hFF55FFFFAA000000 ))
  timest_15_dpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR0(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[15]),
    .ADR4(wr_dst_rdy_i),
    .ADR3(\Result<15>3_0 ),
    .O(timest_15_dpot_20895)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 1'b0 ))
  timest_22 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_22/CLK ),
    .I(timest_22_dpot_20902),
    .O(timest[22]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 64'hCFFFCFFFC000C000 ))
  timest_22_dpot (
    .ADR0(1'b1),
    .ADR4(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[22]),
    .ADR2(wr_dst_rdy_i),
    .ADR1(\Result<22>_0 ),
    .O(timest_22_dpot_20902)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 1'b0 ))
  timest_21 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_21/CLK ),
    .I(timest_21_dpot_20910),
    .O(timest[21]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 64'hAFFFA000AFFFA000 ))
  timest_21_dpot (
    .ADR5(1'b1),
    .ADR1(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR4(timest[21]),
    .ADR2(wr_dst_rdy_i),
    .ADR0(\Result<21>_0 ),
    .O(timest_21_dpot_20910)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 1'b0 ))
  timest_20 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_20/CLK ),
    .I(timest_20_dpot_20916),
    .O(timest[20]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 64'hAAAAFF00FF00FF00 ))
  timest_20_dpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR3(timest[20]),
    .ADR5(wr_dst_rdy_i),
    .ADR0(\Result<20>_0 ),
    .O(timest_20_dpot_20916)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 1'b0 ))
  timest_19 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_19/CLK ),
    .I(timest_19_dpot_20922),
    .O(timest[19]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y49" ),
    .INIT ( 64'hFF33FFFFCC000000 ))
  timest_19_dpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[19]),
    .ADR1(wr_dst_rdy_i),
    .ADR3(\Result<19>_0 ),
    .O(timest_19_dpot_20922)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 1'b0 ))
  timest_26 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_26/CLK ),
    .I(timest_26_dpot_20929),
    .O(timest[26]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 64'hBBFFBBFF88008800 ))
  timest_26_dpot (
    .ADR4(1'b1),
    .ADR2(1'b1),
    .ADR1(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[26]),
    .ADR3(wr_dst_rdy_i),
    .ADR0(\Result<26>_0 ),
    .O(timest_26_dpot_20929)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 1'b0 ))
  timest_25 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_25/CLK ),
    .I(timest_25_dpot_20937),
    .O(timest[25]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 64'hBBFF8800BBFF8800 ))
  timest_25_dpot (
    .ADR5(1'b1),
    .ADR2(1'b1),
    .ADR3(GND_53_o_GND_53_o_equal_156_o),
    .ADR4(timest[25]),
    .ADR1(wr_dst_rdy_i),
    .ADR0(\Result<25>_0 ),
    .O(timest_25_dpot_20937)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 1'b0 ))
  timest_24 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_24/CLK ),
    .I(timest_24_dpot_20943),
    .O(timest[24]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 64'hAFAFA0A0FFFF0000 ))
  timest_24_dpot (
    .ADR3(1'b1),
    .ADR1(1'b1),
    .ADR5(GND_53_o_GND_53_o_equal_156_o),
    .ADR4(timest[24]),
    .ADR2(wr_dst_rdy_i),
    .ADR0(\Result<24>_0 ),
    .O(timest_24_dpot_20943)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 1'b0 ))
  timest_23 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_23/CLK ),
    .I(timest_23_dpot_20949),
    .O(timest[23]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y50" ),
    .INIT ( 64'hFF55FFFFAA000000 ))
  timest_23_dpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR0(GND_53_o_GND_53_o_equal_156_o),
    .ADR5(timest[23]),
    .ADR4(wr_dst_rdy_i),
    .ADR3(\Result<23>_0 ),
    .O(timest_23_dpot_20949)
  );
  X_SFF #(
    .LOC ( "SLICE_X29Y51" ),
    .INIT ( 1'b0 ))
  timest_31 (
    .CE(_n0701_inv1),
    .CLK(\NlwBufferSignal_packet_sender/timest_31/CLK ),
    .I(timest_31_dpot_20958),
    .O(timest[31]),
    .SRST(reset),
    .SET(GND),
    .RST(GND),
    .SSET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X29Y51" ),
    .INIT ( 64'hAAAAFF00FF00FF00 ))
  timest_31_dpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR4(GND_53_o_GND_53_o_equal_156_o),
    .ADR3(timest[31]),
    .ADR5(wr_dst_rdy_i),
    .ADR0(\Result<31>_0 ),
    .O(timest_31_dpot_20958)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 1'b0 ))
  line_cnt_3 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_3/CLK ),
    .I(line_cnt_3_rstpot_20964),
    .O(line_cnt[3]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 64'h00000000FF55AA00 ))
  line_cnt_3_rstpot (
    .ADR2(1'b1),
    .ADR1(1'b1),
    .ADR3(\Result<3>1_0 ),
    .ADR4(line_cnt[3]),
    .ADR0(_n0495_inv),
    .ADR5(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_3_rstpot_20964)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 1'b0 ))
  line_cnt_2 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_2/CLK ),
    .I(line_cnt_2_rstpot_20971),
    .O(line_cnt[2]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 64'h00000000F0F0FF00 ))
  line_cnt_2_rstpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(\Result<2>1_0 ),
    .ADR3(line_cnt[2]),
    .ADR4(_n0495_inv),
    .ADR5(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_2_rstpot_20971)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 1'b0 ))
  line_cnt_1 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_1/CLK ),
    .I(line_cnt_1_rstpot_20977),
    .O(line_cnt[1]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 64'h00F300C000F300C0 ))
  line_cnt_1_rstpot (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR2(\Result<1>1_0 ),
    .ADR4(line_cnt[1]),
    .ADR1(_n0495_inv),
    .ADR3(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_1_rstpot_20977)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 1'b0 ))
  line_cnt_0 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_0/CLK ),
    .I(line_cnt_0_rstpot_20983),
    .O(line_cnt[0]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y39" ),
    .INIT ( 64'h0F000F0F0F000000 ))
  line_cnt_0_rstpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR3(\Result<0>1_0 ),
    .ADR5(line_cnt[0]),
    .ADR4(_n0495_inv),
    .ADR2(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_0_rstpot_20983)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 1'b0 ))
  line_cnt_7 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_7/CLK ),
    .I(line_cnt_7_rstpot_20989),
    .O(line_cnt[7]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 64'h0F0F000F0F000000 ))
  line_cnt_7_rstpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR4(\Result<7>1_0 ),
    .ADR5(line_cnt[7]),
    .ADR3(_n0495_inv),
    .ADR2(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_7_rstpot_20989)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 1'b0 ))
  line_cnt_6 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_6/CLK ),
    .I(line_cnt_6_rstpot_20996),
    .O(line_cnt[6]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 64'h0C0F0C000C0F0C00 ))
  line_cnt_6_rstpot (
    .ADR0(1'b1),
    .ADR5(1'b1),
    .ADR1(\Result<6>1_0 ),
    .ADR4(line_cnt[6]),
    .ADR3(_n0495_inv),
    .ADR2(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_6_rstpot_20996)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 1'b0 ))
  line_cnt_5 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_5/CLK ),
    .I(line_cnt_5_rstpot_21002),
    .O(line_cnt[5]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 64'h5050555550500000 ))
  line_cnt_5_rstpot (
    .ADR3(1'b1),
    .ADR1(1'b1),
    .ADR2(\Result<5>1_0 ),
    .ADR5(line_cnt[5]),
    .ADR4(_n0495_inv),
    .ADR0(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_5_rstpot_21002)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 1'b0 ))
  line_cnt_4 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_4/CLK ),
    .I(line_cnt_4_rstpot_21008),
    .O(line_cnt[4]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y40" ),
    .INIT ( 64'h00000000FF00CCCC ))
  line_cnt_4_rstpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR3(\Result<4>1_0 ),
    .ADR1(line_cnt[4]),
    .ADR4(_n0495_inv),
    .ADR5(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_4_rstpot_21008)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y41" ),
    .INIT ( 1'b0 ))
  line_cnt_9 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_9/CLK ),
    .I(line_cnt_9_rstpot_21021),
    .O(line_cnt[9]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y41" ),
    .INIT ( 64'h00000000F0F0FF00 ))
  line_cnt_9_rstpot (
    .ADR0(1'b1),
    .ADR1(1'b1),
    .ADR2(\Result<9>1_0 ),
    .ADR3(line_cnt[9]),
    .ADR4(_n0495_inv),
    .ADR5(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_9_rstpot_21021)
  );
  X_FF #(
    .LOC ( "SLICE_X33Y41" ),
    .INIT ( 1'b0 ))
  line_cnt_8 (
    .CE(VCC),
    .CLK(\NlwBufferSignal_packet_sender/line_cnt_8/CLK ),
    .I(line_cnt_8_rstpot_21014),
    .O(line_cnt[8]),
    .RST(GND),
    .SET(GND)
  );
  X_LUT6 #(
    .LOC ( "SLICE_X33Y41" ),
    .INIT ( 64'h00000000FF00CCCC ))
  line_cnt_8_rstpot (
    .ADR0(1'b1),
    .ADR2(1'b1),
    .ADR3(\Result<8>1_0 ),
    .ADR1(line_cnt[8]),
    .ADR4(_n0495_inv),
    .ADR5(Reset_OR_DriverANDClockEnable161_21286),
    .O(line_cnt_8_rstpot_21014)
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_7/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_7/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_6/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_6/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_5/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_5/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_11/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_11/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_10/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_10/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_9/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_9/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_8/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_8/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_13/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_13/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/packet_size_count_12/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/packet_size_count_12/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_20/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_20/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_26/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_26/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_8/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_8/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_8/IN  (
    .I(\state[4]_header_checksum_input[8]_Mux_242_o_0 ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_8/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_3/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_6/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_6/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_6/IN  (
    .I(\state[4]_header_checksum_input[6]_Mux_246_o_0 ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_6/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_1/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_0/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_30/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_30/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_31/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_31/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_29/CLK  (
    .I(\state[4]_GND_55_o_Mux_197_o ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_29/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_input_29/IN  (
    .I(\state[4]_header_checksum_input[29]_Mux_200_o_0 ),
    .O(\NlwBufferSignal_packet_sender/header_checksum_input_29/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_26/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_26/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_23/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_23/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_22/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_22/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_20/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_20/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/pkt_offset_8/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/pkt_offset_8/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/pkt_offset_4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/pkt_offset_4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/pkt_offset_3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/pkt_offset_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_25/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_25/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_24/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_24/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_28/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_28/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_27/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_27/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_19/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_19/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_18/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_18/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/header_checksum_reset/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/header_checksum_reset/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_21/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_21/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd5/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd5/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd5_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd5_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd5_1/IN  (
    .I(\state_FSM_FFd5-In ),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd5_1/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd3/IN  (
    .I(\state_FSM_FFd3-In_0 ),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd3/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd2/IN  (
    .I(\state_FSM_FFd2-In_21106 ),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd2/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd3_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd3_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd3_1/IN  (
    .I(\state_FSM_FFd3-In_0 ),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd3_1/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_17/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_17/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_16/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_16/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd4_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd4_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd4_1/IN  (
    .I(\state_FSM_FFd4-In ),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd4_1/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd2_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd2_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd1_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd1_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/state_FSM_FFd1_1/IN  (
    .I(\state_FSM_FFd1-In ),
    .O(\NlwBufferSignal_packet_sender/state_FSM_FFd1_1/IN )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_29/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_29/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_flags_o_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_flags_o_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_flags_o_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_flags_o_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_14/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_14/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_13/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_13/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_12/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_12/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_11/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_11/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_30/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_30/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_13/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_13/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_12/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_12/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_31/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_31/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_6/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_6/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_5/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_5/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_8/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_8/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_7/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_7/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_src_rdy_o/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_src_rdy_o/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_6/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_6/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_5/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_5/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_11/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_11/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_10/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_10/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_9/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_9/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_15/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_15/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_14/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_14/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_13/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_13/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/wr_data_o_12/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/wr_data_o_12/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_30/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_30/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_29/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_29/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_28/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_28/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_27/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_27/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_6/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_6/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_5/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_5/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_7/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_7/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_10/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_10/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_9/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_9/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_8/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_8/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_11/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_11/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_15/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_15/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/seqn_14/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/seqn_14/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_10/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_10/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_9/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_9/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_8/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_8/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_7/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_7/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_18/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_18/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_17/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_17/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_16/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_16/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_15/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_15/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_22/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_22/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_21/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_21/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_20/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_20/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_19/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_19/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_26/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_26/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_25/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_25/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_24/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_24/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_23/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_23/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/timest_31/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/timest_31/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_3/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_3/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_2/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_2/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_1/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_1/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_0/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_0/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_7/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_7/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_6/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_6/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_5/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_5/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_4/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_4/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_9/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_9/CLK )
  );
  X_BUF   \NlwBufferBlock_packet_sender/line_cnt_8/CLK  (
    .I(clk),
    .O(\NlwBufferSignal_packet_sender/line_cnt_8/CLK )
  );
  X_ZERO   NlwBlock_packet_sender_GND (
    .O(GND)
  );
  X_ONE   NlwBlock_packet_sender_VCC (
    .O(VCC)
  );
endmodule
