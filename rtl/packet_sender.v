
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:38:11 12/07/2011 
// Design Name: 
// Module Name:    packet_sender 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps
module packet_sender(
                     input             clk,
                     input             reset,
                     input [7:0]       inter_packet_delay,  //configured via switches
		     input             start,
                     //input [23:0]      bytes,
                     //input             en,
                     //input             HSYNC,
                     //input             VSYNC,
                     //input [10:0]      HCOUNT,
                     //input [10:0]      VCOUNT,
                     //output reg        fifo_full,
                     output reg [3:0]  wr_flags_o,
		     output reg [31:0] wr_data_o,
		     output reg        wr_src_rdy_o,
		     input             wr_dst_rdy_i
                     //    output reg [1300*8-1:0] rtp_pkt //made as output for monitoring purpose.
                     // Remove when synthesizing the core
                     );
  

  

  //states
  //states

  localparam [4:0]
    IDLE       = 0,
    CALC_IP_HDR1_CHKSUM    = 1,
    CALC_IP_HDR2_CHKSUM    = 2,
    CALC_IP_HDR3_CHKSUM    = 3,
    CALC_IP_HDR4_CHKSUM    = 4,
    CALC_IP_HDR5_CHKSUM    = 5,
    SOP                    = 6,
    TRANSMIT_HDR2           = 7,
    TRANSMIT_HDR3           = 8,
    TRANSMIT_HDR4           = 9,
    TRANSMIT_HDR5           = 10,
    TRANSMIT_HDR6           = 11, 
    TRANSMIT_HDR7           = 12,
    TRANSMIT_HDR8           = 13,
    TRANSMIT_HDR9           = 14,
    TRANSMIT_HDR10          = 15,
    TRANSMIT_HDR11          = 16,
    SEND_RTP_HDR1           = 17,
    SEND_RTP_HDR2           = 18,
    SEND_RTP_HDR3           = 19,
    SEND_RTP_HDR4           = 20,
    SEND_RTP_HDR5           = 21,
    SEND_RTP1               = 22,
    SEND_RTP2               = 23,
    SEND_RTP3               = 24,
    SEND_RTP4               = 25,
    CLEANUP                 = 26,
    DELAY_STATE 				 = 27;
  
  

  (* fsm_encoding = "user" *) 
  reg [4:0]                            state, nxt_state;
  
  reg [15:0]                           pixel_cnt, pkt_cnt, line_cnt, frame_cnt,
                                       line_no, seqn, pkt_offset;
  
  reg [15:0]                           nxt_pixel_cnt, nxt_pkt_cnt, nxt_line_cnt, nxt_frame_cnt,
                                       nxt_line_no, nxt_seqn, nxt_pkt_offset;
  reg [31:0]                           timest;
  reg [31:0]                           nxt_timest;
  reg                                  nxt_fifo_full;

  reg [13:0]                           packet_size_count, nxt_packet_size_count;

  reg                                  nxt_wr_src_rdy_o;
  reg [3:0]                            nxt_wr_flags_o;
  reg [31:0]                           nxt_wr_data_o;
  
  //reg              sof, nxt_sof;


  reg [10:0]                           hcount, vcount, nxt_hcount, nxt_vcount;
  reg [15:0]                           delay,nxt_delay;
  reg [1:0]										rgb_cnt, nxt_rgb_cnt;
  
  //temporary variables. DELETE THEM LATER
  //  reg                                  en;
  //reg              VSYNC;  
  //reg              fifo_full;           


  localparam IP_IDENTIFICATION = 16'haabb;
  localparam IP_FRAG = 13'd0;

  parameter SRC_PORT = 16'd5004;
  parameter DST_PORT = 16'd5004;
  parameter SRC_MAC = 48'h0018_3e01_53d5;
  parameter DST_MAC = 48'h782b_cb87_cc67;  //ubuntu mac
  parameter DST_IP = 32'hc0a8_0101;  //192.168.1.1
  parameter SRC_IP = 32'hc0a8_0102;  //192.168.1.2
  
  // Generate valid IP header checksums
  wire [15:0]                          header_checksum;
  reg [31:0]                           header_checksum_input;
  reg                                  header_checksum_reset, nxt_header_checksum_reset;
  
  ip_header_checksum ip_header_checksum (
                                         .clk(clk),
                                         .checksum(header_checksum),
                                         .header(header_checksum_input),
                                         .reset(header_checksum_reset)
                                         );
  
  
  // Calculate packet lengths
  wire [15:0]                          packet_length_udp1, packet_length_ip1, packet_length_rtp1;
  wire [15:0]                          packet_length_udp2, packet_length_ip2, packet_length_rtp2;
  
  // assign packet_length_rtp1 = 16'd1292 ;  //1272+20 byte header = 1292 bytes
  // assign packet_length_udp1 = 16'd1300 ; // 1292 +  8 byte udp hdr
  // assign packet_length_ip1  = 16'd1320 ; // IP header adds 20 bytes to UDP packet

  // assign packet_length_rtp2 = 16'd1316 ;  //1296+20 = 1316 bytes
  // assign packet_length_udp2 = 16'd1324 ; // 1316 +  8 byte hdr
  //  assign packet_length_ip2  = 16'd1344 ; // 1324 + 20 IP header adds 20 bytes to UDP packet
  
  assign packet_length_rtp1 = {3'b000, 8'b0010_1000, 5'b01100}; //rtp payload +  20 byte hdr = 1272 + 20 = 1292
  assign packet_length_udp1 = {3'b000, 8'b0010_1000, 5'b10100}; //rtp + udp hdr = 1300
  assign packet_length_ip1  = {3'b000, 8'b0010_1001, 5'b01000}; //rtp + udp hdr + ip hdr = 1320
  
  //
  assign packet_length_rtp2 = {3'b000, 8'b0010_1001, 5'b00100}; //rtp payload +  20 byte hdr = 1296+20 = 1316
  assign packet_length_udp2 = {3'b000, 8'b0010_1001, 5'b01100}; //rtp + udp hdr = 1316 + 8 = 1324 
  assign packet_length_ip2  = {3'b000, 8'b0010_1010, 5'b00000}; //rtp + udp hdr + ip hdr = 1320  = 1324 + 20 = 1344 
  
  
  //ip packet header (20 bytes)
  wire [31:0]                          header_1, header_2, header_3, header_4, header_5; //broken into 5 (4 bytes)
  
  //assign header_1 = {16'h4500, packet_length_ip};  //{version,ihl}, ip packet length including ip header
  assign header_2 = {IP_IDENTIFICATION[15:0], 3'b000, IP_FRAG[12:0]}; // IP identification, fragment;
  assign header_3 = {16'h4011, 16'h0000}; // TTL, protocol=17 for udp, header checksum
  assign header_4 = SRC_IP;
  assign header_5 = DST_IP;
  
  reg [15:0] INTER_PACKET_DELAY;

  
  reg [24:0] pixel_mem [1280*11-1:0];  //11 rows of 720p frame
  
   
  initial // Read the memory contents from the file hdmi_capture.txt in pixel_mem memory
    begin
      $readmemb("hdmi_capture.txt", pixel_mem);
      $display("Pixel memory read successfully\n");
    end
  
  
  //Sequential logic
  always @(posedge clk)
    if(reset)
      begin
        state         <= #1 IDLE;
        pixel_cnt     <= #1 16'd0;
        pkt_cnt       <= #1 16'd0;
        line_cnt      <= #1 16'd0;
        frame_cnt     <= #1 16'd0;
        //fifo_full     <= #1 0;
        pkt_offset    <= #1 16'd0;
        line_no       <= #1 16'd0;//26;
        seqn          <= #1 16'd0;
        timest        <= #1 32'h0;
        wr_data_o     <= #1 32'd0;
        wr_flags_o    <= #1 4'd0;
        wr_src_rdy_o  <= #1 0;
        //sof           <= #1 0;
        hcount        <= #1 11'd0;
        vcount        <= #1 11'd0;
        header_checksum_reset <= #1 1;
	packet_size_count    <= #1 14'd0;
	delay                 <= #1 16'd0;     
	rgb_cnt               <= #1 2'd0;
      end // if (reset)
  
    else
      begin
        state         <= #1 nxt_state;
        pixel_cnt     <= #1 nxt_pixel_cnt;
        pkt_cnt       <= #1 nxt_pkt_cnt;
        line_cnt      <= #1 nxt_line_cnt;
        frame_cnt     <= #1 nxt_frame_cnt;
        //fifo_full     <= #1 nxt_fifo_full;
        pkt_offset    <= #1 nxt_pkt_offset;
        line_no       <= #1 nxt_line_no;
        seqn          <= #1 nxt_seqn;
        timest        <= #1 nxt_timest;
        wr_data_o     <= #1 nxt_wr_data_o;
        wr_src_rdy_o  <= #1 nxt_wr_src_rdy_o;
        wr_flags_o    <= #1 nxt_wr_flags_o;
        //  sof           <= #1 nxt_sof;
        hcount        <= #1 nxt_hcount;
        vcount        <= #1 nxt_vcount;
        header_checksum_reset <= #1 nxt_header_checksum_reset; //unassert reset
        packet_size_count <= #1 nxt_packet_size_count;
	delay             <= #1 nxt_delay;
	rgb_cnt           <= #1 nxt_rgb_cnt;
      end // else: !if(reset)
  

  //combinational logic

  always @*
    begin
      //assign defaults
      nxt_state        = state;
      nxt_pixel_cnt    = pixel_cnt;
      nxt_pkt_cnt      = pkt_cnt;
      nxt_line_cnt     = line_cnt;
      nxt_frame_cnt    = frame_cnt;
      //nxt_fifo_full    = fifo_full;
      nxt_pkt_offset   = pkt_offset;
      nxt_line_no      = line_no;
      nxt_seqn         = seqn;
      nxt_timest       = timest;
      nxt_wr_src_rdy_o   = wr_src_rdy_o;
      nxt_wr_flags_o     = wr_flags_o;
      nxt_wr_data_o      = wr_data_o;
      //nxt_sof            = sof;
      nxt_hcount         = hcount;
      nxt_vcount         = vcount;
      nxt_header_checksum_reset = header_checksum_reset;
      nxt_packet_size_count = packet_size_count;
      nxt_delay = delay;
		nxt_rgb_cnt = rgb_cnt;
      case(state)
	/*START:
	 begin
	 if(start)
	 nxt_state = IDLE;
	  end	*/
        IDLE:
          begin
            //      nxt_fifo_full = 1'b0;
            
            //Start of traffic (should see a HSYNC transition)
            /* -----\/----- EXCLUDED -----\/-----
             if(HSYNC & ~VSYNC)  //or VCOUNT == 1
             nxt_sof = 1;
             
             if(en && sof)
             nxt_state = MAKE_PKT;
             -----/\----- EXCLUDED -----/\----- */
	    //en = 1; //pixel_mem[hcount][24];
            //VSYNC = 0;  //temporary
            nxt_packet_size_count = 0;
            
            if( /*seqn < 720*/  /* (seqn < 2160) && */ (wr_dst_rdy_i == 1'b1) ) 
	      begin
		nxt_header_checksum_reset = 0; //unassert reset
		nxt_state = CALC_IP_HDR1_CHKSUM;
	      end
          end
        
        
        CALC_IP_HDR1_CHKSUM:
          begin
	    if(wr_dst_rdy_i)
	      begin
                if(pkt_cnt !=2 )
                  header_checksum_input = {16'h4500, packet_length_ip1};
                else
                  header_checksum_input = {16'h4500, packet_length_ip2};
                
                nxt_state = CALC_IP_HDR2_CHKSUM;
	      end
          end

        CALC_IP_HDR2_CHKSUM:
          begin
	    if(wr_dst_rdy_i)
	      begin
                header_checksum_input = header_2;
                nxt_state = CALC_IP_HDR3_CHKSUM;
	      end
	  end
        
        CALC_IP_HDR3_CHKSUM:
          begin
	    if(wr_dst_rdy_i)
	      begin
                header_checksum_input = header_3;
                nxt_state = CALC_IP_HDR4_CHKSUM;
	      end
	  end
        
        CALC_IP_HDR4_CHKSUM:
          begin
	    if(wr_dst_rdy_i)
	      begin
                header_checksum_input = header_4;
                nxt_state = CALC_IP_HDR5_CHKSUM;
	      end
	  end
        
        CALC_IP_HDR5_CHKSUM:
          begin
            header_checksum_input = header_5;
            nxt_state = SOP;
          end
        
        SOP:
          begin
            //start_pkt();  //start of Ethernet frame
            nxt_wr_src_rdy_o = 1;	
            nxt_wr_flags_o = 4'b0001; // Start of packet
            //transmit_header(DST_MAC[47:16]); //start with 32 bits of dst_mac address
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = DST_MAC[47:16];
		nxt_state = TRANSMIT_HDR2;
	      end
          end

        TRANSMIT_HDR2:
          begin
            //transmit_header({DST_MAC[15:0], SRC_MAC[47:32]}); 
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = {DST_MAC[15:0], SRC_MAC[47:32]};
		//clear_mac_flags();
		nxt_state = TRANSMIT_HDR3;
	      end
            //clear mac flags
            nxt_wr_flags_o = 4'b0000;
            
          end
        
        TRANSMIT_HDR3:
          begin
            //transmit_header(SRC_MAC[31:0]);
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = SRC_MAC[31:0];
                nxt_state = TRANSMIT_HDR4;
	      end
          end

        TRANSMIT_HDR4:
          begin
            //transmit_header({16'h0800, 16'h4500}); // First 8 bits: hwtype ethernet (4), protocol type ipv4 (1),  header length (1) (*4), dsc (2)
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = {16'h0800, 16'h4500};		
                nxt_state = TRANSMIT_HDR5;
	      end
          end
        
        TRANSMIT_HDR5:
          begin
	    if(wr_dst_rdy_i)
	      begin
		
		if(pkt_cnt != 2)
		  //transmit_header({packet_length_ip1, header_2[31:16]});
		  nxt_wr_data_o = {packet_length_ip1, header_2[31:16]};
                else
		  //transmit_header({packet_length_ip2, header_2[31:16]});
		  nxt_wr_data_o = {packet_length_ip2, header_2[31:16]};
                nxt_state = TRANSMIT_HDR6;
	      end // if (wr_dst_rdy_i)
          end // case: TRANSMIT_HDR5
        

        TRANSMIT_HDR6:
          begin
            //transmit_header({header_2[15:0], header_3[31:16]});
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = {header_2[15:0], header_3[31:16]};
		nxt_state = TRANSMIT_HDR7;
	      end	
          end
        
        TRANSMIT_HDR7:
          begin
            //transmit_header({header_checksum, header_4[31:16]}); // Inject the calculated header checksum here
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = {header_checksum, header_4[31:16]};		
		nxt_state = TRANSMIT_HDR8;
	      end
          end
        
        TRANSMIT_HDR8:
          begin
            //transmit_header({header_4[15:0], header_5[31:16]});
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = {header_4[15:0], header_5[31:16]};
		nxt_state = TRANSMIT_HDR9;
	      end	
          end
        
        TRANSMIT_HDR9:
          begin
            //transmit_header({header_5[15:0], SRC_PORT});
	    if(wr_dst_rdy_i)
	      begin
		nxt_wr_data_o = {header_5[15:0], SRC_PORT};
		nxt_state = TRANSMIT_HDR10;
	      end
          end
        
        TRANSMIT_HDR10:
          begin
	    if(wr_dst_rdy_i)
	      begin
                if(pkt_cnt !=2)
		  //transmit_header({DST_PORT, packet_length_udp1});
		  nxt_wr_data_o = {DST_PORT, packet_length_udp1};
                else
		  //transmit_header({DST_PORT, packet_length_udp2});
		  nxt_wr_data_o = {DST_PORT, packet_length_udp2};
	        
	        nxt_state = TRANSMIT_HDR11;
	      end
          end
        
        TRANSMIT_HDR11:
          begin
	    if(wr_dst_rdy_i)
	      begin
                //       if(pkt_cnt != 2)
                
                if(inter_packet_delay[7])
                  begin
                    if(seqn  < 16'd33)
		      nxt_wr_data_o = {32'h0000,8'h80,1'b0,7'd24};
                    
                    else
		      nxt_wr_data_o ={32'h0000,8'h80,1'b1,7'd24};
                  end
                  
                else
                  begin
		    if(seqn  < /*719*/ /*101 */ /*17*/ 16'd2159)			//modify this
		      //transmit_header({32'h0000,8'h80,1'b0,7'd24}); // {UDP checksum (4), start of RTP header's 2 bytes}
		      nxt_wr_data_o = {32'h0000,8'h80,1'b0,7'd24};
                    
                    else
		      //transmit_header({32'h0000,8'h80,1'b1,7'd24}); //  1'b1 marks the end of frame
		      nxt_wr_data_o ={32'h0000,8'h80,1'b1,7'd24};
                  end // else: !if(inter_packet_delay[7])
                
                nxt_state = SEND_RTP_HDR1;
	      end // if (wr_dst_rdy_i)
          end // case: TRANSMIT_HDR11
        
        
        SEND_RTP_HDR1:
          begin
            if (wr_dst_rdy_i) 
              begin
                nxt_wr_data_o = {seqn,timest[31:16]};
                nxt_state = SEND_RTP_HDR2;
              end
          end
        
        SEND_RTP_HDR2:
          begin
            if (wr_dst_rdy_i) 
              begin
                nxt_wr_data_o = {timest[15:0],16'd0}; //{timstamp,ssrc[31:16]}
                nxt_state = SEND_RTP_HDR3;
              end
          end
        
        
        SEND_RTP_HDR3:
          begin
            if (wr_dst_rdy_i) 
              begin
                nxt_wr_data_o = {16'd0,16'd0}; //{ssrc[15:0],length[31:16]}
                nxt_state = SEND_RTP_HDR4;
              end
          end

        
        SEND_RTP_HDR4:
          begin
            if (wr_dst_rdy_i) 
              begin
                if(pkt_cnt != 2)
		  nxt_wr_data_o = {16'd1272,line_no[15:0]}; //{length[15:0],line_no}
                else
		  nxt_wr_data_o = {16'd1296,line_no[15:0]};
                
                nxt_state = SEND_RTP_HDR5;
              end
          end
        
        SEND_RTP_HDR5:
          begin
            if(wr_dst_rdy_i)
              begin
                if(inter_packet_delay[7] == 1)
                  nxt_wr_data_o = {pkt_offset,pixel_mem[hcount][23:8]}; //{offset,pixel}
                else
                  begin
		    if(rgb_cnt == 0)
		      nxt_wr_data_o = {pkt_offset,16'hFF_00}; //{offset,pixel}
		    else if(rgb_cnt == 1)
		      nxt_wr_data_o = {pkt_offset,16'h00_FF};
		    else
		      nxt_wr_data_o = {pkt_offset,16'h00_00};
		  end // else: !if(inter_packet_delay[7] == 1)
                
                nxt_state = SEND_RTP1;
              end // if (wr_dst_rdy_i)
          end // case: SEND_RTP_HDR5
        

        SEND_RTP1:
          begin
            if (wr_dst_rdy_i) 
              begin
                if(inter_packet_delay[7] == 1)
                  nxt_wr_data_o = {pixel_mem[hcount][7:0],pixel_mem[hcount+1][23:0]};
                else
                  begin
                    if(rgb_cnt == 0)
		      nxt_wr_data_o = {8'h00,24'hFF_00_00};
		    else if(rgb_cnt == 1)
		      nxt_wr_data_o = {8'h00,24'h00_FF_00};
		    else
		      nxt_wr_data_o = {8'hFF,24'h00_00_FF};
		  end // else: !if(inter_packet_delay[7] == 1)
                
                nxt_packet_size_count = packet_size_count + 1'b1;
		nxt_state = SEND_RTP2;
              end // if (wr_dst_rdy_i)
          end // case: SEND_RTP1
        
        
        SEND_RTP2:
          begin
            if (wr_dst_rdy_i) 
              begin
                if(inter_packet_delay[7] == 1)
                  nxt_wr_data_o = {pixel_mem[hcount+2][23:0],pixel_mem[hcount+3][23:16]};
                else
                  begin
                    if(rgb_cnt == 0)
		      nxt_wr_data_o = {24'hFF_00_00,8'hFF};
		    else if(rgb_cnt == 1)
		      nxt_wr_data_o = {24'h00_FF_00,8'h00};
		    else
		      nxt_wr_data_o = {24'h00_00_FF,8'h00};
		  end // else: !if(inter_packet_delay[7] == 1)
                
                nxt_packet_size_count = packet_size_count + 1'b1;
		nxt_state = SEND_RTP3;
              end // if (wr_dst_rdy_i)
          end // case: SEND_RTP2
        
        
        SEND_RTP3:
              begin
                if (wr_dst_rdy_i) 
                  begin
                    if(inter_packet_delay[7] == 1)
                      nxt_wr_data_o = {pixel_mem[hcount+3][15:0],pixel_mem[hcount+4][23:8]};
                    else
                      begin
                        if(rgb_cnt == 0)
		          nxt_wr_data_o = {16'h00_00,16'hFF_00};
		        else if (rgb_cnt == 1)
		          nxt_wr_data_o = {16'hFF_00,16'h00_FF};
		        else
		          nxt_wr_data_o = {16'h00_FF,16'h00_00};
		      end // else: !if(inter_packet_delay[7] == 1)
                    
		    nxt_packet_size_count = packet_size_count + 1'b1;
		    nxt_state = SEND_RTP4;
                  end // if (wr_dst_rdy_i)
              end // case: SEND_RTP3
        
                
        
        SEND_RTP4:
          begin
            if (wr_dst_rdy_i) 
              begin
                if(inter_packet_delay[7] == 1)
                  nxt_wr_data_o = {pixel_mem[hcount+4][7:0],pixel_mem[hcount+5][23:0]};
                else
                  begin
		    if(rgb_cnt == 0)
		      nxt_wr_data_o = {8'h00,24'hFF_00_00};
		    else if(rgb_cnt == 1)
		      nxt_wr_data_o = {8'h00,24'h00_FF_00};
		    else
		      nxt_wr_data_o = {8'hFF,24'h00_00_FF};
		  end // else: !if(inter_packet_delay[7] == 1)
                
                nxt_pixel_cnt = pixel_cnt + 4; //6;
                nxt_hcount    = hcount + 4;  //adjusted from 6 to 4
                nxt_packet_size_count = packet_size_count + 1'b1;
	        if ( ( (pkt_cnt != 2) && (packet_size_count == {5'b0, 8'b0010_0111, 3'b110}) )||
		     (   (pkt_cnt == 2) && (packet_size_count == {5'b0, 8'b0010_1000, 3'b100}) )
		     )
		  begin
		    nxt_wr_flags_o = 4'b0010; // 4 bytes, EOF	
		    nxt_state = CLEANUP;
		  end
                else
		  nxt_state = SEND_RTP2;
                
              end // if (wr_dst_rdy_i)
            
          end // case: SEND_RTP4
        
        
        CLEANUP:
          begin
            if (wr_dst_rdy_i) // Wait until we're sure the last word has been received.
              begin
		nxt_wr_src_rdy_o = 0;
		nxt_wr_flags_o = 0;
               	nxt_header_checksum_reset = 1;
		
		if(  pkt_cnt != 2 )
		  begin
		    nxt_pkt_cnt = pkt_cnt +1;
		    nxt_pkt_offset = pkt_offset + 16'd424;//16'd1272;  //next packet's offset is 1272 from current
	      end
		else if  (pkt_cnt == 2)
		  begin
		    nxt_line_cnt    = line_cnt + 1;    //move to next line
		    nxt_line_no     = line_no + 1;     //line number is 26 + 1
		    nxt_pkt_offset  = 0;               // reset offset to 0
		    nxt_pkt_cnt     = 0;
		    nxt_hcount      = 0;               //reset hcount
		  end 
		
		if(line_cnt == 719)
		  begin
		    nxt_frame_cnt = nxt_frame_cnt + 1;
		    nxt_line_cnt = 0;
		    nxt_line_no = 0;//26;
		    nxt_timest = nxt_timest + 1;
		    nxt_rgb_cnt  = (rgb_cnt + 1) % 3;
		    nxt_seqn = 0;
		  end
                
		else 
		  nxt_seqn = (seqn + 1);// % 102;
                
		nxt_pixel_cnt = 0;
		
		
		nxt_state = DELAY_STATE;
		
	      end // if (wr_dst_rdy_i)
            
          end // case: CLEANUP
        
	DELAY_STATE:
	  begin
	    if(wr_dst_rdy_i)
	      begin
		if(delay < INTER_PACKET_DELAY)
		  begin
		    nxt_delay = delay + 1;
		  end
	        
		else
		  begin
		    nxt_delay = 0;
		    nxt_state = IDLE;
		  end	
	      end //wr_dst_rdy_i
	  end
	
      endcase // case (state)
      
    end // always @ *
  
  
  always @*
    begin
      case(inter_packet_delay)
	8'd0:
	  begin
	    INTER_PACKET_DELAY = 16'd3000;
	  end
	8'd1:
	  begin
	    INTER_PACKET_DELAY = 16'd2625;
	  end
	8'd2:
	  begin
	    INTER_PACKET_DELAY = 16'd2250;
	  end
	8'd3:
	  begin
	    INTER_PACKET_DELAY = 16'd1875;
	  end
	8'd4:
	  begin
	    INTER_PACKET_DELAY = 16'd1500;
	  end
	8'd5:
	  begin
	    INTER_PACKET_DELAY = 16'd1125;
	  end
	8'd6:
	  begin
	    INTER_PACKET_DELAY = 16'd750;
	  end
	8'd7:
	  begin
	    INTER_PACKET_DELAY = 16'd375; 
	    
	  end
	//increasing inter_packet_delay		
	8'd8:
	  begin
	    INTER_PACKET_DELAY = 16'd5500; 
	  end
	8'd16:
	  begin
	    INTER_PACKET_DELAY = 16'd8000;
	  end
	8'd24:
	  begin
	    INTER_PACKET_DELAY = 16'd10500;
	  end
	8'd32:
	  begin
	    INTER_PACKET_DELAY = 16'd13000;
	  end
	8'd40:
	  begin
	    INTER_PACKET_DELAY = 16'd15500;
	  end
	8'd48:
	  begin
	    INTER_PACKET_DELAY = 16'd18000;
	  end
	8'd56:
	  begin
	    INTER_PACKET_DELAY = 16'd20500;
	  end
	default:
	  INTER_PACKET_DELAY = 16'd3000;
	
      endcase
    end	
  
  
  /* -----\/----- EXCLUDED -----\/-----
   task start_pkt;
   begin
   nxt_wr_src_rdy_o = 1;	
   nxt_wr_flags_o = 4'b0001; // Start of packet
    end

  endtask
   -----/\----- EXCLUDED -----/\----- */
  

  /*task transmit_header;
   input [31:0] header;
   begin
   if (wr_dst_rdy_i)
   nxt_wr_data_o = header;
    end
   
  endtask
   */
  /* -----\/----- EXCLUDED -----\/-----
   task clear_mac_flags;
   begin
   nxt_wr_flags_o = 4'b0000;
    end
  endtask
   -----/\----- EXCLUDED -----/\----- */
  /*
   wire [35:0] CONTROL1;
   
   icon1 ICON1 (
   .CONTROL0(CONTROL1) // INOUT BUS [35:0]
   );

   
   ila1 ILA1 (
   .CONTROL(CONTROL1), // INOUT BUS [35:0]
   .CLK(clk), // IN
   .TRIG0(seqn), // IN BUS [15:0]
   .TRIG1(wr_data_o) // IN BUS [31:0]
   );
   */
  
endmodule // packetizer